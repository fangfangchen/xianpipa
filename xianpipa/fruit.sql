create database fruit;

use fruit;

create table user(
    user_name varchar(20) not null primary key,
    user_pwd varchar(32) not null,
    address varchar(1000),
    email varchar(20),
    tel varchar(15),
    last_time datetime
);

select * from user where(user_name="15159240617");

select * from user;

insert into user(user_name,user_pwd) values("15159240619","az123123");

/*drop table  user;*/

delete from user where(user_name="15159240618");

create table useraddress(
    user_name varchar(20) not null,
    receive nvarchar(100)
);

select * from useraddress;

/*drop table useraddress;*/

create table administrator(
	ad_id int not null auto_increment primary key,
    ad_name varchar(20) not null,
    ad_pwd varchar(32) not null
);

select * from administrator;

create table goods(
	pro_id int not null primary key,
    pro_name nvarchar(20),
    kind_no int,
    sup_no char(6),
    gstorage int,  					/*åºå­*/
    pro_weight nvarchar(10),
    pro_price numeric(7,2),
    pro_disprice numeric(7,2),
    pro_img varchar(1000),
    pro_content nvarchar(1000),		/*æè¿°*/
    pro_browse int,
    pro_sell int
    /*pro_cost numeric(7,2) *è¿è´¹*/
);

/*drop table goods;*/

select * from goods;

SELECT * FROM `goods` WHERE `pro_id` = 1428572218;

SELECT * FROM `goods` WHERE `pro_id` = 1428572218 LIMIT 1;

SELECT `pro_img` FROM `browse` INNER JOIN Goods ON Browse.pro_id = Goods.pro_id WHERE `browse` = '1430125772' LIMIT 1;SELECT `pro_img` FROM `browse` INNER JOIN Goods ON Browse.pro_id = Goods.pro_id WHERE `browse` = '1430125772' LIMIT 1

insert into goods values(150001,"福建莆田早钟枇杷",1,"151101",12,1,20,20,"pic1.png","很甜非常好",10,6);
insert into goods values(150002,"福建莆田早钟枇杷",1,"151101",12,1,22,20,"pic1.png","很甜非常好",10,7);
insert into goods values(150003,"福建莆田早钟枇杷3",1,"151101",12,1,24,20,"pic1.png","很甜非常好",10,8);
insert into goods values(150004,"福建莆田早钟枇杷4",1,"151101",12,1,22,20,"pic1.png","很甜非常好",10,9);
insert into goods values(150005,"福建莆田早钟枇杷5",1,"151101",12,1,21,20,"pic1.png","很甜非常好",10,10);
insert into goods values(150006,"福建莆田早钟枇杷6",1,"151101",12,1,27,20,"pic1.png","很甜非常好",10,11);
insert into goods values(150007,"福建莆田早钟枇杷7",1,"151101",12,1,29,20,"pic1.png","很甜非常好",10,12);
insert into goods values(150008,"福建莆田早钟枇杷8",1,"151101",12,1,23,20,"pic1.png","很甜非常好",10,13);
insert into goods values(150009,"福建莆田早钟枇杷1",1,"151101",12,1,20,20,"pic1.png","很甜非常好",10,6);
insert into goods values(150010,"福建莆田早钟枇杷2",1,"151101",12,1,22,20,"pic1.png","很甜非常好",10,7);
insert into goods values(150011,"福建莆田早钟枇杷3",1,"151101",12,1,24,20,"pic1.png","很甜非常好",10,8);
insert into goods values(150012,"福建莆田早钟枇杷4",1,"151101",12,1,22,20,"pic1.png","很甜非常好",10,9);
insert into goods values(150013,"福建莆田早钟枇杷5",1,"151101",12,1,21,20,"pic1.png","很甜非常好",10,10);
insert into goods values(150014,"福建莆田早钟枇杷6",1,"151101",12,1,27,20,"pic1.png","很甜非常好",10,11);
insert into goods values(150015,"福建莆田早钟枇杷7",1,"151101",12,1,29,20,"pic1.png","很甜非常好",10,12);
insert into goods values(150016,"福建莆田早钟枇杷8",1,"151101",12,1,23,20,"pic1.png","很甜非常好",10,13);
insert into goods values(150017,"福建莆田早钟枇杷6",1,"151101",12,1,27,20,"pic1.png","很甜非常好",10,11);
insert into goods values(150018,"福建莆田早钟枇杷7",1,"151101",12,1,29,20,"pic1.png","很甜非常好",10,12);
insert into goods values(150019,"福建莆田早钟枇杷8",1,"151101",12,1,23,20,"pic1.png","很甜非常好",10,13);
insert into goods values(150020,"福建莆田早钟枇杷8",1,"151101",12,1,23,20,"pic1.png","很甜非常好",10,13);
insert into goods values(150021,"四川早钟枇杷8",1,"151102",12,1,23,20,"pic1.png","很甜非常好",10,13);
insert into goods values(150022,"四川晚钟枇杷8",1,"151102",12,1,23,20,"pic1.png","很甜非常好",10,13);


create table special(
	kind_no int not null primary key,
    kind_name nvarchar(50)
);

drop table special;

select * from special;

insert into special values(1,"鲜枇杷");
insert into special values(2,"枇杷罐头");
insert into special values(3,"枇杷膏");


create table produce(
	sup_no char(6) not null primary key,
    sup_name nvarchar(50)
);

select * from produce;

insert into produce values('151101',"福建");
insert into produce values('151102',"四川");


create table shoppingcart(
	id int not null auto_increment primary key,
    user_name varchar(20) not null,
    pro_id int,
    add_time datetime,
    add_count int,
    order_id varchar(50),
    post_time datetime,
    current_status int,
    sumprice numeric(7,2)
);


select * from shoppingcart;

delete from shoppingcart where id > 7;

select add_count from shoppingcart where user_name="15159240619" and pro_id=150005;

select sumprice from shoppingcart where user_name="15159240619" and pro_id=150005 and current_status=0;

select sum(add_count) from shoppingcart where user_name="13799640850" and current_status=0;

/*drop table shoppingcart;*/

SELECT * FROM `goods` INNER JOIN Shoppingcart ON Shoppingcart.pro_id = goods.pro_id GROUP BY goods.pro_id;

SELECT SUM(add_count) AS tp_sum FROM `shoppingcart` WHERE `order_id` = '76981428829071' AND `user_name` = '15159240619';

SELECT * FROM `goods` INNER JOIN Shoppingcart ON Shoppingcart.pro_id = goods.pro_id WHERE `user_name` = '15159240619' AND `current_status` = 0 GROUP BY goods.pro_id;

select * from `Shoppingcart` INNER JOIN  Goods ON Shoppingcart.pro_id = Goods.pro_id where `user_name` = '15159240619' AND 'order_id' = '24431428998226';

SELECT `id` FROM `shoppingcart` WHERE `user_name` = '15159240619' AND `order_id` = '24431428998226';

SELECT `pro_id` FROM `shoppingcart` WHERE `user_name` = '15159240619' AND `order_id` = '24431428998226';

SELECT * FROM `goods` INNER JOIN Produce ON Goods.sup_no = Produce.sup_no;

SELECT * FROM `goods` INNER JOIN Produce ON Goods.sup_no = Produce.sup_no  WHERE `sup_name` = '福建' ;

delete from shoppingcart where(user_name="15159240619");

create table browse(
	user_name varchar(20) not null,
	pro_id int not null,
	browse int
);


select * from browse;

/*drop table browse;*/


create table focus(
	user_name varchar(20) not null,
	pro_id int not null,
	focustime datetime
);

select * from focus;

SELECT * FROM `focus` INNER JOIN Goods ON Focus.pro_id = Goods.pro_id  WHERE `user_name` = '15159240619' ;


create table userorder(
	id int not null auto_increment primary key,
	order_id varchar(50) not null unique,
    user_name varchar(20) not null,
    total numeric(7,2),     /*应付金额*/
    pro_count int,          /*数量*/
    address nvarchar(200),  /*收货地址*/
    payway nvarchar(20),    /*付款方式*/
    paystatus nvarchar(3),  /*付款状态*/
    status nvarchar(3)      /*订单状态*/
);


select * from userorder;

delete from userorder where id > 5;

/*alter table userorder modify */

select * from userorder INNER JOIN shoppingcart where userorder.order_id=shoppingcart.order_id;

SELECT * FROM `userorder` INNER JOIN Shoppingcart ON Userorder.order_id = Shoppingcart.order_id GROUP BY Userorder.order_id;

SELECT * FROM `userorder` INNER JOIN Shoppingcart ON Userorder.order_id = Shoppingcart.order_id INNER JOIN Goods ON Goods.pro_id = Shoppingcart.pro_id GROUP BY Userorder.order_id;

SELECT * FROM `userorder` INNER JOIN Shoppingcart ON Userorder.order_id = Shoppingcart.order_id INNER JOIN Goods ON Goods.pro_id = Shoppingcart.pro_id WHERE `status` = '已取消' GROUP BY Userorder.order_id;


/*drop table userorder;*/

create table allorder(
	order_id varchar(50) not null primary key,
    delivery_date datetime
);


select * from allorder;

drop database fruit;
/**/