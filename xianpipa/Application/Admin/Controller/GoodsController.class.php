<?php
namespace Admin\Controller;
use Think\Controller;
class GoodsController extends Controller{
    public function publish(){
        $this->display();
    }
    public function sell(){
        $where['pro_status'] = 1;
        $goodslist = D('Goods') -> where($where) -> select();
        $this -> assign('goodslist',$goodslist);
        $this->display();
    }
    public function downoprate(){
    	$data = I('post.');
		$where['pro_id'] = $data['goodsid'];
		$update['pro_status'] = 0;
		$res = D('Goods') -> where($where) -> save($update);
		if($res){
        	$status = 1;
        }else{
            $status = 0;
        }
        $this -> ajaxReturn($status);
    	header("Location:" . U("/Admin/Goods/depot"));
    }
    public function depot(){
    	/*仓库中的宝贝*/
    	if(IS_POST){
    		$data = I('post.');
    		$where['pro_id'] = $data['goodsid'];
    		$res = D('Goods') -> where($where) -> delete();
    		if($res){
            	$status = 1;
	        }else{
	            $status = 0;
	        }
	        $this -> ajaxReturn($status);
    	}else{
	        $goodslist = D('Goods') -> select();
	        $this -> assign('goodslist',$goodslist);
	        $this->display();
	    }
    }
}