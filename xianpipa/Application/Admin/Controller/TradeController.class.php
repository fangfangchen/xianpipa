<?php
namespace Admin\Controller;
use Think\Controller;
class TradeController extends Controller {
    public function sell() {
    	/*获取所有的订单*/
        $orderlists = D('Userorder')
                       -> join('Shoppingcart ON Userorder.order_id = Shoppingcart.order_id')
                       -> join('Goods ON Goods.pro_id = Shoppingcart.pro_id')
                       -> group('Userorder.order_id')
                       -> select();
        $this -> assign('orderlists',$orderlists);
        /*等待卖家付款*/
        $wait['status'] = '进行中';
        $waitpaylists = D('Userorder')
                       -> join('Shoppingcart ON Userorder.order_id = Shoppingcart.order_id')
                       -> join('Goods ON Goods.pro_id = Shoppingcart.pro_id')
                       -> where($wait)
                       -> group('Userorder.order_id')
                       -> select();
        $this -> assign('waitpaylists',$waitpaylists);
        /*等待发货*/
        $waitsend['status'] = '等待发货';
        $waitsendlists = D('Userorder')
                       -> join('Shoppingcart ON Userorder.order_id = Shoppingcart.order_id')
                       -> join('Goods ON Goods.pro_id = Shoppingcart.pro_id')
                       -> where($waitsend)
                       -> group('Userorder.order_id')
                       -> select();
        $this -> assign('$waitsendlists',$waitsendlists);
        /*已发货*/
        $send['status'] = '已发货';
        $sendlists = D('Userorder')
                       -> join('Shoppingcart ON Userorder.order_id = Shoppingcart.order_id')
                       -> join('Goods ON Goods.pro_id = Shoppingcart.pro_id')
                       -> where($send)
                       -> group('Userorder.order_id')
                       -> select();
        $this -> assign('sendlists',$sendlists);
        /*退款中*/
        $refound['status'] = '退款中';
        $refoundlists = D('Userorder')
                       -> join('Shoppingcart ON Userorder.order_id = Shoppingcart.order_id')
                       -> join('Goods ON Goods.pro_id = Shoppingcart.pro_id')
                       -> where($refound)
                       -> group('Userorder.order_id')
                       -> select();
        $this -> assign('refoundlists',$refoundlists);
        /*成功的订单*/
        $sucess['status'] = '完成';
        $sucesslists = D('Userorder')
                       -> join('Shoppingcart ON Userorder.order_id = Shoppingcart.order_id')
                       -> join('Goods ON Goods.pro_id = Shoppingcart.pro_id')
                       -> where($sucess)
                       -> group('Userorder.order_id')
                       -> select();
        $this -> assign('sucesslists',$sucesslists);
        /*取消的订单*/
        $cancel['status'] = '取消';
        $cancellists = D('Userorder')
                       -> join('Shoppingcart ON Userorder.order_id = Shoppingcart.order_id')
                       -> join('Goods ON Goods.pro_id = Shoppingcart.pro_id')
                       -> where($cancel)
                       -> group('Userorder.order_id')
                       -> select();
        $this -> assign('cancellists',$cancellists);
        $this->display();
    }
    public function assess() {
        $this->display();
    }
}