<?php
namespace Admin\Controller;
use Think\Controller;
class IndexController extends Controller {
    // public function upload(){
    //     $upload = new \Think\Upload();// 实例化上传类
    //     $upload -> maxSize   =     3145728 ;// 设置附件上传大小
    //     $upload -> exts      =     array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
    //     $upload -> rootPath  =      './Public/images/'; // 设置附件上传根目录
    //     $upload -> savePath  =      ''; // 设置附件上传（子）目录
    //     //设置文件上传名(按照时间)  
    //     $upload -> saveRule = "time";  
    //     // 上传文件 
    //     $info   =   $upload->upload();
    //     if(!$info) {// 上传错误提示错误信息
    //         $this -> error($upload->getError());
    //     }else{// 上传成功 获取上传文件信息
    //         foreach($info as $file){
    //             echo $file['savepath'].$file['savename'];
    //         }
    //     }
    //     $savename = $info[0]['savename'];  
    //     $imgurl = $file['savepath'].$file['savename'];
    //     return $imgurl;
    // }

    public function updatePwd(){
        $data = I('post.');
        $updatapwd['ad_pwd'] = $data['newpwd'];
        $where['ad_name'] = $data['adminname'];
        $result = D('Administrator') -> where($where) -> save($updatapwd);
        if($result){
            $status = 1;
        }else{
            $status = 0;
        }
        $this -> ajaxReturn($status);
        header('location:' . U('Admin/Inde/index'));
    }

    public function index(){
    	$this -> assign('title', '后台首页 - 天天鲜果');
        if(IS_POST){
            // $data = I('post.');
            // $data_arr['pro_id'] = time();                 //编号
            // $data_arr['pro_name'] = $data['goodstitle'];  //名称
            // $data_arr['kind_no'] = $data['special'];      //类别
            // $data_arr['sup_no'] = $data['produce'];       //产家
            // $data_arr['pro_price'] = $data['price'];      //原价
            // $data_arr['pro_disprice'] = $data['price1'];   //折扣价
            // $data_arr['gstorage'] = $data['storage'];     //库存
            // $data_arr['pro_weight'] = $data['weight'];    //规格
            // $data_arr['pro_img'] = $data['imgfile'];      //图片
            // $data_arr['pro_content'] = $data['content'];  //描述
            // D('Goods') -> add($data_arr);

            // $data = I('post.');
            $data_arr['pro_id'] = time();                  //编号
            $data_arr['pro_name'] = $_POST['title'];       //名称
            if($_POST['special'] == "鲜枇杷"){
                $special = 1;
            }else if($_POST['special'] == "枇杷罐头"){
                $special = 2;
            }else if($_POST['special'] == "枇杷膏"){
                $special = 3;
            }
            if($_POST['produce'] == '福建'){
                $produce = "151101";
            }else if($_POST['produce'] == '四川'){
                $produce = "151102";
            }
            $data_arr['kind_no'] = $special;               //类别
            $data_arr['sup_no'] = $produce;                //产家
            $data_arr['pro_price'] = $_POST['price'];      //原价
            $data_arr['pro_disprice'] = $_POST['price1'];  //折扣价
            $data_arr['gstorage'] = $_POST['storage'];     //库存
            $data_arr['pro_weight'] = $_POST['weight'];    //规格
            $data_arr['pro_status'] = 1;                   //状态

            $upload = new \Think\Upload();// 实例化上传类
            $upload -> maxSize   =     3145728 ;// 设置附件上传大小
            $upload -> exts      =     array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
            $upload -> rootPath  =      './Public/images/'; // 设置附件上传根目录
            $upload -> savePath  =      ''; // 设置附件上传（子）目录
            //设置文件上传名(按照时间)
            $upload -> saveRule = "time";  
            // 上传文件 
            $info   =   $upload -> upload();
            if(!$info) {// 上传错误提示错误信息
                $this -> error($upload->getError());
            }else{// 上传成功 获取上传文件信息
                // foreach($info as $file){
                //     echo $file['savepath'].$file['savename'];
                // }
            }
            // $savename = $info[0]['savename'];
            foreach($info as $file){
                $imgurl = $imgurl . ',' . $file['savepath'].$file['savename'];
            }
            // $imgurl = $file['savepath'].$file['savename'];
            foreach($info as $file){
                $image = new \Think\Image();
                $image->open('./Public/images/' . $file['savepath'].$file['savename']);
                // 生成一个固定大小的缩略图并保存
                $saveurl = $file['savepath'].'1_'.$file['savename'];
                $image->thumb(370, 370,\Think\Image::IMAGE_THUMB_FIXED)->save('./Public/images/' . $saveurl);
                $saveurl1 = $file['savepath'].'2_'.$file['savename'];
                $image->thumb(740, 740,\Think\Image::IMAGE_THUMB_FIXED)->save('./Public/images/' . $saveurl1);
            }
            $data_arr['pro_img'] = $imgurl;//"default.png";         //图片
            $data_arr['pro_content'] = $_POST['content'];  //描述
            if(D('Goods') -> add($data_arr)){ //写入数据库成功
                // $res = "恭喜您，发布成功！";
                $this -> redirect("index","",2,"发布成功！两秒后跳回");
                // header("Location:".U("/Admin/Index/index"));
            }else{
                $res = "不好意思，发布失败！";
            }
            // $this -> ajaxReturn($res);
        }else{
    	    $this->display();
        }
    }
	
    public function login(){
    	$this->assign('title', '后台登录 - 天天鲜果');
    	if(IS_POST){
            $data = I("post.");
            $where['ad_name'] = $data['username'];
            $where['ad_pwd'] = $data['pwd'];
            $result = D('Administrator') -> where($where) -> select();
            if($result){
                $status = 1;
                session('adminname',$data["username"]);
                $this -> ajaxReturn($status);
            }else{
                $str = "用户名或者密码出错~请重新输入!";
                $this -> ajaxReturn($str);
            }
        }else{
            $this->display();
        }
    }

    public function logout(){
        session('adminname',null);
        header("Location:".U("/Admin/Index/login"));
    }
}