<?php
namespace Home\Controller;
use Think\Controller;
class UserController extends Controller {
    public function date(){
        $today = date('Y-m-d');
        $this -> assign('today',$today);
    }
    public function index(){
        $this -> date();
    	$this -> assign('title','天天鲜果');
        $this -> display();
    }
    public function login(){
    	$this -> assign('title','欢迎登录-天天鲜果');
        if(IS_POST){
            $data = I("post.");
            $info = D('User') -> login($data);
            if($info['status'] == 1){
                $this -> date();
                // $this -> assign('loginname', $data['username']);
                /*获取购物车中的商品数量*/
                $thiscon['user_name'] = session('user_name');
                $lasttime = D('User') -> where($thiscon) -> getField('last_time');
                session('lasttime',$lasttime);
                $data_time['last_time'] = date('Y-m-d H:i:s');
                D('User') -> where($thiscon) -> save($data_time); // 根据条件更新记录
                // echo D('User') -> getLastSql();
                $thiscon['current_status'] = 0;
                $goodsnum = D('Shoppingcart') -> where($thiscon) -> sum('add_count');
                $goodsnum = $goodsnum  | 0;
                session('goodsnum',$goodsnum);
            }
            $this -> ajaxReturn($info);
        }else{
            $this -> display();
        }
    }
    public function register(){
        $this -> date();
    	$this -> assign('title','欢迎注册-天天鲜果');
        if(IS_POST){
            $data = I("post.");
            $this -> ajaxReturn(D('User') -> register($data));            
        }else{
            $this -> display();
        }
    }
    public function exitthis(){
        session("user_name",null);
        session("goodsnum",0);
        session('totalprice',0);
        header("Location:".U("/Home/Index/index"));
    }
    /*添加新的收货地址*/
    public function addaddress(){
        $data = I('post.');
        /*新的收货地址写入数据库*/
        $updateadd['user_name'] = session('user_name');
        $updateadd['receive'] = $data['address'];
        $addresscon['user_name'] = session('user_name');
        $curaddress = D('Useraddress') -> where($addresscon) -> getField('receive');
        if($curaddress != $data['address']){
            $res = D('Useraddress') -> add($updateadd);
            if($res){
                $status = 1;
            }else{
                $status = 0;
            }
        }else{
            $status = 0;
        }
        $this -> ajaxReturn($status);
        header("Location:".U("/Home/User/myfruit"));
    }
    /*修改收货地址*/
    public function updateaddress(){
        $data = I('post.');
        $oldaddress['user_name'] = session('user_name');
        $oldaddress['receive'] = $data['oldaddress'];
        $addressid['id'] = D('Useraddress') -> where($oldaddress) -> getField('id');

        $updateadd['user_name'] = session('user_name');
        $updateadd['receive'] = $data['address'];
        $res = D('Useraddress') -> where($addressid) -> save($updateadd);
        $this -> ajaxReturn($status);
        header("Location:".U("/Home/User/myfruit"));
    }
    /*删除收货地址*/
    public function deleteaddress(){
        $data = I('post.');
        $where['user_name'] = $data['username'];
        $where['receive'] = $data['address'];
        $result = D('Useraddress') -> where($where) -> delete();
        if($result){
            $status = 1;
        }else{
            $status = 0;
        }
        $this -> ajaxReturn($status);
        header("Location:".U("/Home/User/myfruit"));
    }
    public function myfruit(){
        $this -> date();
        $this -> assign('title','我的果园-天天鲜果');
        
        if(IS_POST){
            $data = I('post.');
            $con['order_id'] = $data['orderid'];
            
            $order_data['total'] = D('Userorder') -> where($con) -> getField('total');
            $straddress = D('Userorder') -> where($con) -> getField('address');
            $order_data['payway'] = D('Userorder') -> where($con) -> getField('payway');
            $order_data['paystatus'] = D('Userorder') -> where($con) -> getField('paystatus');
            $order_data['status'] = D('Userorder') -> where($con) -> getField('status');
            $order_data['address'] = explode(',',$straddress);
            
            $where['user_name'] = session('user_name');
            $where['order_id'] = $data['orderid'];
            $prolists = D('Shoppingcart') -> where($where) -> getField('pro_id',true);
            $sumpricelists = D('Shoppingcart') -> where($where) -> getField('sumprice',true);
            $addcountlists = D('Shoppingcart') -> where($where) -> getField('add_count',true);
            $order_data['posttime'] = D('Shoppingcart') -> where($where) -> getField('post_time');
            // echo  D('Shoppingcart') -> getLastSql();
            $order_data['orderlength'] = sizeof($prolists);
            for($i = 0; $i < $order_data['orderlength']; $i++){
                $condition['pro_id'] = $prolists[$i];
                $order_data[$i]['proid'] = $prolists[$i];
                $order_data[$i]['proname'] = D('Goods') -> where($condition) -> getField('pro_name');
                $order_data[$i]['proimg'] = D('Goods') -> where($condition) -> getField('pro_img');
                $order_data[$i]['proweight'] = D('Goods') -> where($condition) -> getField('pro_weight');
                $order_data[$i]['proprice'] = D('Goods') -> where($condition) -> getField('pro_price');                
                $order_data[$i]['prodisprice'] = D('Goods') -> where($condition) -> getField('pro_disprice');
                $order_data[$i]['proaddcount'] = $addcountlists[$i];
                $order_data[$i]['prosumprice'] = $sumpricelists[$i];
            }
            // echo $order_data['orderlength'];
            // echo D('Goods') -> getLastSql();
            // $this -> assign('order_data','$order_data');
            $this -> ajaxReturn($order_data);
        }else{
            $where['user_name'] = session("user_name");
            $logintime = D("User") -> where($where) -> getField("last_time");
            $data_info['logintime'] = $logintime;
            $data_info['username'] = session("user_name");
            $this -> assign('data_info',$data_info);

            /*获取所有的订单*/
            $orderlists = D('Userorder') 
                           -> join('Shoppingcart ON Userorder.order_id = Shoppingcart.order_id')
                           -> join('Goods ON Goods.pro_id = Shoppingcart.pro_id')
                           -> group('Userorder.order_id')
                           -> select();
            $this -> assign('orderlists',$orderlists);
            /*获取完成的订单*/
            $con['status'] = "已完成";
            $finishorder = D('Userorder')
                           -> join('Shoppingcart ON Userorder.order_id = Shoppingcart.order_id')
                           -> join('Goods ON Goods.pro_id = Shoppingcart.pro_id')
                           -> group('Userorder.order_id')
                           -> where($con)
                           -> select();
            $this -> assign('finishorder',$finishorder);
            /*获取取消的订单*/
            $con['status'] = "已取消";
            $cancelorder = D('Userorder')
                           -> join('Shoppingcart ON Userorder.order_id = Shoppingcart.order_id')
                           -> join('Goods ON Goods.pro_id = Shoppingcart.pro_id')
                           -> group('Userorder.order_id')
                           -> where($con)
                           -> select();
            $this -> assign('cancelorder',$cancelorder);
            // /*获取待付款的订单*/
            // $con['paystatus'] = "未付款";
            // $waitorder = D('Userorder')
            //                -> join('Shoppingcart ON Userorder.order_id = Shoppingcart.order_id')
            //                -> join('Goods ON Goods.pro_id = Shoppingcart.pro_id')
            //                -> group('Userorder.order_id')
            //                -> where($con)
            //                -> select();
            // $this -> assign('waitorder',$waitorder);
            // dump($orderlists);
            // echo $orderlists[0]['order_id'];
            // echo D('Userorder') -> getLastSql();
            /*获取关注的列表*/
            $focuscon['user_name'] = session('user_name');
            $focuslist = D('Focus')
                           -> join('Goods ON Focus.pro_id = Goods.pro_id')
                           -> where($focuscon)
                           -> select();
            $this -> assign('focuslist',$focuslist);
            /*获取收获地址列表*/
            $addwhere['user_name'] = session('user_name');
            $addresslist = D('Useraddress') -> where($addwhere) -> select();
            $this -> assign('addresslist',$addresslist);
            $this -> display();
        }
    }
    /*取消关注商品*/
    public function goodsnotfocus(){
        $data = I('post.');
        $whe['user_name'] = session('user_name');
        $whe['pro_id'] = $data['goodsid'];
        $res = D('Focus') -> where($whe) -> delete();
        if($res){
            $status = 1;            
        }else{
            $status = 0;
        }
        $this -> ajaxReturn($status);
        header("Location:" . U("/Home/User/myfruit"));
    }
    public function help(){
        $this -> date();
        $this -> assign('title','帮助中心-天天鲜果');
        $this -> display();
    }
    public function delete(){
        $data = I('post.');
        $where['pro_id'] = $data['goodsid'];
        $where['user_nam'] = session('user_name');
        if(D("Shoppingcart") -> where($where) -> delete()){
            $info['status'] = 1;
        }else{
            $info['status'] = 0;
        }
        /*获取购物车中的商品数量*/
        $thiscon['user_name'] = session('user_name');
        $thiscon['current_status'] = 0;
        $goodsnum = D('Shoppingcart') -> where($thiscon) -> sum('add_count');
        $goodsnum = $goodsnum ? $goodsnum : 0;
        session('goodsnum',$goodsnum);
        $info['goodsnum'] = $goodsnum;
        $this -> ajaxReturn($info);
        header("Location:" . U('Home/User/cart'));
    }
    // public function getdetail(){
    //     /*浏览记录*/
    //     $data = I('post.');
    //     $delete['user_name'] = $data['username'];
    //     $delete['pro_id'] = $data['goodsid'];
    //     D('Browse') ->where($delete) -> delete();
    //     $insert['user_name'] = $data['username'];
    //     $insert['pro_id'] = $data['goodsid'];
    //     $insert['browse'] = time();
    //     D('Browse') -> add($insert);
    //     /*浏览次数+1*/
    //     $con['pro_id'] = $data['goodsid'];
    //     $curbrowse = D('Goods') -> where($con) -> getField('pro_browse');
    //     // echo D('Goods') -> getLastSql();
    //     $update['pro_browse'] = $curbrowse + 1;
    //     D("Goods") -> where($con) -> save($update);
    //     header("Location:" . U("/Home/Index/detail"));
    // }
    public function detail(){
        $this -> date();
        $this -> assign('title','鲜果区-天天鲜果');
        // if(IS_POST){
        //     $data = I('post.');
        //     // $goodid = D('Shoppingcart') -> GoodsNum($data);
        //     /*获取购物车中的商品数量*/
        //     $thiscon['user_name'] = $data['username'];
        //     $thiscon['current_status'] = 0;
        //     $goodsnum = D('Shoppingcart') -> where($thiscon) -> sum('add_count');
        //     session('goodsnum',$goodsnum);
        //     /*获取购物车中的所有商品的总价格*/
        //     $totalprice = D('Shoppingcart')
        //                 -> where($thiscon)
        //                 -> sum('sumprice');
        //     $totalprice = $totalprice?$totalprice : 0.0;
        //     session('totalprice',$totalprice);

        //     $where['pro_id'] = $data['goodsid'];
        //     $detail = D('Goods') -> where($where) -> find();
        //     // $detail_arr['goodsid'] = $detail['pro_id'];
        //     // echo D('Goods') -> getLastSql();
        //     // echo $detail['pro_name'];
        //     // $this -> assign('detail',$detail);
            

        //     /*返回数组数据*/
        //     $something['goodsnum'] = $goodsnum;
        //     $something['totalprice'] = $totalprice;
        //     $this -> ajaxReturn($something);
            
        // }else{            
            $where['user_name'] = session('user_name');
            $maxbrowse = D('Browse')
                    -> where($where)
                    -> max('browse');
            $con['browse'] = $maxbrowse;
            $goodsname = D('Browse')
                    -> join('Goods ON Browse.pro_id = Goods.pro_id')
                    -> where($con) 
                    -> getField("pro_name");
            $goodsid = D('Browse') -> where($con) -> getField("pro_id");
            $weight = D('Browse')
                    -> join('Goods ON Browse.pro_id = Goods.pro_id')
                    -> where($con) 
                    -> getField("pro_weight");
            $price = D('Browse')
                    -> join('Goods ON Browse.pro_id = Goods.pro_id')
                    -> where($con) 
                    -> getField("pro_price");
            $disprice = D('Browse')
                    -> join('Goods ON Browse.pro_id = Goods.pro_id')
                    -> where($con) 
                    -> getField("pro_disprice");
            $storage = D('Browse')
                    -> join('Goods ON Browse.pro_id = Goods.pro_id')
                    -> where($con) 
                    -> getField("gstorage");
            $content = D('Browse')
                    -> join('Goods ON Browse.pro_id = Goods.pro_id')
                    -> where($con) 
                    -> getField("pro_content");
            $detail['goodsname'] = $goodsname;
            $detail['goodsid'] = $goodsid;
            $detail['weight'] = $weight;
            $detail['price'] = $price;
            $detail['disprice'] = $disprice;
            $detail['storage'] = $storage;
            $detail['content'] = $content;

            $focuscon['user_name'] = session('user_name');
            $focuscon['pro_id'] = $goodsid;
            $result = D('Focus') -> where($focuscon) -> select();
            if($result){
                $detail['focus'] = true;
            }
            
            // echo D('Browse') -> getLastSql();
            $this -> assign("detail",$detail);

            /*最近浏览列表*/
            $lastlist = D('Browse') 
                        -> join('Goods ON Browse.pro_id = Goods.pro_id')
                        -> where($where) 
                        -> getLastList(6);
            $this -> assign('lastlist',$lastlist);

            $this -> display();
        // }
        }
    /*购物车*/
    public function cart(){
        $this -> date();
        /*鲜果列表*/
        $where['user_name'] = session('user_name');
        $where['current_status'] = 0;
        $cartlist = D('Goods')
                    -> join('Shoppingcart ON Shoppingcart.pro_id = Goods.pro_id')
                    -> where($where)
                    -> group('Goods.pro_id')
                    -> select();
        $this -> assign('cartlist',$cartlist);
        $condition['user_name'] = session('user_name');
        $condition['current_status'] = 0;
        $totalprice = D('Shoppingcart')
                        -> where($condition)
                        -> sum('sumprice');
        $totalprice = $totalprice ? $totalprice : 0.0;
        session('totalprice',$totalprice);
        // echo  D('Goods') -> getLastSql();
        /*最近浏览列表*/
        $con['user_name'] = session('user_name');
        $lastlist = D('Browse') 
                    -> join('Goods ON Browse.pro_id = Goods.pro_id')
                    -> where($con) 
                    -> getLastList(6);
        $this -> assign('lastlist',$lastlist);
        $this -> assign('title','我的购物车-我的果园');
        if(IS_POST){
            $data = I('post.');
            $where['user_name'] = session('user_name');
            $where['pro_id'] = $data['goodsid'];
            $data_arr['add_count'] = $data['goodsnum'];
            $data_arr['sumprice'] = $data['sumprice'];
            D('Shoppingcart') -> where($where) -> save($data_arr);

            /*获取购物车中的商品数量*/
            $thiscon['user_name'] = $data['username'];
            $thiscon['current_status'] = 0;
            $goodsnum = D('Shoppingcart') -> where($thiscon) -> sum('add_count');
            session('goodsnum',$goodsnum);
            $this -> ajaxReturn($goodsnum);
        }else{
            $this -> display();
        }
    }
    /*提交订单*/
    public function check(){        
        $this -> assign('title','订单信息-我的果园');
        $addwhere['user_name'] = session('user_name');
        $addresslist = D('Useraddress') -> where($addwhere) -> select();
        $this -> assign('addresslist',$addresslist);
        /*鲜果列表*/
        $where['user_name'] = session('user_name');
        $where['current_status'] = 0;
        $cartlist = D('Goods')
                    -> join('Shoppingcart ON Shoppingcart.pro_id = Goods.pro_id')
                    -> where($where)
                    -> group('Goods.pro_id')
                    -> select();
        $this -> assign('cartlist',$cartlist);
        $condition['user_name'] = session('user_name');
        $condition['current_status'] = 0;
        $totalprice = D('Shoppingcart')
                        -> where($condition)
                        -> sum('sumprice');
        $totalprice = $totalprice ? $totalprice : 0.0;
        session('totalprice',$totalprice);
        // echo  D('Goods') -> getLastSql();

        /*获取购物车中的商品数量*/
        $thiscon['user_name'] = session('user_name');
        $thiscon['current_status'] = 0;
        $goodsnum = D('Shoppingcart') -> where($thiscon) -> sum('add_count');
        $goodsnum = $goodsnum ? $goodsnum : 0;
        session('goodsnum',$goodsnum);

        if(IS_POST){
            /*提交购物车，修改状态*/
            $thiscon['user_name'] = session('user_name');
            $thiscon['current_status'] = 0;
            $update['current_status'] = 1;
            $update['order_id'] = strval(rand(10,99)) . strval(rand(10,99)) . strval(time());
            $update['post_time'] = date('Y-m-d H:i:s');
            $update['current_status'] = 1;
            $goodsnum = D('Shoppingcart') -> where($thiscon) -> save($update);
            // session('goodsnum',0);

            $data = I('post.');
            $whe['id'] = D('Shoppingcart') -> max(id);
            $con1['order_id'] = D('Shoppingcart') -> where($whe) -> getField('order_id');
            // echo D('Shoppingcart') -> getLastSql();
            $con1['user_name'] = session('user_name');
            $total = D('Shoppingcart') -> where($con1) -> sum('sumprice');
            $count = D('Shoppingcart') -> where($con1) -> sum('add_count');
            /*修改库存量*/
            $goodsid = D('Shoppingcart') -> where($con1) -> getField('pro_id');
            $updategoods['gstorage'] = D('Goods') -> where("pro_id = $goodsid") -> getField('gstorage');
            $updategoods['gstorage'] = $updategoods['gstorage'] - $count;
            D('Goods') -> where("pro_id = $goodsid") -> save($updategoods);

            $updateinfo['order_id'] = $con1['order_id'];
            $updateinfo['user_name'] = session('user_name');
            $updateinfo['total'] = $total;
            $updateinfo['pro_count'] = $count;
            $updateinfo['paystatus'] = '未付款';
            $updateinfo['status'] = '进行中';
            $updateinfo['address'] = $data['address'];
            $updateinfo['payway'] = $data['payway'];
            session('payway',$data['payway']);
            /*新的收货地址写入数据库*/
            $updateadd['user_name'] = session('user_name');
            $updateadd['receive'] = $data['address'];
            $addresscon['user_name'] = session('user_name');
            $curaddress = D('Useraddress') -> where($addresscon) -> getField('receive');
            if($curaddress != $data['address']){
                D('Useraddress') -> add($updateadd);
            }
            // dump($curaddress);
            $result = D('Userorder') -> add($updateinfo);
            if($result){
                $info['status'] = 1;
                session('ordertotal',$total);
                session('orderid',$con1['order_id']);
            }else{
                $info['status'] = 0;
            }
            $this -> ajaxReturn($info);
        }else{
            $this -> display();
        }
    }
    /*立即支付*/
    public function pay(){
        $condition['user_name'] = session('user_name');
        $condition['current_status'] = 0;
        $totalprice = D('Shoppingcart')
                        -> where($condition)
                        -> sum('sumprice');
        $totalprice = $totalprice?$totalprice : 0.0;
        session('totalprice',$totalprice);
        // echo  D('Goods') -> getLastSql();

        /*获取购物车中的商品数量*/
        $thiscon['user_name'] = session('user_name');
        $thiscon['current_status'] = 0;
        $goodsnum = D('Shoppingcart') -> where($thiscon) -> sum('add_count');
        $goodsnum = $goodsnum ? $goodsnum : 0;
        session('goodsnum',$goodsnum);

        // $this -> assign('cartlist',$cartlist);
        $this -> assign('title','立即支付-我的果园');
        $this -> display();
    }
}