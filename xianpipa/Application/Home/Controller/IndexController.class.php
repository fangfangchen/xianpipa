<?php
namespace Home\Controller;
use Think\Controller;
class IndexController extends Controller {
    public function date(){
        $today = date('Y-m-d');
        $this -> assign('today',$today);
    }
    public function getGoodsNum(){
        $userid = session('user_id');
        $where['user_name'] = $user_id;
        $goodsnum = D('Shoppingcart') -> where($where) -> count('pro_id');
        dump($userid . ' ' . $goodsnum);
    }
    public function index(){
        $this -> date();
        $this -> assign('title','天天鲜果-欢迎您');
        /*鲜果推荐列表*/
        $whe['kind_no'] = 1;
        $whe['pro_status'] = 1;
        $recommendlist = D('Goods') -> where($whe) -> getGoodsList(8);
        $this -> assign('recommendlist',$recommendlist);
        /*送礼佳品列表*/
        $con['kind_no'] = array('gt',1);
        $con['pro_status'] = 1;
        $goodslist = D('Goods') -> where($con) -> getGoodsList(4);
        $this -> assign('goodslist',$goodslist);
        /*热销商品列表*/
        $condition['pro_status'] = 1;
        $hotlist = D('Goods') -> where($condition) -> getHotlist();
        $this -> assign('hotlist',$hotlist);

        /*获取购物车中的商品数量*/
        $goodsnum = session('goodsnum') ? session('goodsnum') : 0;
        $this -> assign('goodsnum',$goodsnum);

        if(IS_POST){
            $data = I('post.');
            $this -> ajaxReturn($data['thisid']);
        }else{
            $this -> display();
        }        
    }
    public function choseProduce($supname){
        $this -> date();
        /*最近浏览列表*/
        $con['user_name'] = session('user_name');
        $lastlist = D('Browse') 
                    -> join('Goods ON Browse.pro_id = Goods.pro_id')
                    -> where($con) 
                    -> getLastList(6);
        $this -> assign('lastlist',$lastlist);
        $this -> assign('title','天天鲜果-鲜果区');
        /*鲜果列表*/
        $where['sup_name'] = $supname;
        $condition['pro_status'] = 1;
        $condition['sup_no'] = D('Produce') -> where($where) -> getField('sup_no');
        $choselist = D('Goods') -> where($condition) -> getGoodsList(15);
        // echo D('Goods') -> getLastSql();
        $this -> assign('choselist',$choselist);
        $this -> display();
    }
    public function fjproduce(){
        $this -> choseProduce('福建');
    }
    public function scproduce(){
        $this -> choseProduce('四川');
    }
    public function fruit(){
        $this -> date();
        /*鲜果列表,默认排列*/
        $whe['kind_no'] = 1;
        $whe['pro_status'] = 1;
        $fruitlist = D('Goods') -> where($whe) -> getGoodsList(15);
        $this -> assign('fruitlist',$fruitlist);
        /*鲜果列表,按低到高排列*/
        $fruitlist1 = D('Goods') -> where($whe) -> order('pro_disprice asc') -> getGoodsList(15);
        $this -> assign('fruitlist1',$fruitlist1);
        /*鲜果列表,按高到低排列*/
        $fruitlist2 = D('Goods') -> where($whe) -> order('pro_disprice desc') -> getGoodsList(15);
        $this -> assign('fruitlist2',$fruitlist2);
        /*最近浏览列表*/
        $con['user_name'] = session('user_name');
        $lastlist = D('Browse') 
                    -> join('Goods ON Browse.pro_id = Goods.pro_id')
                    -> where($con) 
                    -> getLastList(6);
        $this -> assign('lastlist',$lastlist);
        $this -> assign('title','天天鲜果-鲜果区');
        if(IS_POST){
            $data = I('post.');
            // $goodid = D('Shoppingcart') -> GoodsNum($data);
            /*添加的时候是否存在，存在就更新，不存在就插入新的数据*/
            $where['pro_id'] = $data['goodsid'];
            $where['user_name'] = $data['username'];
            $condition['pro_id'] = $data['goodsid'];
            $condition['user_name'] = $data['username'];
            $condition['current_status'] = 0;
            $count = D('Shoppingcart') -> where($condition) -> getField('add_count');
            if($count > 0){
                $update['add_count'] = $count + $data['pronum'];
                $disprice = D('Goods')
                        -> where("pro_id='{$data['goodsid']}'")
                        -> getField('pro_disprice');
                $sumprice = D('Shoppingcart')
                        -> where("pro_id='{$data['goodsid']}'")
                        -> getField('sumprice');
                // echo D('Goods') -> getLastSql();
                $update['sumprice'] = $sumprice + $disprice * $data['pronum'];
                D('Shoppingcart') -> where($condition) -> save($update);
            }else{
                $data_arr['user_name'] = $data['username'];
                $data_arr['pro_id'] = $data['goodsid'];
                $data_arr['add_time'] = date('Y-m-d H:i:s');
                $data_arr['current_status'] = 0;
                $data_arr['add_count'] = $data['pronum'];
                $price = D('Goods')
                        -> where("pro_id='{$data['goodsid']}'")
                        -> getField('pro_disprice');
                $data_arr['sumprice'] = $price * $data['pronum'];
                D('Shoppingcart') -> add($data_arr);
            }
            /*获取购物车中的商品数量*/
            $thiscon['user_name'] = $data['username'];
            $thiscon['current_status'] = 0;
            $goodsnum = D('Shoppingcart') -> where($thiscon) -> sum('add_count');
            session('goodsnum',$goodsnum);
            /*获取购物车中的所有商品的总价格*/
            $totalprice = D('Shoppingcart')
                        -> where($thiscon)
                        -> sum('sumprice');
            $totalprice = $totalprice ? $totalprice : 0.0;
            session('totalprice',$totalprice);
            /*返回数组数据*/
            $something['goodsnum'] = $goodsnum;
            $something['totalprice'] = $totalprice;
            $this -> ajaxReturn($something);
        }else{          
            $this -> display();
        }
    }
    public function gift(){
        $this -> date();
        /*礼品列表*/
        $whe['kind_no'] = array('gt',1);
        $whe['pro_status'] = 1;
        $giftlist = D('Goods') -> where($whe) -> getGoodsList(15);
        $this -> assign('giftlist',$giftlist);
        /*鲜果列表,按低到高排列*/
        $giftlist1 = D('Goods') -> where($whe) -> order('pro_disprice asc') -> getGoodsList(15);
        $this -> assign('giftlist1',$giftlist1);
        /*鲜果列表,按高到低排列*/
        $giftlist2 = D('Goods') -> where($whe) -> order('pro_disprice desc') -> getGoodsList(15);
        $this -> assign('giftlist2',$giftlist2);
        $this -> assign('title','天天鲜果-礼品区');
        /*最近浏览列表*/
        $con['user_name'] = session('user_name');
        $lastlist = D('Browse')
                    -> join('Goods ON Browse.pro_id = Goods.pro_id')
                    -> where($con) 
                    -> getLastList(6);
        $this -> assign('lastlist',$lastlist);
        if(IS_POST){
            $data = I('post.');
            // $goodid = D('Shoppingcart') -> GoodsNum($data);
            /*添加的时候是否存在，存在就更新，不存在就插入新的数据*/
            $where['pro_id'] = $data['goodsid'];
            $where['user_name'] = $data['username'];
            $condition['pro_id'] = $data['goodsid'];
            $condition['user_name'] = $data['username'];
            $condition['current_status'] = 0;
            $count = D('Shoppingcart') -> where($condition) -> getField('add_count');
            if($count > 0){
                $update['add_count'] = $count + 1;
                $disprice = D('Goods')
                        -> where("pro_id='{$data['goodsid']}'")
                        -> getField('pro_disprice');
                $sumprice = D('Shoppingcart')
                        -> where("pro_id='{$data['goodsid']}'")
                        -> getField('sumprice');
                $update['sumprice'] = $disprice * $data['pronum'] + $sumprice;
                D('Shoppingcart') -> where($condition) -> save($update);
            }else{
                $data_arr['user_name'] = $data['username'];
                $data_arr['pro_id'] = $data['goodsid'];
                $data_arr['add_time'] = date('Y-m-d H:i:s');
                $data_arr['current_status'] = 0;
                $data_arr['add_count'] = $data['pronum'];
                $price = D('Goods')
                        -> where("pro_id='{$data['goodsid']}'")
                        -> getField('pro_disprice');
                $data_arr['sumprice'] = $price * $data['pronum'];
                D('Shoppingcart') -> add($data_arr);
            }
            /*获取购物车中的商品数量*/
            $thiscon['user_name'] = $data['username'];
            $thiscon['current_status'] = 0;
            $goodsnum = D('Shoppingcart') -> where($thiscon) -> sum('add_count');
            session('goodsnum',$goodsnum);
            /*获取购物车中的所有商品的总价格*/
            $totalprice = D('Shoppingcart')
                        -> where($thiscon)
                        -> sum('sumprice');
            $totalprice = $totalprice ? $totalprice : 0.0;
            session('totalprice',$totalprice);
            /*返回数组数据*/
            $something['goodsnum'] = $goodsnum;
            $something['totalprice'] = $totalprice;
            $this -> ajaxReturn($something);
        }else{          
            $this -> display();
        }
    }
    public function knowledge(){
        $this -> date();
        $this -> assign('title','天天鲜果-果食');
        $this -> display();
    }
    public function getdetail(){
        /*浏览记录*/
        $data = I('post.');
        if($data['username']){
            $delete['user_name'] = $data['username'];
            $delete['pro_id'] = $data['goodsid'];
            D('Browse') ->where($delete) -> delete();
            $insert['user_name'] = $data['username'];
            $insert['pro_id'] = $data['goodsid'];
            $insert['browse'] = time();
            D('Browse') -> add($insert);
            /*浏览次数+1*/
            $con['pro_id'] = $data['goodsid'];
            $curbrowse = D('Goods') -> where($con) -> getField('pro_browse');
            // echo D('Goods') -> getLastSql();
            $update['pro_browse'] = $curbrowse + 1;
            D("Goods") -> where($con) -> save($update);
        }else{
            session('goodsid',$data['goodsid']);
        }
        header("Location:" . U("/Home/Index/detail"));
    }
    /*关注商品*/
    public function goodsfocus(){
        $data = I('post.');
        $whe['user_name'] = session('user_name');
        $whe['pro_id'] = $data['goodsid'];
        $res = D('Focus') -> where($whe) -> select();
        if(!$res){
            $update['user_name'] = session('user_name');
            $update['pro_id'] = $data['goodsid'];
            $update['focustime'] = date('Y-m-d H:i:s');
            D('Focus') -> add($update);
        }
        header("Location:" . U("/Home/Index/detail"));
    }
    /*取消关注商品*/
    public function goodsnotfocus(){
        $data = I('post.');
        $whe['user_name'] = session('user_name');
        $whe['pro_id'] = $data['goodsid'];
        $res = D('Focus') -> where($whe) -> delete();
        header("Location:" . U("/Home/Index/detail"));
    }
    public function detail(){
        $this -> date();
        $this -> assign('title','鲜果区-天天鲜果');
        // if(IS_POST){
        //     $data = I('post.');
        //     // $goodid = D('Shoppingcart') -> GoodsNum($data);
        //     /*获取购物车中的商品数量*/
        //     $thiscon['user_name'] = $data['username'];
        //     $thiscon['current_status'] = 0;
        //     $goodsnum = D('Shoppingcart') -> where($thiscon) -> sum('add_count');
        //     session('goodsnum',$goodsnum);
        //     /*获取购物车中的所有商品的总价格*/
        //     $totalprice = D('Shoppingcart')
        //                 -> where($thiscon)
        //                 -> sum('sumprice');
        //     $totalprice = $totalprice ? $totalprice : 0.0;
        //     session('totalprice',$totalprice);

        //     $where['pro_id'] = $data['goodsid'];
        //     $detail = D('Goods') -> where($where) -> find();
        //     // $detail_arr['goodsid'] = $detail['pro_id'];
        //     // echo D('Goods') -> getLastSql();
        //     // echo $detail['pro_name'];
        //     // $this -> assign('detail',$detail);
            

        //     /*返回数组数据*/
        //     $something['goodsnum'] = $goodsnum;
        //     $something['totalprice'] = $totalprice;
        //     $this -> ajaxReturn($something);
            
        // }else{
        if(session('user_name')){
            $where['user_name'] = session('user_name');
            $maxbrowse = D('Browse')
                    -> where($where)
                    -> max('browse');
            $con['browse'] = $maxbrowse;
            $goodsname = D('Browse')
                    -> join('Goods ON Browse.pro_id = Goods.pro_id')
                    -> where($con) 
                    -> getField("pro_name");
            $goodsid = D('Browse') -> where($con) -> getField("pro_id");
            // $weight = D('Browse')
            //         -> join('Goods ON Browse.pro_id = Goods.pro_id')
            //         -> where($con) 
            //         -> getField("pro_weight");
            // $price = D('Browse')
            //         -> join('Goods ON Browse.pro_id = Goods.pro_id')
            //         -> where($con) 
            //         -> getField("pro_price");
            // $disprice = D('Browse')
            //         -> join('Goods ON Browse.pro_id = Goods.pro_id')
            //         -> where($con) 
            //         -> getField("pro_disprice");
            // $storage = D('Browse')
            //         -> join('Goods ON Browse.pro_id = Goods.pro_id')
            //         -> where($con) 
            //         -> getField("gstorage");
            // $goodsdetail = D('Browse')
            //         -> join('Goods ON Browse.pro_id = Goods.pro_id')
            //         -> where($con) 
            //         -> getField("pro_img");
            // $content = D('Browse')
            //         -> join('Goods ON Browse.pro_id = Goods.pro_id')
            //         -> where($con) 
            //         -> getField("pro_content");
            $con['pro_id'] = $goodsid;
            $goodsdetail = D('Goods') -> where($con) -> find();
            $detail['goodsname'] = $goodsdetail['pro_name'];
            $detail['goodsid'] = $goodsdetail['pro_id'];
            $detail['weight'] = $goodsdetail['pro_weight'];
            $detail['price'] = $goodsdetail['pro_price'];
            $detail['disprice'] = $goodsdetail['pro_disprice'];
            $detail['storage'] = $goodsdetail['gstorage'];
            $detail['detailimg'] = $goodsdetail['pro_img'];
            $detail['content'] = $goodsdetail['pro_content'];
            // $detail['goodsname'] = $goodsname;
            // $detail['goodsid'] = $goodsid;
            // $detail['weight'] = $weight;
            // $detail['price'] = $price;
            // $detail['disprice'] = $disprice;
            // $detail['storage'] = $storage;
            // $detail['detailimg'] = $goodsdetail['pro_img'];
            // $detail['content'] = $content;

            $focuscon['user_name'] = session('user_name');
            $focuscon['pro_id'] = $goodsid;
            $result = D('Focus') -> where($focuscon) -> select();
            if($result){
                $detail['focus'] = true;
            }
            // echo D('Browse') -> getLastSql();
            $this -> assign("detail",$detail);

            /*最近浏览列表*/
            $lastlist = D('Browse') 
                        -> join('Goods ON Browse.pro_id = Goods.pro_id')
                        -> where($where) 
                        -> getLastList(6);
            $this -> assign('lastlist',$lastlist);
        }else{
            $con['pro_id'] = session('goodsid');
            $goodsdetail = D('Goods') -> where($con) -> find();
            $detail['goodsname'] = $goodsdetail['pro_name'];
            $detail['goodsid'] = $goodsdetail['pro_id'];
            $detail['weight'] = $goodsdetail['pro_weight'];
            $detail['price'] = $goodsdetail['pro_price'];
            $detail['disprice'] = $goodsdetail['pro_disprice'];
            $detail['storage'] = $goodsdetail['gstorage'];
            $detail['detailimg'] = $goodsdetail['pro_img'];
            $detail['content'] = $goodsdetail['pro_content'];
            $detail['focus'] = false;
            $this -> assign("detail",$detail);
        }
        $this -> display();
        // }
    }
}