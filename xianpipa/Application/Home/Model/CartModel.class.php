<?php
namespace Home\Model;
use Think\Model;
class CartModel extends Model {
    public function getLastList($limit){
        $page = I('p',1,'int');
        $data = $this -> page($page,$limit) -> order('browse desc') -> select();
        //$data = $this -> page($_GET['p'].',3') -> select(); //全部显示出来，因为$_GET['p']没有获取到
        $count = $this -> count();
        $Page = new \Think\Page($count,$limit);
        $show = $Page -> show();
        return array('list' => $data,'page' => $show);
    }
}