<?php
namespace Home\Model;
use Think\Model;
class GoodsModel extends Model {
    //自动验证
    protected $_validate = array(
        array('username','require','验证码必须！'), 
        array('name','','帐号名称已经存在！',0,'unique',1), 
        array('value',array(1,2,3),'值的范围不正确！',2,'in'), 
        array('repassword','password','确认密码不正确',0,'confirm'), 
        array('password','checkPwd','密码格式不正确',0,'function'), 
   );

    //字段验证
    protected $_map = array(
        'username' =>'user_name', // 把表单中name映射到数据表的username字段
        'password'  =>'user_pwd', // 把表单中的mail映射到数据表的email字段
    );

    //自动完成
    protected $_auto = array ( 
        array('user_pwd','md5',3,'function') , // 对password字段在新增和编辑的时候使md5函数处理
    );

    public function getGoodsList($limit){
        $page = I('p',1,'int');
        $data = $this -> page($page,$limit) -> select();
        //$data = $this -> page($_GET['p'].',3') -> select(); //全部显示出来，因为$_GET['p']没有获取到
        $count = $this -> count();
        $Page = new \Think\Page($count,$limit);
        $show = $Page -> show();
        return array('list' => $data,'page' => $show);
    }

    public function getHotlist(){
        $page = I('p',1,'int');
        $data = $this -> order('pro_sell desc') -> page($page,6) -> select();
        //$data = $this -> page($_GET['p'].',3') -> select(); //全部显示出来，因为$_GET['p']没有获取到
        $count = $this -> count();
        $Page = new \Think\Page($count,6);
        $show = $Page -> show();
        return array('list' => $data,'page' => $show);
    }
}