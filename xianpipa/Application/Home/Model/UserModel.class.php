<?php
namespace Home\Model;
use Think\Model;
class UserModel extends Model {
    //自动验证
    protected $_validate = array(
        array('username','require','验证码必须！'), 
        array('name','','帐号名称已经存在！',0,'unique',1), 
        array('value',array(1,2,3),'值的范围不正确！',2,'in'), 
        array('repassword','password','确认密码不正确',0,'confirm'), 
        array('password','checkPwd','密码格式不正确',0,'function'), 
   );

    //字段验证
    protected $_map = array(
        'username' =>'user_name', // 把表单中name映射到数据表的username字段
        'password'  =>'user_pwd', // 把表单中的mail映射到数据表的email字段
    );

    //自动完成
    protected $_auto = array ( 
        array('user_pwd','md5',3,'function') , // 对password字段在新增和编辑的时候使md5函数处理
    );
    
    public function login($data){
        $info = array();
        $User = M('User');
        $where['user_name'] = $data['username'];
        $where['user_pwd'] = md5($data['password']);  //md5加密解密
        $list = $User -> where($where) -> select();
        $time = date('Y-m-d H:i:s');
        if(!$list){
            $info = array(
                'status' => 0,
                'info' => '该用户不存在或者密码错误！'
            );
            return $info;
        }else{
            // $data_time['last_time'] = $time;
            // $where['user_name'] = $data['username'];
            // $User -> where($where) -> save($data_time); // 根据条件更新记录
            $info = array(
                'status' => 1,
                'info' => '登录成功！'
            );
            session('lasttime',date('Y-m-d H:i:s'));               //登录时间
            session('user_name',$data['username']);  //登录成功就写入session
            return $info;
        }
    }

    public function register($data){
        $info = array();
        $data_arr['user_name'] = $data['username'];
        $data_arr['user_pwd'] = md5($data['password']);  //md5加密解密
        $data_arr['last_time'] = date('Y-m-d H:i:s');    //首次注册未登录时间
        $User = M('User');
        $where['user_name'] = $data['username'];
        // $where['user_pwd'] = $data['password'];
        $User -> create(); // 生成数据对象
        $list = $User -> where($where) -> select();
        if(!$list){
            if($User -> add($data_arr) > 0){
                $info = array(
                    'status' => 1,
                    'info' => '恭喜您，注册成功！'
                );
                return $info;
            }else {
                $info = array(
                    'status' => 0,
                    'info' => '不好意思，注册失败，请再试一次！'
                );
                return $info;
            }            
        }else{
            $info = array(
                'status' => 0,
                'info' => '该用户已存在，请直接登录！'
            );
            return $info;
        }
    }

    public function getUserList(){
        $page = I('p',1,'int');
        $data = $this -> page($page,3) -> select();
        //$data = $this -> page($_GET['p'].',3') -> select(); //全部显示出来，因为$_GET['p']没有获取到
        $count = $this -> count();
        $Page = new \Think\Page($count,3);
        $show = $Page -> show();
        return array('list' => $data,'page' => $show);
    }
}