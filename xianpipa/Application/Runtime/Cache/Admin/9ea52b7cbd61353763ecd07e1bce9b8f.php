<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<body>
	<p class="crumbs">宝贝管理&gt;&gt;发布宝贝</p>
	<form class="publish-form" action="<?php echo U('Admin/Index/index');?>" enctype="multipart/form-data" method="post">
		<div class="row">
			<label class="label">商品名称</label>
			<input type="text" id="title" name="title" placeholder="例：福建莆田早钟枇杷" value="四川晚钟枇杷">
			<label class="error"><img src="/xianpipa/Public/images/unchecked.gif" alt="错误" />标题长度3~30个汉字</label>
		</div>
		<div class="row">
			<label class="label">商品类别</label>
			<select id="special" name="special">
				<option data-id="none">请选择</option>
				<option data-id="1">鲜枇杷</option>
				<option data-id="2">枇杷罐头</option>
				<option data-id="3">枇杷膏</option>
			</select>
			<label class="error"><img src="/xianpipa/Public/images/unchecked.gif" alt="错误" />请选择商品所属的类别</label>
		</div>
		<div class="row">
			<label class="label">商品产地</label>
			<input type="text" id="produce" name="produce" placeholder="例：福建或者四川"  value="四川">
			<label class="error"><img src="/xianpipa/Public/images/unchecked.gif" alt="错误" />产地长度2~6个汉字</label>
		</div>
		<div class="row">
			<label class="label">商品原价</label>
			<input type="text" id="price" name="price" placeholder="例：30" value="12">
			<span class="unit">元</span>
			<label class="error"><img src="/xianpipa/Public/images/unchecked.gif" alt="错误" />价格在0.01-1亿元之间，且最多2位小数</label>
		</div>
		<div class="row">
			<label class="label">商品折后价</label>
			<input type="text" id="price1" name="price1" placeholder="例：12" value="12">
			<span class="unit">元</span>
			<label class="error"><img src="/xianpipa/Public/images/unchecked.gif" alt="错误" />价格在0.01-1亿元之间，且最多2位小数</label>
		</div>
		<div class="row">
			<label class="label">商品规格</label>
			<input type="text" id="weight" name="weight" placeholder="例：1斤装或者10斤/箱" value="1">
			<label class="error"><img src="/xianpipa/Public/images/unchecked.gif" alt="错误" />请输入整数，比如：3</label>
		</div>
		<div class="row">
			<label class="label">库存</label>
			<input type="text" id="storage" name="storage" placeholder="例：10" value="1">
			<label class="error"><img src="/xianpipa/Public/images/unchecked.gif" alt="错误" />请输入整数，比如：3</label>
		</div>
		<!-- <div class="row">
			<label class="label">运费</label>
			<input type="text" id="cost">
			<span class="unit">元</span>
			<label class="error"><img src="/xianpipa/Public/images/unchecked.gif" alt="错误" />运费填写有错，请重新输入</label>
		</div> -->
		<!-- 展示图 一张 -->
		<div class="row">
			<label class="label fleft">宝贝展示图片</label>
			<!-- <img src="/xianpipa/Public/images/img.png" alt="添加图片" /> -->
			<div class="fleft" id="imgPreview" style='width:100px; height:120px;'>
			    <img src="/xianpipa/Public/images/img.png" width="100" height="120" />
			</div>
			<label class="imgtips">上传展示图，规格为200*180像素大小，图片每张最大3M</label><br />
		</div>
		<div class="row">
			<!-- <form action="<?php echo U('Admin/Index/upload');?>" enctype="multipart/form-data" method="post">-->
				<label class="label"></label>
				<!-- <input type="text" name="name"> -->
				<input id="imgfile" type="file" name="photo[]" onchange='PreviewImage(this,"imgPreview")'><br />
				<label class="label"></label>	
				<label class="error" id="imgerror"></label>
				<!-- <input type="submit" value="上传"> 
			</form>-->
		</div>
		<!-- 详情图 至少1张 至多4张 no.1-->
		<div class="row">
			<label class="label fleft">宝贝详情图片</label>
			<!-- <img src="/xianpipa/Public/images/img.png" alt="添加图片" /> -->
			<div class="fleft" id="imgPreview1" style='width:100px; height:120px;'>
			    <img src="/xianpipa/Public/images/img.png" width="100" height="120" />
			</div>
			<label class="imgtips">上传详情图，至少1张最多4张，每张规格85*85像素大小，图片每张最大3M</label><br />
		</div>
		<div class="row">
			<!-- <form action="<?php echo U('Admin/Index/upload');?>" enctype="multipart/form-data" method="post">-->
				<label class="label"></label>
				<!-- <input type="text" name="name"> -->
				<input id="imgfile1" type="file" name="photo[]" onchange='PreviewImage(this,"imgPreview1")'><br />	
				<label class="error" id="imgerror"></label>
				<!-- <input type="submit" value="上传"> 
			</form>-->
		</div>
		<div>
		<!-- no.2 -->
		<div class="row">
			<label class="label fleft"></label>
			<div class="fleft" id="imgPreview2" style='width:100px; height:120px;'>
			    <img src="/xianpipa/Public/images/img.png" width="100" height="120" />
			</div>
			<label class="imgtips">详情图片第二张</label><br />
		</div>
		<div class="row">
				<label class="label"></label>
				<input id="imgfile2" type="file" name="photo[]" onchange='PreviewImage(this,"imgPreview2")'><br />	
				<label class="error" id="imgerror"></label>
		</div>
		<!-- no.3 -->
		<div class="row">
			<label class="label fleft"></label>
			<div class="fleft" id="imgPreview3" style='width:100px; height:120px;'>
			    <img src="/xianpipa/Public/images/img.png" width="100" height="120" />
			</div>
			<label class="imgtips">详情图片第三张</label><br />
		</div>
		<div class="row">
			<label class="label"></label>
			<input id="imgfile3" type="file" name="photo[]" onchange='PreviewImage(this,"imgPreview3")'><br />	
			<label class="error" id="imgerror"></label>
		</div>
		<!-- no.4 -->
		<div class="row">
			<label class="label fleft"></label>
			<div class="fleft" id="imgPreview4" style='width:100px; height:120px;'>
			    <img src="/xianpipa/Public/images/img.png" width="100" height="120" />
			</div>
			<label class="imgtips">详情图片第四张</label><br />
		</div>
		<div class="row">
			<label class="label"></label>
			<input id="imgfile4" type="file" name="photo[]" onchange='PreviewImage(this,"imgPreview4")'><br />	
			<label class="error" id="imgerror"></label>
		</div>
		<div class="row">
			<label class="label">宝贝描述</label>
			<textarea cols="50" rows="10" id="content" name="content" placeholder="请输入宝贝的详情描述"></textarea>
		</div>
		<div class="row"><input class="publishnow" type="submit" value="立刻发布"></div>
	</form>
	<script type="text/javascript">
	/*名称判断*/
	$("#title").keyup(function(){
		var reg = /^[\u4e00-\u9fa5]{3,30}$/gi;
		if(!reg.test($("#title").val())){
			$(this).parent('.row').children('.error').css({"display":"inline-block"});
		}else{
			$(this).parent('.row').children('.error').css({"display":"none"});
		}
	});
	/*产地判断*/
	$("#produce").keyup(function(){
		var reg = /^[\u4e00-\u9fa5]{2,6}$/gi;
		if(!reg.test($("#produce").val())){
			$(this).parent('.row').children('.error').css({"display":"inline-block"});
		}else{
			$(this).parent('.row').children('.error').css({"display":"none"});
		}
	});
	/*价格判断*/
	$("#price").keyup(function(){
		var reg = /^(([0-9]{1,8}(\.[0-9]{1,2})?)|[1-9]|10{1,8})$/;
		if(!reg.test($("#price").val())){
			$(this).parent('.row').children('.error').css({"display":"inline-block"});
		}else{
			$(this).parent('.row').children('.error').css({"display":"none"});
		}
	});
	$("#price1").keyup(function(){
		var reg = /^(([0-9]{1,8}(\.[0-9]{1,2})?)|[1-9]|10{1,8})$/;
		if(!reg.test($("#price1").val())){
			$(this).parent('.row').children('.error').css({"display":"inline-block"});
		}else{
			$(this).parent('.row').children('.error').css({"display":"none"});
		}
	});
	/*运费判断*/
	// $("#cost").keyup(function(){
	// 	var reg = /^[1-9]\d*$/;
	// 	if(!reg.test($("#cost").val())){
	// 		$(this).parent('.row').children('.error').css({"display":"inline-block"});
	// 	}else{
	// 		$(this).parent('.row').children('.error').css({"display":"none"});
	// 	}
	// });
	/*库存判断*/
	$("#storage").keyup(function(){
		var reg = /^[1-9]\d*$/;
		if(!reg.test($("#storage").val())){
			$(this).parent('.row').children('.error').css({"display":"inline-block"});
		}else{
			$(this).parent('.row').children('.error').css({"display":"none"});
		}
	});

	/*立即发布*/
	$('.publish-form').submit(function(){
		/*名称判断*/
		var reg = /^[\u4e00-\u9fa5]{3,30}$/gi;
		if(!reg.test($("#title").val())){
			$("#title").parent('.row').children('.error').css({"display":"inline-block"});
			return false;
		}else{
			$("#title").parent('.row').children('.error').css({"display":"none"});
		}
		/*类别判断*/
		if($("#special").val() == "请选择"){
			$("#special").parent('.row').children('.error').css({"display":"inline-block"});
			return false;
		}else{
			$("#special").parent('.row').children('.error').css({"display":"none"});
		}
		/*产地判断*/
		var reg = /^[\u4e00-\u9fa5]{2,6}$/gi;
		if(!reg.test($("#produce").val())){
			$("#produce").parent('.row').children('.error').css({"display":"inline-block"});
			return false;
		}else{
			$("#produce").parent('.row').children('.error').css({"display":"none"});
		}
		/*价格判断*/
		var reg = /^(([0-9]{1,8}(\.[0-9]{1,2})?)|[1-9]|10{1,8})$/;
		if(!reg.test($("#price").val())){
			$("#price").parent('.row').children('.error').css({"display":"inline-block"});
			return false;
		}else{
			$("#price").parent('.row').children('.error').css({"display":"none"});
		}
		/*价格判断*/
		var reg = /^(([0-9]{1,8}(\.[0-9]{1,2})?)|[1-9]|10{1,8})$/;
		if(!reg.test($("#price1").val())){
			$("#price1").parent('.row').children('.error').css({"display":"inline-block"});
			return false;
		}else{
			$("#price1").parent('.row').children('.error').css({"display":"none"});
		}
		/*库存判断*/
		var reg = /^[1-9]\d*$/;
		if(!reg.test($("#storage").val())){
			$("#storage").parent('.row').children('.error').css({"display":"inline-block"});
			return false;
		}else{
			$("#storage").parent('.row').children('.error').css({"display":"none"});
		}

		/*文件判断*/
		if($('#imgfile').val().length < 1){
			$('#imgerror').css({"display":"inline-block"}).html('<img src="/xianpipa/Public/images/unchecked.gif" alt="错误" />' + "宝贝图片至少上传一张！");
			return false;
		}else{
			$('#imgerror').html(" ");
			var x = "";
	        var y = document.getElementById("imgfile");
	        if (!x || !y.value || !y) return;
	        var pt = /\.jpg$|\.gif$|\.png$|\.jpeg$/i;
	        if (pt.test(y.value)){
	        	x = "file://" + y.value;
	        }else {
	            document.getElementById("imgfile").value = "";
	            $('#imgerror').css({"display":"inline-block"}).html('<img src="/xianpipa/Public/images/unchecked.gif" alt="错误" />' + "对不起，系统仅支持.jpg .gif .jpeg .png标准格式的照片，请您调整格式后重新上传，谢谢 !");
	            return false;
	        }
		}
		alert('恭喜您，发布成功！');
		// $action = $('.publish-form').attr('action');
		// $goodstitle = $('#title').val();          //商品名称
		// $specialcon = $('#special').val();         //商品类别
		// if($specialcon == "鲜枇杷"){
		// 	$special = 1;
		// }else if($specialcon == "枇杷罐头"){
		// 	$special = 2;
		// }else if($specialcon == "枇杷膏"){
		// 	$special = 3;
		// }
		// $producecon = $('#produce').val();		   //商品产地
		// if($producecon.indexOf('福建') != -1){
		// 	$produce = "151101";
		// }else if($producecon.indexOf('四川') != -1){
		// 	$produce = "151102";
		// }
		// $price = parseFloat($('#price').val());    //商品原价
		// $price1 = parseFloat($('#price1').val());  //商品折后价
		// $weight = $('#weight').val();              //商品规格
		// $storage = $('#storage').val();            //商品库存
		// $imgfile = $('#imgfile').val();            //商品图片
		// $content = $('#content').val();            //商品描述
		// // alert("action:" + $action + "\ngoodstitle:" + $goodstitle + "\nspecial:" + $special + "\nproduce:" + $produce + "\nprice:" + $price + "\nprice1:" + $price1 + "\nweight:" + $weight + "\nstorage:" + $storage + "\nimgfile:" + $imgfile + "\ncontent:" + $content);
		// $.post($action,{goodstitle:$goodstitle, special:$special, produce:$produce, price:$price, price1:$price1, weight:$weight, storage:$storage, imgfile:$imgfile, content:$content},function(data){
		// 	alert(data);
		// });
	});
	//图片预览
	function PreviewImage(imgFile,thisid)
	{
		if($('#imgfile').val().length < 1){
			$('#imgerror').css({"display":"inline-block"}).html('<img src="/xianpipa/Public/images/unchecked.gif" alt="错误" />' + "宝贝图片至少上传一张！");
			return false;
		}
	    var filextension = imgFile.value.substring(imgFile.value.lastIndexOf("."),imgFile.value.length);
	    filextension = filextension.toLowerCase();
	    if ((filextension!='.jpg')&&(filextension!='.gif')&&(filextension!='.jpeg')&&(filextension!='.png')){
	        $('#imgerror').css({"display":"inline-block"}).html('<img src="/xianpipa/Public/images/unchecked.gif" alt="错误" />' + "对不起，系统仅支持.jpg .gif .jpeg .png标准格式的照片，请您调整格式后重新上传，谢谢 !");
	    	document.getElementById(thisid).html('<img src="/xianpipa/Public/images/img.png" width="100" height="120" />');
	        imgFile.focus();
	        return false;
	    }else
	    {
	        var path;
	        if(document.all)//IE
	        {
	            imgFile.select();
	            path = document.selection.createRange().text;
	            document.getElementById(thisid).innerHTML = "";
	            document.getElementById(thisid).style.filter = "progid:DXImageTransform.Microsoft.AlphaImageLoader(enabled='true',sizingMethod='scale',src=\"" + path + "\")";//使用滤镜效果  
	            $('#imgerror').html(" ");
	        }
	        else//FF
	        {
	            path=window.URL.createObjectURL(imgFile.files[0]);// FF 7.0以上
	            //path = imgFile.files[0].getAsDataURL();// FF 3.0
	            document.getElementById(thisid).innerHTML = "<img width='100px' height='120px' src='"+path+"'/>";
	            $('#imgerror').html(" ");
	            //document.getElementById("img1").src = path;
	        }
	    }
	}
	</script>
</body>
</html>