<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
	<title><?php echo ($title); ?></title>
	<meta http-equiv="Content-Type" content="text/html;charset=utf-8"/>
	<link rel="shortcut icon" href="/xianpipa/Public/images/1.ico" />
	<link rel="stylesheet" type="text/css" href="/xianpipa/Public/css/dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/xianpipa/Public/css/admin.css">
</head>
<body>
	<div class="content">
		<div class="form loginform">
			<div class="logo"><img src="/xianpipa/Public/images/logo.png" alt="logo图片" /></div>
			<form class="form-horizontal signinform" action="<?php echo U('Admin/Index/login');?>" method="post">
				<div class="form-group">
					<label class="control-label col-xs-3">用户名：</label>
					<div class="col-xs-9"><input class="form-control username" type="text" maxlength="50" name="username" placeholder="请输入用户名" value="123"></div>
				</div>
				<div class="form-group">
					<label class="control-label col-xs-3">密&nbsp;&nbsp;&nbsp;码：</label>
					<div class="col-xs-9"><input class="form-control pwd" type="password" maxlength="20" name="pwd" placeholder="请输入密码" value="admin"></div>
				</div>
				<div class="form-group">
					<label class="control-label" name="error"></label>
				</div>
				<div class="form-group txtcenter" id="p_button">
					<div class="col-xs-4 col-xs-offset-4"><input class="form-control" type="button" value="登录" id="signbtn"></div>
					<a href="#">忘记密码？</a>
				</div>
				<div class="form-group txtcenter">
					<label class="control-label" id="error" name="error" style="text-align:center;display:none;"></label>
				</div>
			</form>
		</div>
	</div>
	<script type="text/javascript" src="/xianpipa/Public/js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$('#signbtn').click(function(){
			var $name = $('.username').val(),
				$pwd = $('.pwd').val(),
				$action = $('.signinform').attr('action');
			// alert($name + " " + $pwd + " " + $action);
			$.post($action,{username:$name, pwd:$pwd},function(data){
				if(data == 1){
					window.location.href="<?php echo U('Admin/Index/index');?>"; 
				}else{
					$('#error').css({"display":"block"}).empty().html('<img src="/xianpipa/Public/images/unchecked.gif" alt="错误" />' + data);
				}
			});
		});
	});
	</script>
</body>
</html>