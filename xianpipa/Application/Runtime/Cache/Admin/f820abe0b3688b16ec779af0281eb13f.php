<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<body>
	<p class="crumbs">宝贝管理&gt;&gt;仓库中的宝贝</p>
	<div class="goodsblock">
		<table class="table">
			<thead>
				<th class="width5">选择</th>
				<th class="width5">宝贝编号</th>
				<th class="width12">宝贝</th>
				<th class="width12">原价(元)</th>
				<th class="width12">单价(元)</th>
				<th class="width12">库存</th>
				<th class="width12">规格</th>
				<th class="width12">描述</th>
				<th class="width12">操作</th>
			</thead>
			<tbody>
				<?php if(is_array($goodslist)): $i = 0; $__LIST__ = $goodslist;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$all): $mod = ($i % 2 );++$i;?><tr>
						<td class="width5"><input type="checkbox" name="lastthree"></td>
						<td class="width5"><?php echo ($all["pro_id"]); ?></td>
						<td class="width12"><img src="/xianpipa/Public/images/<?php echo explode(',', $all['pro_img'])[1];?>" alt="枇杷" style="width:80px;height:70px;" /><br /><?php echo ($all["pro_name"]); ?></td>
						<td class="width12">￥<?php echo ($all["pro_price"]); ?></td>
						<td class="width12">￥<?php echo ($all["pro_disprice"]); ?></td>
						<td class="width12"><?php echo ($all["gstorage"]); ?></td>
						<td class="width12"><?php echo ($all["pro_weight"]); ?></td>
						<td class="width12"><?php echo ($all["pro_content"]); ?></td>
						<td class="width12"><a class="deleteop" data-id="<?php echo ($all["pro_id"]); ?>" data-url="<?php echo U('Admin/Goods/depot');?>">删除</a>&nbsp;&nbsp;|&nbsp;&nbsp;<a class="downop" data-id="<?php echo ($all["pro_id"]); ?>" data-url="<?php echo U('Admin/Goods/downoprate');?>"><?php if($all['pro_status'] == 0){echo '已下架';}else{echo '下架';} ?></a></td>
					</tr><?php endforeach; endif; else: echo "" ;endif; ?>
			</tbody>
		</table>
		<div class="operate">
			<input class="selectall" type="checkbox" name="selectall" onclick="selectall(this.checked,'lastthree');">全选
			<a href="#">批量删除</a>
			<a href="#">批量下架</a>
		</div>
	</div>
	<script type="text/javascript">
	/*全选操作*/
	function selectall(checked,name){
		var qx = document.getElementsByName(name);
		for(var i = 0; i < qx.length; i++){
			if(qx[i].type = "checkbox"){
				qx[i].checked = checked;
			}
		}
	}
	/*删除操作*/
	$('.deleteop').click(function(){
		var r = confirm("是否要删除该商品记录？");
		if (r == true){
			$action = $(this).attr('data-url');
			$goodsid = $(this).attr('data-id');
			$.post($action,{goodsid:$goodsid},function(data){
				if(data == 1){
					alert('删除成功！');
					// var pageIndex = $(this).parents('.left-nav').attr("data-id") + '/' + $(this).attr("data-id");
					$("#r-content").load("/xianpipa/index.php/Admin/Goods/depot.html");
				}else if(data == 0){
					alert('删除失败！');
				}
			});
		}
	});
	/*下架操作*/
	$('.downop').click(function(){
		if($(this).text() == '已下架'){
			$('.downop').attr('data-url','');
		}else{
			var r = confirm("是否要下架该商品呢？");
			if (r == true){
				$action = $(this).attr('data-url');
				$goodsid = $(this).attr('data-id');
				$.post($action,{goodsid:$goodsid},function(data){
					if(data == 1){
						alert('下架成功！');
						$("#r-content").load("/xianpipa/index.php/Admin/Goods/depot.html");
					}else if(data == 0){
						alert('下架失败！');
					}
				});
			}
		}
	});
	</script>
</body>
</html>