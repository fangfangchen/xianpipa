<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<body>
	<p class="crumbs">交易管理&gt;&gt;已卖出的宝贝&gt;&gt;<span id="newlocation">最近订单</span></p>
	<ul class="b-menulists">
		<li class="btitle menu-current" data-id="lastthree">最近订单</li>
		<li class="btitle" data-id="waitpay">等待买家付款</li>
		<li class="btitle" data-id="waitsend">等待发货</li>
		<li class="btitle" data-id="sended">已发货</li>
		<li class="btitle" data-id="refund">退款中</li>
		<li class="btitle" data-id="success">成功的订单</li>
		<li class="btitle" data-id="close">关闭的订单</li>
	</ul>
	<!-- 最近订单 -->
	<div class="block bcurrent" id="lastthree">
		<table class="table">
			<thead>
				<th class="width5"></th>
				<th class="width28">宝贝</th>
				<th class="width12">单价(元)</th>
				<th class="width12">数量</th>
				<th class="width12">买家</th>
				<th class="width12">交易状态</th>
				<th class="width12">实收款</th>
			</thead>
			<tbody>
				<?php if(is_array($orderlists)): $i = 0; $__LIST__ = $orderlists;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$all): $mod = ($i % 2 );++$i;?><tr>
						<td class="width5"><input type="checkbox" name="lastthree"></td>
						<td class="width28"><img src="/xianpipa/Public/images/<?php echo explode(',', $all['pro_img'])[1];?>" alt="枇杷" style="width:80px;height:70px;" /><?php echo ($all["pro_name"]); ?></td>
						<td class="width12">￥<?php echo ($all["pro_disprice"]); ?></td>
						<td class="width12"><?php echo ($all["pro_count"]); ?></td>
						<td class="width12"><?php echo ($all["user_name"]); ?></td>
						<td class="width12"><?php echo ($all["status"]); ?></td>
						<td class="width12">￥<?php echo ($all["total"]); ?></td>
					</tr><?php endforeach; endif; else: echo "" ;endif; ?>
			</tbody>
		</table>
		<div class="operate">
			<input class="selectall" type="checkbox" name="selectall" onclick="selectall(this.checked,'lastthree');">全选
			<!-- <a href="#">批量发货</a>
			<a href="#">批量免运费</a> -->
		</div>
	</div>
	<!-- 等待买家付款 -->
	<div class="block" id="waitpay">
		<table class="table">
			<table class="table">
			<thead>
				<th class="width5"></th>
				<th class="width28">宝贝</th>
				<th class="width12">单价(元)</th>
				<th class="width12">数量</th>
				<th class="width12">买家</th>
				<th class="width12">交易状态</th>
				<th class="width12">实收款</th>
			</thead>
			<tbody>
				<?php if(is_array($waitpaylists)): $i = 0; $__LIST__ = $waitpaylists;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$all): $mod = ($i % 2 );++$i;?><tr>
						<td class="width5"><input type="checkbox" name="lastthree"></td>
						<td class="width28"><img src="/xianpipa/Public/images/<?php echo explode(',', $all['pro_img'])[1];?>" alt="枇杷" style="width:80px;height:70px;" /><?php echo ($all["pro_name"]); ?></td>
						<td class="width12">￥<?php echo ($all["pro_disprice"]); ?></td>
						<td class="width12"><?php echo ($all["pro_count"]); ?></td>
						<td class="width12"><?php echo ($all["user_name"]); ?></td>
						<td class="width12"><?php echo ($all["status"]); ?></td>
						<td class="width12">￥<?php echo ($all["total"]); ?></td>
					</tr><?php endforeach; endif; else: echo "" ;endif; ?>
			</tbody>
		</table>
		<div class="operate">
			<input class="selectall" type="checkbox" name="selectall" onclick="selectall(this.checked,'waitpay');">全选
			<!-- <a href="#">批量发货</a>
			<a href="#">批量免运费</a> -->
		</div>
	</div>
	<!-- 等待发货 -->
	<div class="block" id="waitsend">
		<table class="table">
			<thead>
				<th class="width5"></th>
				<th class="width28">宝贝</th>
				<th class="width12">单价(元)</th>
				<th class="width12">数量</th>
				<th class="width12">买家</th>
				<th class="width12">交易状态</th>
				<th class="width12">实收款</th>
			</thead>
			<tbody>
				<?php if(is_array($waitsendlists)): $i = 0; $__LIST__ = $waitsendlists;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$all): $mod = ($i % 2 );++$i;?><tr>
						<td class="width5"><input type="checkbox" name="lastthree"></td>
						<td class="width28"><img src="/xianpipa/Public/images/<?php echo explode(',', $all['pro_img'])[1];?>" alt="枇杷" style="width:80px;height:70px;" /><?php echo ($all["pro_name"]); ?></td>
						<td class="width12">￥<?php echo ($all["pro_disprice"]); ?></td>
						<td class="width12"><?php echo ($all["pro_count"]); ?></td>
						<td class="width12"><?php echo ($all["user_name"]); ?></td>
						<td class="width12"><?php echo ($all["status"]); ?></td>
						<td class="width12">￥<?php echo ($all["total"]); ?></td>
					</tr><?php endforeach; endif; else: echo "" ;endif; ?>
			</tbody>
		</table>
		<div class="operate">
			<input class="selectall" type="checkbox" name="selectall" onclick="selectall(this.checked,'waitsend');">全选
			<a href="#">批量发货</a>
			<a href="#">批量免运费</a>
		</div>
	</div>
	<!-- 已发货 -->
	<div class="block" id="sended">
		<table class="table">
			<table class="table">
			<thead>
				<th class="width5"></th>
				<th class="width28">宝贝</th>
				<th class="width12">单价(元)</th>
				<th class="width12">数量</th>
				<th class="width12">买家</th>
				<th class="width12">交易状态</th>
				<th class="width12">实收款</th>
			</thead>
			<tbody>
				<?php if(is_array($sendlists)): $i = 0; $__LIST__ = $sendlists;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$all): $mod = ($i % 2 );++$i;?><tr>
						<td class="width5"><input type="checkbox" name="lastthree"></td>
						<td class="width28"><img src="/xianpipa/Public/images/<?php echo explode(',', $all['pro_img'])[1];?>" alt="枇杷" style="width:80px;height:70px;" /><?php echo ($all["pro_name"]); ?></td>
						<td class="width12">￥<?php echo ($all["pro_disprice"]); ?></td>
						<td class="width12"><?php echo ($all["pro_count"]); ?></td>
						<td class="width12"><?php echo ($all["user_name"]); ?></td>
						<td class="width12"><?php echo ($all["status"]); ?></td>
						<td class="width12">￥<?php echo ($all["total"]); ?></td>
					</tr><?php endforeach; endif; else: echo "" ;endif; ?>
			</tbody>
		</table>
		<div class="operate">
			<input class="selectall" type="checkbox" name="selectall" onclick="selectall(this.checked,'sended');">全选
			<!-- <a href="#">批量发货</a>
			<a href="#">批量免运费</a> -->
		</div>
	</div>
	<!-- 退款中 -->
	<div class="block" id="refund">
		<table class="table">
			<thead>
				<th class="width5"></th>
				<th class="width28">宝贝</th>
				<th class="width12">单价(元)</th>
				<th class="width12">数量</th>
				<th class="width12">买家</th>
				<th class="width12">交易状态</th>
				<th class="width12">实收款</th>
			</thead>
			<tbody>
				<?php if(is_array($refoundlists)): $i = 0; $__LIST__ = $refoundlists;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$all): $mod = ($i % 2 );++$i;?><tr>
						<td class="width5"><input type="checkbox" name="lastthree"></td>
						<td class="width28"><img src="/xianpipa/Public/images/<?php echo explode(',', $all['pro_img'])[1];?>" alt="枇杷" style="width:80px;height:70px;" /><?php echo ($all["pro_name"]); ?></td>
						<td class="width12">￥<?php echo ($all["pro_disprice"]); ?></td>
						<td class="width12"><?php echo ($all["pro_count"]); ?></td>
						<td class="width12"><?php echo ($all["user_name"]); ?></td>
						<td class="width12"><?php echo ($all["status"]); ?></td>
						<td class="width12">￥<?php echo ($all["total"]); ?></td>
					</tr><?php endforeach; endif; else: echo "" ;endif; ?>
			</tbody>
		</table>
		<div class="operate">
			<input class="selectall" type="checkbox" name="selectall" onclick="selectall(this.checked,'refund');">全选
			<!-- <a href="#">批量发货</a>
			<a href="#">批量免运费</a> -->
		</div>
	</div>
	<!-- 成功的订单 -->
	<div class="block" id="success">
		<table class="table">
			<thead>
				<th class="width5"></th>
				<th class="width28">宝贝</th>
				<th class="width12">单价(元)</th>
				<th class="width12">数量</th>
				<th class="width12">买家</th>
				<th class="width12">交易状态</th>
				<th class="width12">实收款</th>
			</thead>
			<tbody>
				<?php if(is_array($sucesslists)): $i = 0; $__LIST__ = $sucesslists;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$all): $mod = ($i % 2 );++$i;?><tr>
						<td class="width5"><input type="checkbox" name="lastthree"></td>
						<td class="width28"><img src="/xianpipa/Public/images/<?php echo explode(',', $all['pro_img'])[1];?>" alt="枇杷" style="width:80px;height:70px;" /><?php echo ($all["pro_name"]); ?></td>
						<td class="width12">￥<?php echo ($all["pro_disprice"]); ?></td>
						<td class="width12"><?php echo ($all["pro_count"]); ?></td>
						<td class="width12"><?php echo ($all["user_name"]); ?></td>
						<td class="width12"><?php echo ($all["status"]); ?></td>
						<td class="width12">￥<?php echo ($all["total"]); ?></td>
					</tr><?php endforeach; endif; else: echo "" ;endif; ?>
			</tbody>
		</table>
		<div class="operate">
			<input class="selectall" type="checkbox" name="selectall" onclick="selectall(this.checked,'success');">全选
			<!-- <a href="#">批量发货</a>
			<a href="#">批量免运费</a> -->
		</div>
	</div>
	<!-- 关闭的订单 -->
	<div class="block" id="close">
		<table class="table">
			<thead>
				<th class="width5"></th>
				<th class="width28">宝贝</th>
				<th class="width12">单价(元)</th>
				<th class="width12">数量</th>
				<th class="width12">买家</th>
				<th class="width12">交易状态</th>
				<th class="width12">实收款</th>
			</thead>
			<tbody>
				<?php if(is_array($cancellists)): $i = 0; $__LIST__ = $cancellists;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$all): $mod = ($i % 2 );++$i;?><tr>
						<td class="width5"><input type="checkbox" name="lastthree"></td>
						<td class="width28"><img src="/xianpipa/Public/images/<?php echo explode(',', $all['pro_img'])[1];?>" alt="枇杷" style="width:80px;height:70px;" /><?php echo ($all["pro_name"]); ?></td>
						<td class="width12">￥<?php echo ($all["pro_disprice"]); ?></td>
						<td class="width12"><?php echo ($all["pro_count"]); ?></td>
						<td class="width12"><?php echo ($all["user_name"]); ?></td>
						<td class="width12"><?php echo ($all["status"]); ?></td>
						<td class="width12">￥<?php echo ($all["total"]); ?></td>
					</tr><?php endforeach; endif; else: echo "" ;endif; ?>
			</tbody>
		</table>
		<div class="operate">
			<input class="selectall" type="checkbox" name="selectall" onclick="selectall(this.checked,'close');">全选
			<!-- <a href="#">批量发货</a>
			<a href="#">批量免运费</a> -->
		</div>
	</div>
	<script type="text/javascript">
	$(".btitle").click(function(){
		var idIndex = $(this).attr("data-id");
		$('.btitle').removeClass('menu-current');
		$(this).addClass('menu-current');
		$("#newlocation").html($(this).text());
		$('.block').removeClass('bcurrent');
		$('#' + idIndex).addClass("bcurrent");
	});
	/*全选操作*/
	function selectall(checked,name){
		var qx = document.getElementsByName(name);
		for(var i = 0; i < qx.length; i++){
			if(qx[i].type = "checkbox"){
				qx[i].checked = checked;
			}
		}
	}
	</script>
</body>
</html>