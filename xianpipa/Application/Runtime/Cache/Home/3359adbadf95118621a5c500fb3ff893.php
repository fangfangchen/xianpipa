<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
	<title><?php echo ($title); ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
	<link rel="shortcut icon" href="/xianpipa/Public/images/1.ico" />
	<link rel="stylesheet" type="text/css" href="/xianpipa/Public/css/dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/xianpipa/Public/css/user.css">
	<!--[if lt IE 9]>
	<script type="text/javascript">
		location.href = "/xianpipa/index.php/Home/User/ie";
	</script>
	<![endif]-->
</head>
<body>
	<div class="wrapper">
		
	<div class="top-nav">
		<div class="container">
			<div class="tn-left"><p>您好，欢迎来到天天鲜果！<?php echo $today;?></p></div>
			<div class="tn-right">
				<span class="thisname">您好，<span class="loginname"><?php echo session('user_name');?></span></span>
				<a class="loginbtn" href="<?php echo U('Home/User/login');?>">[登录]</a><span class="split">|</span>
				<a href="<?php echo U('Home/Index/index');?>">[首页]</a><span class="split">|</span>
				<a class="myfruit">我的果园</a>
			</div>
		</div>
	</div>
	<div class="user-header">
		<div class="container">
			<a class="fleft" href="<?php echo U('Home/Index/index');?>"><img src="/xianpipa/Public/images/logo.png" alt="logo" /></a>
			<span class="welcome-tips"><span class="split">|</span>欢迎注册</span>
		</div>
	</div>
	<!-- class="user-content" start -->
	<div class="user-content">
		<div class="container">
			<div class="content-left fleft"><img src="/xianpipa/Public/images/registerleft.png" alt="logo" /></div>
			<div class="content-right fright">
				<p class="form-title">注册天天鲜果</p>
				<form class="form-horizontal register-form" action="<?php echo U('Home/User/register');?>" method="post">
					<div class="form-group">
						<label class="control-label col-xs-3">手机/邮箱</label>
						<div class="col-xs-9"><input class="form-control" id="username" name="username" type="text" placeholder="请输入手机或邮箱"></div>
						<label class="control-label usernameerror error"><img src="/xianpipa/Public/images/unchecked.gif" alt="错误" />只能输入手机或邮箱，请重新输入！</label>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3">密码</label>
						<div class="col-xs-9"><input class="form-control" id="password" name="password" type="password" placeholder="请输入密码"></div>
						<label class="control-label pwderror error"><img src="/xianpipa/Public/images/unchecked.gif" alt="错误" />密码请使用数字和字母，长度8~20之间</label>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3">确认密码</label>
						<div class="col-xs-9"><input class="form-control" id="password1" name="password1" type="password" placeholder="请再次输入密码"></div>
						<label class="control-label pwd1error error"><img src="/xianpipa/Public/images/unchecked.gif" alt="错误" />两次密码不一致，请重新输入！</label>
					</div>
					<div class="form-group">
						<button class="btn btn-default register userbtn" id="signup">注册</button>
					</div>
					<div class="form-group txtcenter"><label class="control-label" id="notice"></label></div>
				</form>
			</div>
		</div>
	</div>
	<!-- class="user-content" end -->

		<div class="footer txtcenter">
	<div class="footer-nav">
		<a href="<?php echo U('Home/User/help');?>">友情链接</a>
		<a href="<?php echo U('Home/User/help');?>">关于天天鲜果</a>
		<a href="<?php echo U('Home/User/help');?>">问题与帮助</a>
		<a href="<?php echo U('Home/User/help');?>">联系我们</a>
		<a href="<?php echo U('Admin/Index/login');?>">后台管理</a>
	</div>
	<div class="copyright">
		<p>版权所有 © 2015天天鲜果 保留所有权利 | <a>站长统计</a></p>
		<p>天天鲜果&nbsp;&nbsp;&nbsp;&nbsp;鲜果网购</p>
	</div>
</div>
	</div>
	
	<script type="text/javascript" src="/xianpipa/Public/js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="/xianpipa/Public/js/myjs.js"></script>
	<script type="text/javascript">
	$("#username").blur(function(){
		var reg = /(^(13[0-9]|14[5|7]|15[0|1|2|3|5|6|7|8|9]|18[0|1|2|3|5|6|7|8|9])\d{8}$)|(^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$)/;
		if(!reg.test($("#username").val())){
			$('.usernameerror').css({"display":"inline-block"});
		}else{
			$('.usernameerror').css({"display":"none"});
		}
	});
	$("#password").blur(function(){
		var reg = /^[0-9a-zA-Z]{8,20}$/gi;
		if(!reg.test($("#password").val())){
			$('.pwderror').css({"display":"inline-block"});
		}else{
			$('.pwderror').css({"display":"none"});
		}
	});
	$("#password1").blur(function(){
		if($("#password1").val() !== $("#password").val()){
			$('.pwd1error').css({"display":"inline-block"});
		}else{
			$('.pwd1error').css({"display":"none"});
		}
	});
	$('#signup').click(function(e){
		var reg = /(^(13[0-9]|14[5|7]|15[0|1|2|3|5|6|7|8|9]|18[0|1|2|3|5|6|7|8|9])\d{8}$)|(^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$)/;
		if(!reg.test($("#username").val())){
			$('.usernameerror').css({"display":"inline-block"});
			return false;
		}else{
			$('.usernameerror').css({"display":"none"});
		}
		var reg1 = /^[0-9a-zA-Z]{8,20}$/gi;
		if(!reg1.test($("#password").val())){
			$('.pwderror').css({"display":"inline-block"});
			return false;
		}else{
			$('.pwderror').css({"display":"none"});
		}
		if($("#password1").val() !== $("#password").val()){
			$('.pwd1error').css({"display":"inline-block"});
			return false;
		}else{
			$('.pwd1error').css({"display":"none"});
		}
		$username = $("#username").val();
		$pwd = $("#password").val();
		$action = $('.register-form').attr('action');
		$.post($action, {username:$username, password:$pwd}, function(data){
			if(data.status == 1){
				$('#notice').html(data.info);
				setTimeout(function(){
					window.location.href="<?php echo U('Home/User/login');?>"; 
				},1000);
			}else{
				$('#notice').html('<img src="/xianpipa/Public/images/unchecked.gif" alt="错误" />' + data.info);
			}
		});
		e.preventDefault();
	});
	</script>

</body>
</html>