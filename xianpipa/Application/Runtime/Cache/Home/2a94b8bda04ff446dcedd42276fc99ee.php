<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
	<title><?php echo ($title); ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
	<link rel="shortcut icon" href="/xianpipa/Public/images/1.ico" />
	<link rel="stylesheet" type="text/css" href="/xianpipa/Public/css/dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/xianpipa/Public/css/index.css">
	<!--[if lt IE 9]>
	<script type="text/javascript">
		location.href = "/xianpipa/index.php/Home/User/ie";
	</script>
	<![endif]-->
</head>
<body>
	<div class="wrapper">
		<div class="top-nav">
	<div class="container">
		<div class="tn-left"><p>您好，欢迎来到天天鲜果！<?php echo $today;?></p></div>
		<div class="tn-right">
			<span class="thisname">您好，<span class="loginname"><?php echo session('user_name');?></span><span class="split">|</span></span>
			<a class="loginbtn" href="<?php echo U('Home/User/login');?>">[登录]<span class="split">|</span></a>
			<a class="registerbtn" href="<?php echo U('Home/User/register');?>">[注册]<span class="split">|</span></a>
			<a class="exitbtn" href="<?php echo U('Home/User/exitthis');?>">[退出]<span class="split">|</span></a>
			<a class="myfruit" href="<?php echo U('Home/User/myfruit');?>">我的果园</a>
		</div>
	</div>
</div>
<div class="user-header">
	<div class="container">
		<a class="logo fleft" href="<?php echo U('Home/Index/index');?>"><img src="/xianpipa/Public/images/logo.png" alt="logo" /></a>
		<div class="search fleft"><input class="form-control searchinput fleft" type="text" placeholder="请输入要搜索的内容"><button class="btn btn-default searchbtn fleft">搜索</button></div>
		<a class="fright mycart">
			<img class="myhover" src="/xianpipa/Public/images/hover.png" alt="购物车" />
			<span class="goodsnum"><?php echo session('goodsnum');?></span>
		</a>
	</div>
</div>
<div class="mainnav" data-action="<?php echo U('Home/Index/index');?>">
	<div class="container">
		<a class="current ml100" id="index" href="<?php echo U('Home/Index/index');?>">首页</a>
		<a id="fruit" href="<?php echo U('Home/Index/fruit');?>">鲜果区</a>
		<a id="gift" href="<?php echo U('Home/Index/gift');?>">礼品区</a>
		<a id="knowledge" href="<?php echo U('Home/Index/knowledge');?>">果食</a>
	</div>
</div>
		
	<div class="content">
		<div class="imgshow">
			<div class="container">
				<ul id="banner">
					<li class="current" id="banner01"><img src="/xianpipa/Public/images/banner1.png" alt="" /></li>
					<li id="banner02"><img src="/xianpipa/Public/images/banner2.png" alt="" /></li>
					<li id="banner03"><img src="/xianpipa/Public/images/banner3.png" alt="" /></li>
				</ul>
				<a class="preimg changebtn" id="prebtn"></a>
				<a class="nextimg changebtn" id="nextbtn"></a>
			</div>
		</div>
		<div class="fruit">
			<div class="container">
				<div class="fruit-left fleft">
					<div class="recommend bordertop">
						<div class="blocktop">
							<p class="block-title fleft">鲜果推荐</p>
							<a class="more fright" href="<?php echo U('Home/Index/fruit');?>">查看更多>></a>
						</div>
						<ul class="goodslist">
							<!-- 获取鲜果推荐列表 -->
							<?php if(is_array($recommendlist["list"])): $i = 0; $__LIST__ = $recommendlist["list"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$data): $mod = ($i % 2 );++$i;?><li data-id="<?php echo ($data['pro_id']); ?>">
									<a class="detailbtn" data-href="<?php echo U('Home/Index/getdetail');?>">
										<img src="/xianpipa/Public/images/<?php echo explode(',', $data['pro_img'])[1];?>" alt="枇杷" />
										<p class="goodstitle"><?php echo ($data['pro_name']); ?></p>
										<p><?php echo ($data['pro_weight']); ?></p>
										<p class="price">￥<?php echo ($data['pro_disprice']); ?></p>
									</a>
								</li><?php endforeach; endif; else: echo "" ;endif; ?>
						</ul>
					</div>
					<div class="gift bordertop">
						<div class="blocktop">
							<p class="block-title fleft">送礼佳品</p>
							<a class="more fright" href="<?php echo U('Home/Index/gift');?>">查看更多>></a>
						</div>
						<ul class="goodslist">
							<!-- 获取送礼佳品列表 -->
							<?php if(is_array($goodslist["list"])): $i = 0; $__LIST__ = $goodslist["list"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$data): $mod = ($i % 2 );++$i;?><li data-id="<?php echo ($data['pro_id']); ?>">
									<a class="detailbtn" data-href="<?php echo U('Home/Index/getdetail');?>">
										<img src="/xianpipa/Public/images/<?php echo explode(',', $data['pro_img'])[1];?>" alt="枇杷" />
										<p class="goodstitle"><?php echo ($data['pro_name']); ?></p>
										<p><?php echo ($data['pro_weight']); ?></p>
										<p class="price">￥<?php echo ($data['pro_disprice']); ?></p>
									</a>
								</li><?php endforeach; endif; else: echo "" ;endif; ?>
						</ul>
					</div>
				</div>
				<div class="fruit-right fright">
					<p class="hotgoods">热销商品</p>
					<ul class="hotlists">
						<?php if(is_array($hotlist["list"])): $i = 0; $__LIST__ = $hotlist["list"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$data): $mod = ($i % 2 );++$i;?><li data-id="<?php echo ($data['pro_id']); ?>">
								<a class="detailbtn" data-href="<?php echo U('Home/Index/getdetail');?>">
									<img src="/xianpipa/Public/images/<?php echo explode(',', $data['pro_img'])[1];?>" alt="枇杷" style="width:80px;height:70px;" />
									<div class="hotright fright">
										<p><?php echo ($data['pro_name']); ?></p>
										<p class="hot-price">￥<?php echo ($data['pro_disprice']); ?></p>
									</div>
								</a>
							</li><?php endforeach; endif; else: echo "" ;endif; ?>
					</ul>
				</div>
			</div>
		</div>
		<div class="gotop" id="gotopbtn"><img src="/xianpipa/Public/images/top.png" alt="回到顶部" /></div>
	</div>

		<div class="footer txtcenter">
	<div class="footer-nav">
		<a href="<?php echo U('Home/User/help');?>">友情链接</a>
		<a href="<?php echo U('Home/User/help');?>">关于天天鲜果</a>
		<a href="<?php echo U('Home/User/help');?>">问题与帮助</a>
		<a href="<?php echo U('Home/User/help');?>">联系我们</a>
		<a href="<?php echo U('Admin/Index/login');?>">后台管理</a>
	</div>
	<div class="copyright">
		<p>版权所有 © 2015天天鲜果 保留所有权利 | <a>站长统计</a></p>
		<p>天天鲜果&nbsp;&nbsp;&nbsp;&nbsp;鲜果网购</p>
	</div>
</div>
	</div>
	
	<script type="text/javascript" src="/xianpipa/Public/js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="/xianpipa/Public/js/myjs.js"></script>
	<script type="text/javascript">
	$('#banner,.changebtn').mouseenter(function(){
		$('.changebtn').css({'display':"block"});
	}).mouseleave(function(){
		$('.changebtn').css({'display':"none"});
	});
	var len = $("#banner li").length;
	$('#nextbtn').click(function(){
		var index = ($('#banner .current').attr('id').toString().slice(7) + 1) % len;
		$('#banner .current').fadeOut('slow');
		$('#banner li').removeClass('current');
		if(index == '0'){
			index = len;
		}
		$('#banner0' + index).fadeIn('slow').addClass('current');/*.animate({"left":"0px"},200)*/
	});

	$('#prebtn').click(function(){
		var index = ($('#banner  .current').attr('id').toString().slice(7) - 1) % len;
		 $('.current').fadeOut('slow');
		$('#banner li').removeClass('current');
		if(index == '0'){
			index = len;
		}
		$('#banner0' + index).fadeIn('slow').addClass('current');
	});

	/*商品详细*/
	$('.detailbtn').click(function(e){
		$action = $(this).attr('data-href');
		$goodsid = $(this).parent('li').attr('data-id');
		$username = $('.loginname').text();
		// alert($username + " " + $goodsid);
		$.post($action,{username:$username,goodsid:$goodsid},function(data){
			$('.goodsnum').html(data.goodsnum);
			$('.total').html(data.totalprice);
			$('.hasnum').html(data.goodsnum);
			// $('.goodstitle').html(data.proname);
			location.href = "detail.html";
		});
		e.preventDefault();
	});
	</script>

</body>
</html>