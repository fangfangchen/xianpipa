<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
	<title><?php echo ($title); ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
	<link rel="shortcut icon" href="/xianpipa/Public/images/1.ico" />
	<link rel="stylesheet" type="text/css" href="/xianpipa/Public/css/dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/xianpipa/Public/css/index.css">
	<!--[if lt IE 9]>
	<script type="text/javascript">
		location.href = "/xianpipa/index.php/Home/User/ie";
	</script>
	<![endif]-->
</head>
<body>
	<div class="wrapper">
		<div class="top-nav">
	<div class="container">
		<div class="tn-left"><p>您好，欢迎来到天天鲜果！<?php echo $today;?></p></div>
		<div class="tn-right">
			<span class="thisname">您好，<span class="loginname"><?php echo session('user_name');?></span><span class="split">|</span></span>
			<a class="loginbtn" href="<?php echo U('Home/User/login');?>">[登录]<span class="split">|</span></a>
			<a class="registerbtn" href="<?php echo U('Home/User/register');?>">[注册]<span class="split">|</span></a>
			<a class="exitbtn" href="<?php echo U('Home/User/exitthis');?>">[退出]<span class="split">|</span></a>
			<a class="myfruit" href="<?php echo U('Home/User/myfruit');?>">我的果园</a>
		</div>
	</div>
</div>
<div class="user-header">
	<div class="container">
		<a class="logo fleft" href="<?php echo U('Home/Index/index');?>"><img src="/xianpipa/Public/images/logo.png" alt="logo" /></a>
		<a class="fright mycart">
			<img class="myhover" src="/xianpipa/Public/images/hover.png" alt="购物车" />
			<span class="goodsnum"><?php echo session('goodsnum');?></span>
		</a>
	</div>
</div>
		
	<div class="content">
		<div class="container mycartblock">
			<!-- 填写并核对订单信息 -->
			<div class="checkcart" id="checkinfoblock">
				<div class="current-process">
					<p class="process-title fleft">填写并核对订单信息</p>
					<div class="checkimg processimg fright"></div>
				</div>
				<div class="orangeborder receiveinfo">
					<div style="width: 100%;overflow: hidden;">
						<p class="border-title fleft">收货人信息</p>
						<a class="update fleft" id="updateinfo">[修改]</a>
					</div>
					<ul id="curaddress">
						<?php if(is_array($addresslist)): $i = 0; $__LIST__ = $addresslist;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$addlists): $mod = ($i % 2 );++$i;?><li><p class="have"><?php echo ($addlists['receive']); ?></p></li><?php endforeach; endif; else: echo "" ;endif; ?>
					</ul>					
					<form class="form-horizontal infoform" action="<?php echo U('Home/User/check');?>" method="post">
						<ul id="addradio">
							<?php if(is_array($addresslist)): $i = 0; $__LIST__ = $addresslist;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$addlists): $mod = ($i % 2 );++$i;?><li>
									<div class="radio">
										<label>
											<input type="radio" name="address" value="<?php echo ($addlists['receive']); ?>" checked><?php echo ($addlists['receive']); ?>
										</label>
										&nbsp;&nbsp;<a class="edit">编辑</a>&nbsp;&nbsp;<a class="delete">删除</a>
									</div>
								</li><?php endforeach; endif; else: echo "" ;endif; ?>
						</ul>
						<div class="radio">
							<label>
								<input type="radio" name="address" id="newaddress" value="address2">
								使用新地址
							</label>
						</div>
						<div class="address-block">
							<div class="form-group">
								<label class="form-label col-xs-2"><span class="required">*</span>收货人：</label>
								<div class="col-xs-5"><input class="form-control" id="receive" name="receive" type="text" required palceholder="请输入收货人的名字"></div>
								<!-- <label class="control-label col-xs-5 usernameerror infoerror"><img src="/xianpipa/Public/images/unchecked.gif" alt="错误" />只能输入手机或邮箱，请重新输入！</label> -->
							</div>
							<div class="form-group">
								<label class="form-label col-xs-2"><span class="required">*</span>选择地区：</label>
								<div class="col-xs-5">
									<div class="col-xs-4 nopadding">
										<select class="form-control" id="province">
											<option>省份</option>
											<option>福建</option>
											<option>四川</option>
										</select>
									</div>
									<div class="col-xs-4 nopadding">
										<select class="form-control" id="city">
											<option>市</option>										
										</select>
									</div>
									<div class="col-xs-4 nopadding">
										<select class="form-control" id="county">
											<option>区</option>
										</select>
									</div>
								</div>
								<!-- <label class="control-label col-xs-5 usernameerror infoerror"><img src="/xianpipa/Public/images/unchecked.gif" alt="错误" />只能输入手机或邮箱，请重新输入！</label> -->
							</div>
							<div class="form-group">
								<label class="form-label col-xs-2"><span class="required">*</span>详细地址：</label>
								<div class="col-xs-5"><input class="form-control" id="detailadd" name="detailadd" type="text" required palceholder="请输入详细地址"></div>
								<!-- <label class="control-label col-xs-5 usernameerror infoerror"><img src="/xianpipa/Public/images/unchecked.gif" alt="错误" />只能输入手机或邮箱，请重新输入！</label> -->
							</div>
							<div class="form-group">
								<label class="form-label col-xs-2"><span class="required">*</span>联系方式：</label>
								<div class="col-xs-5"><input class="form-control" id="contact" name="contact" type="text" required palceholder="请输入联系方式"></div>
								<label class="control-label col-xs-5 usernameerror infoerror"><img src="/xianpipa/Public/images/unchecked.gif" alt="错误" />只能输入手机或邮箱，请重新输入！</label>
							</div>
						</div>
						<div class="form-group">
							<a class="btn btn-default save" id="saveinfo">保存收货信息</a>
						</div>
					</form>
				</div>
				<div class="orangeborder payway">
					<div style="width: 100%;overflow: hidden;">
						<p class="border-title fleft">支付方式</p>
						<a class="update fleft" id="updateway">[修改]</a>
					</div>
					<p class="have">支付宝付款</p>
					<form class="form-horizontal payform" action="" method="">
						<div class="way radio">
							<label>
								<input type="radio" name="way" id="way1" value="支付宝" checked>
								<img src="/xianpipa/Public/images/alipay.jpg" alt="支付宝" />
							</label>
						</div>
						<div class="way radio">
							<label>
								<input type="radio" name="way" id="way2" value="中国银行">
								<img src="/xianpipa/Public/images/bank.jpg" alt="中国银行" />
							</label>
						</div>
						<div class="form-group">
							<a class="btn btn-default save" id="saveway">保存支付方式</a>
						</div>
					</form>
				</div>
				<div class="orangeborder">
					<div style="width: 100%;overflow: hidden;">
						<p class="border-title fleft">商品清单</p>
						<a class="backupdate fleft" href="<?php echo U('Home/User/cart');?>">[返回购物车修改]</a>
					</div>
					<table class="table ordertable cartlists">
						<thead>
							<th>商品信息</th>
							<th>规格</th>
							<th>单价(元)</th>
							<th>数量</th>
							<th>优惠</th>
							<th>小计</th>
						</thead>
						<tbody>
							<?php if(is_array($cartlist)): $i = 0; $__LIST__ = $cartlist;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$data): $mod = ($i % 2 );++$i;?><tr>
									<td><img src="/xianpipa/Public/images/<?php echo explode(',', $data['pro_img'])[1];?>" alt="枇杷" style="width: 80px;height 70px;margin-right:10px;" /><?php echo ($data['pro_name']); ?></td>
									<td><?php echo ($data['pro_weight']); ?></td>
									<td><?php echo ($data['pro_price']); ?></td>
									<td><?php echo ($data['add_count']); ?></td>
									<td></td>
									<td><?php echo ($data['sumprice']); ?></td>
								</tr><?php endforeach; endif; else: echo "" ;endif; ?>
						</tbody>
					</table>
					<div class="cart-operate txtright">
						<p>商品件数：<span><?php echo session('goodsnum');?></span>件</p>
						<p>运费：<span>￥0</span></p>
						<p>应付：<span class="orange total">￥<?php echo session('totalprice');?></span></p>
						<p>寄送至：<span id="sendto"></span></p>
						<p>收货人：<span id="receiver"></span></p>
						<p>支付方式：<span id="payway">支付宝付款</span></p>
						<div class="oprea-group">
							<a class="btn btn-default submitorder fright">提交订单</a><!--  data-href="<?php echo U('Home/User/check');?>" -->
						</div>
					</div>
				</div>
			</div>
			</div>
	</div>

		<div class="footer txtcenter">
	<div class="footer-nav">
		<a href="<?php echo U('Home/User/help');?>">友情链接</a>
		<a href="<?php echo U('Home/User/help');?>">关于天天鲜果</a>
		<a href="<?php echo U('Home/User/help');?>">问题与帮助</a>
		<a href="<?php echo U('Home/User/help');?>">联系我们</a>
		<a href="<?php echo U('Admin/Index/login');?>">后台管理</a>
	</div>
	<div class="copyright">
		<p>版权所有 © 2015天天鲜果 保留所有权利 | <a>站长统计</a></p>
		<p>天天鲜果&nbsp;&nbsp;&nbsp;&nbsp;鲜果网购</p>
	</div>
</div>
	</div>
	
	<script type="text/javascript" src="/xianpipa/Public/js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="/xianpipa/Public/js/myjs.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		if($('#curaddress li').length == 1){
			$('.infoform').css({"display":"none"});
			$str = $('#curaddress .have').text();
			$strarray = $str.split(',');
			$('#sendto').html($strarray[1]);
			$('#receiver').html($strarray[0] + " " + $strarray[2]);
			$('.submitorder').bind('click',foo);
		}else{
			$('#curaddress').css({"display":"none"});
			$('#updateinfo').css({"display":"none"}).parents('.orangeborder').css({"border":"2px solid #f60"});
			$('.submitorder').css({'background-color':"#eee",'color':'#aaa'});
			$('.infoform').css({"display":"block"});
			$('#newaddress').checked = true;
		}
		/*选择省市区*/
		$('#province').change(function(){			
			if($('#province').val() == "福建"){
				$('#city').empty().append('<option>厦门</option>');
				$('#county').empty().append('<option>集美区</option>')
							.append('<option>湖里区</option>');
			}else if($('#province').val() == "四川"){
				$('#city').empty().append('<option>成都</option>');
				$('#county').empty().append('<option>锦江区</option>')
							.append('<option>青羊区</option>');
			}else if($('#province').val() == "省份"){
				$('#city').empty().append('<option>市</option>');
				$('#county').empty().append('<option>区</option>');
			}
		});
		/*修改收货信息*/
		$('#updateinfo').click(function(){
			var $thisindex = $(this).parents('.orangeborder');
			$(this).css({"display":"none"});
			$('#curaddress').css({"display":"none"});
			$thisindex.css({"border":"2px solid #f60"});
			$thisindex.children('form').css({"display":"block"});
			$('.submitorder').css({'background-color':"#eee",'color':'#aaa'});
			$('.submitorder').unbind('click',foo);
		});
		$('#saveinfo').click(function(){
			var $receive = $('#receive').val(),
				$province = $('#province').val(),
				$city = $('#city').val(),
				$county = $('#county').val(),
				$detailadd = $('#detailadd').val(),
				$contact = $('#contact').val();
			var $thisindex = $(this).parents('.orangeborder');
			var addradio = document.all("address");
			for(var i = 0; i < addradio.length - 1; i++){
				if(addradio[i].checked){
					$('#curaddress').empty().append('<li><p class="have">' + addradio[i].value + '</p></li>');
				}
			}
			if(document.getElementById('newaddress').checked == true){
				var infostr = $receive + "," + $province + "省" + $city + "市" + $county + $detailadd + "," + $contact;			
				$('.submitorder').css({'background-color':"#f60",'color':'#fff'});
				if($receive != "" && $province != "省份" && $city != "市" && $county != "区" && $detailadd != "" && $contact != ""){
					var addradio = document.all("address");
					for(var i = 0; i < addradio.length; i++){
						if(infostr == addradio[i].value){
							alert('新添加的地址已存在！');
							return false;
						}
					}
					$('#curaddress').empty().append('<li><p class="have">' + infostr + '</p></li>');
					$('#addradio').append('<li><div class="radio"><label><input type="radio" name="address" value="' + infostr + '" checked>' + infostr + '</label>&nbsp;&nbsp;<a class="edit">编辑</a>&nbsp;&nbsp;<a class="delete">删除</a></div></li>');
					$('#receive').val(" ");
					$('#detailadd').val(" ");
					$('#contact').val(" ");
					$('#sendto').html($province + "省" + $city + "市" + $county + $detailadd);
					$('#receiver').html($receive + '&nbsp;&nbsp;' + $contact);
					$thisindex.children('form').css({"display":"none"});
					$thisindex.css({"border":"2px solid #ccc"});
					$('#curaddress').css({"display":"block"});
					$thisindex.children().children('.update').css({"display":"block"});
					$('.submitorder').css({'background-color':"#f60",'color':'#fff'});
					$('.submitorder').bind('click',foo);
					return false;
				}
			}else{
				$receiveinfo = $('.receiveinfo .have').text();
				$infoarray = $receiveinfo.split(',');
				$('#sendto').html($infoarray[1]);
				$('#receiver').html($infoarray[0] + ' ' + $infoarray[2]);
				$thisindex.children('form').css({"display":"none"});
				$thisindex.css({"border":"2px solid #ccc"});
				$('#curaddress').css({"display":"block"});
				$thisindex.children().children('.update').css({"display":"block"});
				$('.submitorder').css({'background-color':"#f60",'color':'#fff'});
				$('.submitorder').bind('click',foo);
				return false;
			}
		});
		$('#newaddress').change(function(){
			$('.address-block').css({"display":"block"});
		});
		/*修改支付方式*/
		$('#updateway').click(function(){
			var $thisindex = $(this).parents('.orangeborder');
			$(this).css({"display":"none"});
			$thisindex.children('.have').css({"display":"none"});
			$thisindex.css({"border":"2px solid #f60"});
			$thisindex.children('form').css({"display":"block"});
			$('.submitorder').css({'background-color':"#eee",'color':'#aaa'});
			$('.submitorder').unbind('click',foo);
		});
		$('#saveway').click(function(){
			var $thisindex = $(this).parents('.orangeborder');
			$thisindex.children('form').css({"display":"none"});
			$thisindex.css({"border":"2px solid #ccc"});
			$thisindex.children('.have').css({"display":"block"});
			$thisindex.children().children('.update').css({"display":"block"});
			var radio = document.all("way");
			for(var i = 0; i < radio.length; i++){
				if(radio[i].checked){
					$('.payway .have').html(radio[i].value + '付款');
				}
			}
			$('.submitorder').css({'background-color':"#f60",'color':'#fff'});
			$('.submitorder').bind('click',foo);
			$('#payway').html($('.payway .have').html());
			return false;
		});
		/*提交订单*/
		function foo(){
			var $action = $('.infoform').attr('data-href');
			$address = $('#curaddress .have').text();
			$payway = $('.payway .have').text();
			// alert($address + ' ' + $payway);
			$.post($action,{address:$address,payway:$payway},function(data){
				if(data.stauts = 1){
					alert('提交订单成功！');
					location.href = "pay.html";
				}else{
					alert('提交订单失败！');
				}
			});
		}
		// $('.submitorder').click(function(){
		// });
	});
	</script>

</body>
</html>