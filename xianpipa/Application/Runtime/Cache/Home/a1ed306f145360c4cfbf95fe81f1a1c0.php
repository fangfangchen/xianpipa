<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
	<title><?php echo ($title); ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
	<link rel="shortcut icon" href="/xianpipa/Public/images/1.ico" />
	<link rel="stylesheet" type="text/css" href="/xianpipa/Public/css/dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/xianpipa/Public/css/index.css">
	<!--[if lt IE 9]>
	<script type="text/javascript">
		location.href = "/xianpipa/index.php/Home/User/ie";
	</script>
	<![endif]-->
</head>
<body>
	<div class="wrapper">
		<div class="top-nav">
	<div class="container">
		<div class="tn-left"><p>您好，欢迎来到天天鲜果！<?php echo $today;?></p></div>
		<div class="tn-right">
			<span class="thisname">您好，<span class="loginname"><?php echo session('user_name');?></span><span class="split">|</span></span>
			<a class="loginbtn" href="<?php echo U('Home/User/login');?>">[登录]<span class="split">|</span></a>
			<a class="registerbtn" href="<?php echo U('Home/User/register');?>">[注册]<span class="split">|</span></a>
			<a class="exitbtn" href="<?php echo U('Home/User/exitthis');?>">[退出]<span class="split">|</span></a>
			<a class="myfruit" href="<?php echo U('Home/User/myfruit');?>">我的果园</a>
		</div>
	</div>
</div>
<div class="user-header">
	<div class="container">
		<a class="logo fleft" href="<?php echo U('Home/Index/index');?>"><img src="/xianpipa/Public/images/logo.png" alt="logo" /></a>
		<div class="search fleft"><input class="form-control searchinput fleft" type="text" placeholder="请输入要搜索的内容"><button class="btn btn-default searchbtn fleft">搜索</button></div>
		<a class="fright mycart">
			<img class="myhover" src="/xianpipa/Public/images/hover.png" alt="购物车" />
			<span class="goodsnum"><?php echo session('goodsnum');?></span>
		</a>
	</div>
</div>
<div class="mainnav" data-action="<?php echo U('Home/Index/index');?>">
	<div class="container">
		<a class="current ml100" id="index" href="<?php echo U('Home/Index/index');?>">首页</a>
		<a id="fruit" href="<?php echo U('Home/Index/fruit');?>">鲜果区</a>
		<a id="gift" href="<?php echo U('Home/Index/gift');?>">礼品区</a>
		<a id="knowledge" href="<?php echo U('Home/Index/knowledge');?>">果食</a>
	</div>
</div>
		
	<div class="content">
		<div class="container">
			<div class="crumb"><a href="<?php echo U('Home/Index/index');?>"><span class="glyphicon glyphicon-home"></span>首页</a> >> <span class="glyphicon glyphicon-book"></span>果食</div>
			<div class="article fleft" id="loadarticle">
				<h1>维C到底能不能预防感冒？破解维生素C的5大谣言！</h1>
				<p class="article-resource">fday</p>
				<div class="ar-block">谣言1</div>
				<p>吃完海鲜不能吃维生素C，会中毒。</p>
				<div class="ar-block">真相</div>
				<p>吃完海鲜可以吃维生素C，不会中毒。</p>
				<p>谣言产生是由于海鲜中有无机砷，而无机砷+维生素C会发生化学反应转变成三价砷（也就是砒霜）。</p>
				<p>对于健康成年人来说，砒霜的致死量约为100~300mg。按100mg砒霜来算，其砷元素含有量为75mg，按照我国国家标准虾蟹中无机砷含量不能超过0.5mg/kg来推算，你需要吃下150公斤虾（按照无机砷含量上限计算）才足以被毒死。</p>
				<p>所以，通过这样的方式被毒死之前，其实你已经撑死了。</p>
				<img src="/xianpipa/Public/images/640.png" alt="" />

				<div class="ar-block">谣言2</div>
				<p>牙龈出血是因为缺乏维生素C</p>
				<div class="ar-block">真相</div>
				<p>牙龈出血不一定是因为缺乏维生素C。</p>
				<p>日常饮食中的果蔬牛奶等食物中都含有维生素C，所以一般人不会因缺乏维生素C而导致牙龈出血。绝大多数的牙龈出血都是牙龈炎的表现，除了极少部分可能是因为血液病等全身疾病。</p>
				<img src="/xianpipa/Public/images/641.png" alt="" />

				<div class="ar-block">谣言3</div>
				<p>不能用热水泡柠檬，会损失维生素C。</p>
				<div class="ar-block">真相</div>
				<p>用热水泡柠檬没那么容易损失维生素C。</p>
				<p>柠檬酸性较强，而维生素C在酸性条件下耐热性很好，没那么容易损失，即使水温度超过60℃也没问题。</p>
				<img src="/xianpipa/Public/images/642.png" alt="" />

				<div class="ar-block">谣言4</div>
				<p>手指长倒刺是因为缺乏维生素C</p>
				<div class="ar-block">真相</div>
				<p>单纯的手指长倒刺一般和缺乏维生素无关。</p>
				<p>维生素缺乏的确可能引起一些皮肤问题，但多伴有其它症状（比如缺乏维生素A可导致毛囊过度角化，但同时还可能引起眼部及视力异常）。所以单纯手指指甲周围长倒刺一般是由于物理摩擦或洗手过多所致，绝大多数都和缺乏维生素无关。</p>
				<img src="/xianpipa/Public/images/643.png" alt="" />

				<div class="ar-block">谣言5</div>
				<p>补充维生素C能预防感冒</p>
				<div class="ar-block">真相</div>
				<p>补充维生素C不能预防感冒。</p>
				<p>感冒分为普通感冒和流行性感冒，两者的致病原因和发病规律都不同。普通感冒通常不吃药也能好；但流感就需要及时医治。</p>
				<p>谣言中的“感冒”是指普通感冒。大量科学研究都已证实大剂量维生素C并不能降低普通感冒发病率。但多吃水果补充维生素Ｃ确实可以提高身体免疫力。</p>
				<p>虽然很多问题不能归结于维生素C缺乏，但是维生素C确实是人体必须的。平时多吃水果蔬菜，补充维生素C，提高身体免疫力。</p>
				<img src="/xianpipa/Public/images/644.png" alt="" />
			</div>
			<div class="other-article fright">
				<h5><span class="glyphicon glyphicon-tags"></span>水果百科</h5>
				<ul>
					<li id="howto">
						<img src="/xianpipa/Public/images/howto.jpg" alt="" />
						<p class="article-title">枇杷快速剥皮的方法</p>
						<p class="article-resource">中国吃网原创</p>
					</li>
					<li id="effect">
						<img src="/xianpipa/Public/images/effect.jpg" alt="" />
						<p class="article-title">枇杷花茶功效与作用</p>
						<p class="article-resource">中国食品科技网</p>
					</li>
					<li id="add">
						<img src="/xianpipa/Public/images/7.jpg" alt="" />
						<p class="article-title">立秋养生小知识：吃枇杷给肺“补水”</p>
						<p class="article-resource">百度百科</p>
					</li>
				</ul>
			</div>
			<div class="gotop" id="gotopbtn"><img src="/xianpipa/Public/images/top.png" alt="回到顶部" /></div>
		</div>
	</div>

		<div class="footer txtcenter">
	<div class="footer-nav">
		<a href="<?php echo U('Home/User/help');?>">友情链接</a>
		<a href="<?php echo U('Home/User/help');?>">关于天天鲜果</a>
		<a href="<?php echo U('Home/User/help');?>">问题与帮助</a>
		<a href="<?php echo U('Home/User/help');?>">联系我们</a>
		<a href="<?php echo U('Admin/Index/login');?>">后台管理</a>
	</div>
	<div class="copyright">
		<p>版权所有 © 2015天天鲜果 保留所有权利 | <a>站长统计</a></p>
		<p>天天鲜果&nbsp;&nbsp;&nbsp;&nbsp;鲜果网购</p>
	</div>
</div>
	</div>
	
	<script type="text/javascript" src="/xianpipa/Public/js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="/xianpipa/Public/js/myjs.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$('.mainnav a').removeClass('current');
		$('.mainnav #knowledge').addClass('current');
		
		/*加载水果百科*/
		$('.other-article li').click(function(){
			var page = $(this).attr('id');
			$('#loadarticle').load(page + '.html');
		});
	});
	</script>

</body>
</html>