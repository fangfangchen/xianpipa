<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
	<title><?php echo ($title); ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
	<link rel="shortcut icon" href="/xianpipa/Public/images/1.ico" />
	<link rel="stylesheet" type="text/css" href="/xianpipa/Public/css/dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/xianpipa/Public/css/index.css">
	<!--[if lt IE 9]>
	<script type="text/javascript">
		location.href = "/xianpipa/index.php/Home/User/ie";
	</script>
	<![endif]-->
</head>
<body>
	<div class="wrapper">
		<div class="top-nav">
	<div class="container">
		<div class="tn-left"><p>您好，欢迎来到天天鲜果！<?php echo $today;?></p></div>
		<div class="tn-right">
			<span class="thisname">您好，<span class="loginname"><?php echo session('user_name');?></span><span class="split">|</span></span>
			<a class="loginbtn" href="<?php echo U('Home/User/login');?>">[登录]<span class="split">|</span></a>
			<a class="registerbtn" href="<?php echo U('Home/User/register');?>">[注册]<span class="split">|</span></a>
			<a class="exitbtn" href="<?php echo U('Home/User/exitthis');?>">[退出]<span class="split">|</span></a>
			<a class="myfruit" href="<?php echo U('Home/User/myfruit');?>">我的果园</a>
		</div>
	</div>
</div>
<div class="user-header">
	<div class="container">
		<a class="logo fleft" href="<?php echo U('Home/Index/index');?>"><img src="/xianpipa/Public/images/logo.png" alt="logo" /></a>
		<a class="fright mycart">
			<img class="myhover" src="/xianpipa/Public/images/hover.png" alt="购物车" />
			<span class="goodsnum"><?php echo session('goodsnum');?></span>
		</a>
	</div>
</div>
		
	<div class="content">
		<div class="container mycartblock">
			<!-- 支付 -->
			<div class="checkcart" id="chosepayblock">
				<div class="current-process">
					<div class="fleft leftp">
						<p class="orange">您已提交订单，请尽快支付！</p>
						<p>请在半小时内付款，超过时间系统将自动取消订单！</p>
					</div>
					<div class="payimg processimg fright"></div>
				</div>
				<div class="paynow">
					<p>您的订单号：<?php echo session('orderid');?><a href="/xianpipa/index.php/Home/User/myfruit.html" target="_blank">&nbsp;&nbsp;查看订单&gt;&gt;</a></p>
					<p>支付金额：<span class="orange total">￥<?php echo session('ordertotal');?></span></span></p>
					<div class="orangeborder payway">
						<div style="width: 100%;overflow: hidden;">
						<p class="border-title fleft">支付方式</p>
						<a class="update fleft" id="updateway">[修改]</a>
					</div>
					<p class="have"><?php echo session('payway');?></p>
					<form class="form-horizontal payform" action="" method="">
						<div class="way radio">
							<label>
								<input type="radio" name="way" id="way1" value="支付宝" checked>
								<img src="/xianpipa/Public/images/alipay.jpg" alt="支付宝" />
							</label>
						</div>
						<div class="way radio">
							<label>
								<input type="radio" name="way" id="way2" value="中国银行">
								<img src="/xianpipa/Public/images/bank.jpg" alt="中国银行" />
							</label>
						</div>
						<div class="form-group">
							<a class="btn btn-default save" id="saveway">保存支付方式</a>
						</div>
					</form>
					</div>
					<div class="form-group">
						<a class="btn btn-default save" id="paynow" href="https://www.alipay.com/" target="_blank">立即支付</a>
					</div>
				</div>
			</div>
			</div>
	</div>

		<div class="footer txtcenter">
	<div class="footer-nav">
		<a href="<?php echo U('Home/User/help');?>">友情链接</a>
		<a href="<?php echo U('Home/User/help');?>">关于天天鲜果</a>
		<a href="<?php echo U('Home/User/help');?>">问题与帮助</a>
		<a href="<?php echo U('Home/User/help');?>">联系我们</a>
		<a href="<?php echo U('Admin/Index/login');?>">后台管理</a>
	</div>
	<div class="copyright">
		<p>版权所有 © 2015天天鲜果 保留所有权利 | <a>站长统计</a></p>
		<p>天天鲜果&nbsp;&nbsp;&nbsp;&nbsp;鲜果网购</p>
	</div>
</div>
	</div>
	
	<script type="text/javascript" src="/xianpipa/Public/js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="/xianpipa/Public/js/myjs.js"></script>
	<script type="text/javascript">
	/*修改支付方式*/
	$('#updateway').click(function(){
		var $thisindex = $(this).parents('.orangeborder');
		$(this).css({"display":"none"});
		$thisindex.children('.have').css({"display":"none"});
		$thisindex.css({"border":"2px solid #f60"});
		$thisindex.children('form').css({"display":"block"});
		$('#paynow').css({'background-color':"#eee",'color':'#aaa'});
		$('#paynow').attr('href','');
	});
	$('#saveway').click(function(){
		var $thisindex = $(this).parents('.orangeborder');
		$thisindex.children('form').css({"display":"none"});
		$thisindex.css({"border":"2px solid #ccc"});
		$thisindex.children('.have').css({"display":"block"});
		$('#updateway').css({"display":"block"});
		var radio = document.all("way");
		for(var i = 0; i < radio.length; i++){
			if(radio[i].checked){
				$('.paynow .have').html(radio[i].value + '付款');
			}
		}
		$('#paynow').css({'background-color':"#f60",'color':'#fff'});
		$('#paynow').attr('href','https://www.alipay.com/');
		return false;
	});
	</script>

</body>
</html>