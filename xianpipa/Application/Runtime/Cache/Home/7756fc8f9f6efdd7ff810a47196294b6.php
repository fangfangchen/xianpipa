<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
	<title><?php echo ($title); ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
	<link rel="shortcut icon" href="/xianpipa/Public/images/1.ico" />
	<link rel="stylesheet" type="text/css" href="/xianpipa/Public/css/dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/xianpipa/Public/css/index.css">
	<!--[if lt IE 9]>
	<script type="text/javascript">
		location.href = "/xianpipa/index.php/Home/User/ie";
	</script>
	<![endif]-->
</head>
<body>
	<div class="wrapper">
		<div class="top-nav">
	<div class="container">
		<div class="tn-left"><p>您好，欢迎来到天天鲜果！<?php echo $today;?></p></div>
		<div class="tn-right">
			<span class="thisname">您好，<span class="loginname"><?php echo session('user_name');?></span><span class="split">|</span></span>
			<a class="loginbtn" href="<?php echo U('Home/User/login');?>">[登录]<span class="split">|</span></a>
			<a class="registerbtn" href="<?php echo U('Home/User/register');?>">[注册]<span class="split">|</span></a>
			<a class="exitbtn" href="<?php echo U('Home/User/exitthis');?>">[退出]<span class="split">|</span></a>
			<a class="myfruit" href="<?php echo U('Home/User/myfruit');?>">我的果园</a>
		</div>
	</div>
</div>
<div class="user-header">
	<div class="container">
		<a class="logo fleft" href="<?php echo U('Home/Index/index');?>"><img src="/xianpipa/Public/images/logo.png" alt="logo" /></a>
		<div class="search fleft"><input class="form-control searchinput fleft" type="text" placeholder="请输入要搜索的内容"><button class="btn btn-default searchbtn fleft">搜索</button></div>
		<a class="fright mycart">
			<img class="myhover" src="/xianpipa/Public/images/hover.png" alt="购物车" />
			<span class="goodsnum"><?php echo session('goodsnum');?></span>
		</a>
	</div>
</div>
<div class="mainnav" data-action="<?php echo U('Home/Index/index');?>">
	<div class="container">
		<a class="current ml100" id="index" href="<?php echo U('Home/Index/index');?>">首页</a>
		<a id="fruit" href="<?php echo U('Home/Index/fruit');?>">鲜果区</a>
		<a id="gift" href="<?php echo U('Home/Index/gift');?>">礼品区</a>
		<a id="knowledge" href="<?php echo U('Home/Index/knowledge');?>">果食</a>
	</div>
</div>
		
	<div class="content">
		<div class="container">
			<div class="account fleft">
				<p>帮助中心</p>
				<ul id="accountmenu">
					<li data-id="process">购物流程</li>
					<li data-id="problem">常见问题</li>
					<li data-id="aboutus">关于天天鲜果</li>
					<li data-id="links">友情链接</li>
					<li data-id="contact">联系我们</li>
				</ul>
			</div>
			<div class="account-right fleft">
				<div class="crumb">
					<a href="<?php echo U('Home/Index/index');?>"><span class="glyphicon glyphicon-home"></span>首页</a> >> <span class="glyphicon glyphicon-question-sign"></span>帮助中心 >> <span class="glyphicon glyphicon-screenshot"></span><span class="account-lbs">购物流程</span>
				</div>
				<p class="account-lbs curtags">购物流程</p>
				<!-- 购物流程 -->
				<div class="help-block current" id="process">
					<p>1、在天天鲜果网站登录或者注册，成为天天鲜果的会员，即可享受接下来的购物。</p>
					<img src="/xianpipa/Public/images/login.png" alt="登录" />
					<img src="/xianpipa/Public/images/register.png" alt="注册" />
					<p>2、在天天鲜果网站浏览选购您需要的产品，选择 需要的商品及规格， 点击“加入购物车”即加入到购物车。</p>
					<img src="/xianpipa/Public/images/chose.png" alt="选择商品" />
					<p>3、查看购物车，确认商品后点击“去结算”。</p>
					<img src="/xianpipa/Public/images/topay.png" alt="去结算" />
					<p>4、进入购物车，按照要求填写收货人信息、选择送货时间以及付款方式。信息填写完整后即可提交订单，进入支付页面。</p>
					<img src="/xianpipa/Public/images/check.png" alt="填写并核查信息" />
					<p>5、订单提交成功后，点击前往银行付款。</p>
					<img src="/xianpipa/Public/images/pay.png" alt="立即付款" />
				</div>
				<!-- 常见问题 -->
				<div class="help-block" id="problem"></div>
				<!-- 关于天天鲜果 -->
				<div class="help-block" id="aboutus">
					<p>该网站作为毕业设计的项目，主要想通过互联网结合当下流行的营销模式，打破传统的水果销售局限，找到一种新的盈利方式。</p>
				</div>
				<!-- 友情链接 -->
				<div class="help-block" id="links">
					<p>支付宝：<a href="https://www.alipay.com/">https://www.alipay.com/</a></p>
				</div>
				<!-- 联系我们 -->
				<div class="help-block" id="contact">
					<p>此网站为水果类购物网站，如在购物过程中有任何问题，可以咨询<strong>QQ：2354797719</strong>。祝您购物愉快！~</p>
				</div>
			</div>
		</div>
	</div>

		<div class="footer txtcenter">
	<div class="footer-nav">
		<a href="<?php echo U('Home/User/help');?>">友情链接</a>
		<a href="<?php echo U('Home/User/help');?>">关于天天鲜果</a>
		<a href="<?php echo U('Home/User/help');?>">问题与帮助</a>
		<a href="<?php echo U('Home/User/help');?>">联系我们</a>
		<a href="<?php echo U('Admin/Index/login');?>">后台管理</a>
	</div>
	<div class="copyright">
		<p>版权所有 © 2015天天鲜果 保留所有权利 | <a>站长统计</a></p>
		<p>天天鲜果&nbsp;&nbsp;&nbsp;&nbsp;鲜果网购</p>
	</div>
</div>
	</div>
	
	<script type="text/javascript" src="/xianpipa/Public/js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript">
	$('#accountmenu li').click(function(){
		var curblock = $(this).attr('data-id');
		var text = $(this).text();
		$('.account-lbs').html(text);
		$('.help-block').removeClass('current');
		$('#' + curblock).addClass('current');
	});
	// /*我的订单*/
	// $('#myorder li').click(function(){
	// 	var blockid = $(this).attr('data-id');
	// 	$('.order-block').removeClass('cur-myorder');
	// 	$('#' + blockid).addClass('cur-myorder');
	// 	$('#myorder li').removeClass('myorder-current');
	// 	$(this).addClass('myorder-current');
	// });
	</script>

</body>
</html>