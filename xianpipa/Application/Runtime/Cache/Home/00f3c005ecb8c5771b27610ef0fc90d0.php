<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
	<title><?php echo ($title); ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
	<link rel="shortcut icon" href="/xianpipa/Public/images/1.ico" />
	<link rel="stylesheet" type="text/css" href="/xianpipa/Public/css/dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/xianpipa/Public/css/index.css">
	<!--[if lt IE 9]>
	<script type="text/javascript">
		location.href = "/xianpipa/index.php/Home/User/ie";
	</script>
	<![endif]-->
</head>
<body>
	<div class="wrapper">
		<div class="top-nav">
	<div class="container">
		<div class="tn-left"><p>您好，欢迎来到天天鲜果！<?php echo $today;?></p></div>
		<div class="tn-right">
			<span class="thisname">您好，<span class="loginname"><?php echo session('user_name');?></span><span class="split">|</span></span>
			<a class="loginbtn" href="<?php echo U('Home/User/login');?>">[登录]<span class="split">|</span></a>
			<a class="registerbtn" href="<?php echo U('Home/User/register');?>">[注册]<span class="split">|</span></a>
			<a class="exitbtn" href="<?php echo U('Home/User/exitthis');?>">[退出]<span class="split">|</span></a>
			<a class="myfruit" href="<?php echo U('Home/User/myfruit');?>">我的果园</a>
		</div>
	</div>
</div>
<div class="user-header">
	<div class="container">
		<a class="logo fleft" href="<?php echo U('Home/Index/index');?>"><img src="/xianpipa/Public/images/logo.png" alt="logo" /></a>
		<div class="search fleft"><input class="form-control searchinput fleft" type="text" placeholder="请输入要搜索的内容"><button class="btn btn-default searchbtn fleft">搜索</button></div>
		<a class="fright mycart">
			<img class="myhover" src="/xianpipa/Public/images/hover.png" alt="购物车" />
			<span class="goodsnum"><?php echo session('goodsnum');?></span>
		</a>
	</div>
</div>
<div class="mainnav" data-action="<?php echo U('Home/Index/index');?>">
	<div class="container">
		<a class="current ml100" id="index" href="<?php echo U('Home/Index/index');?>">首页</a>
		<a id="fruit" href="<?php echo U('Home/Index/fruit');?>">鲜果区</a>
		<a id="gift" href="<?php echo U('Home/Index/gift');?>">礼品区</a>
		<a id="knowledge" href="<?php echo U('Home/Index/knowledge');?>">果食</a>
	</div>
</div>
		
	<div class="content">
		<div class="container">
			<div class="crumb"><a href="<?php echo U('Home/Index/index');?>"><span class="glyphicon glyphicon-home"></span>首页</a> >> <span class="glyphicon glyphicon-cutlery"></span>鲜果区</div>
			<div class="condition">
				<div class="conoption resource">产地：
					<a class="con-current" href="<?php echo U('Home/Index/fruit');?>">不限</a>
					<a href="<?php echo U('Home/Index/fjproduce');?>">福建</a>
					<a href="<?php echo U('Home/Index/scproduce');?>">四川</a>
				</div>
				<div class="conoption inter-price" data-url="">价格：
					<a class="con-current" href="#">不限</a>
					<a href="#">100以下</a>
					<a href="#">100~200</a>
					<a href="#">200以上</a>
				</div>
			</div>
			<div class="splitline">
				<a class="sort-current" id="default">默认</a><span class="split">|</span>
				<a id="higher">价格从低到高</a><span class="split">|</span>
				<a id="lower">价格从高到低</a>
			</div>
			<ul class="goodslist fruitlist">
				<form class="addcartform pricedefault" action="<?php echo U('Home/Index/fruit');?>" method="post">
					<?php if(is_array($fruitlist["list"])): $i = 0; $__LIST__ = $fruitlist["list"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$data): $mod = ($i % 2 );++$i;?><li data-id="<?php echo ($data['pro_id']); ?>" data-storage="<?php echo ($data['gstorage']); ?>">
							<a class="detailbtn" data-href="<?php echo U('Home/Index/getdetail');?>">
								<img src="/xianpipa/Public/images/<?php echo explode(',', $data['pro_img'])[1];?>" alt="枇杷" />
								<p class="goodstitle"><?php echo ($data['pro_name']); ?></p>
								<p><?php echo ($data['pro_weight']); ?></p>
								<p class="price">￥<?php echo ($data['pro_disprice']); ?></p>
							</a>
							<div class="ml10 fleft">
								<span class="operate decrease"><img src="/xianpipa/Public/images/bag_close.gif" alt="数量减少" /></span>
								<span class="number txtcenter">1</span>
								<span class="operate increase"><img src="/xianpipa/Public/images/bag_open.gif" alt="数量增加" /></span>
							</div>
							<div class="addcartbtn fright"><img src="/xianpipa/Public/images/cart.png" alt="加入到购物车" /></div>
						</li><?php endforeach; endif; else: echo "" ;endif; ?>
					<div class="pagelist"><?php echo ($fruitlist["page"]); ?></div>
				</form>
				<!-- 按低到高 -->
				<form class="addcartform pricehigher" action="<?php echo U('Home/Index/fruit');?>" method="post" style="display:none;">
					<?php if(is_array($fruitlist1["list"])): $i = 0; $__LIST__ = $fruitlist1["list"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$data): $mod = ($i % 2 );++$i;?><li data-id="<?php echo ($data['pro_id']); ?>" data-storage="<?php echo ($data['gstorage']); ?>">
							<a class="detailbtn" data-href="<?php echo U('Home/Index/getdetail');?>">
								<img src="/xianpipa/Public/images/<?php echo explode(',', $data['pro_img'])[1];?>" alt="枇杷" />
								<p class="goodstitle"><?php echo ($data['pro_name']); ?></p>
								<p><?php echo ($data['pro_weight']); ?></p>
								<p class="price">￥<?php echo ($data['pro_disprice']); ?></p>
							</a>
							<div class="ml10 fleft">
								<span class="operate decrease"><img src="/xianpipa/Public/images/bag_close.gif" alt="数量减少" /></span>
								<span class="number txtcenter">1</span>
								<span class="operate increase"><img src="/xianpipa/Public/images/bag_open.gif" alt="数量增加" /></span>
							</div>
							<div class="addcartbtn fright"><img src="/xianpipa/Public/images/cart.png" alt="加入到购物车" /></div>
						</li><?php endforeach; endif; else: echo "" ;endif; ?>
					<div class="pagelist"><?php echo ($fruitlist1["page"]); ?></div>
				</form>
				<!-- 按高到低 -->
				<form class="addcartform pricelower" action="<?php echo U('Home/Index/fruit');?>" method="post" style="display:none;">
					<?php if(is_array($fruitlist2["list"])): $i = 0; $__LIST__ = $fruitlist2["list"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$data): $mod = ($i % 2 );++$i;?><li data-id="<?php echo ($data['pro_id']); ?>" data-storage="<?php echo ($data['gstorage']); ?>">
							<a class="detailbtn" data-href="<?php echo U('Home/Index/getdetail');?>">
								<img src="/xianpipa/Public/images/<?php echo explode(',', $data['pro_img'])[1];?>" alt="枇杷" />
								<p class="goodstitle"><?php echo ($data['pro_name']); ?></p>
								<p><?php echo ($data['pro_weight']); ?></p>
								<p class="price">￥<?php echo ($data['pro_disprice']); ?></p>
							</a>
							<div class="ml10 fleft">
								<span class="operate decrease"><img src="/xianpipa/Public/images/bag_close.gif" alt="数量减少" /></span>
								<span class="number txtcenter">1</span>
								<span class="operate increase"><img src="/xianpipa/Public/images/bag_open.gif" alt="数量增加" /></span>
							</div>
							<div class="addcartbtn fright"><img src="/xianpipa/Public/images/cart.png" alt="加入到购物车" /></div>
						</li><?php endforeach; endif; else: echo "" ;endif; ?>
					<div class="pagelist"><?php echo ($fruitlist2["page"]); ?></div>
				</form>
			</ul>
			<div class="current-search">
				<p class="curtitle">最近浏览的商品</p>
				<ul class="curlists">
					<?php if(is_array($lastlist["list"])): $i = 0; $__LIST__ = $lastlist["list"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$data): $mod = ($i % 2 );++$i;?><li data-id="<?php echo ($data['pro_id']); ?>">
							<a class="detailbtn" data-href="<?php echo U('Home/Index/getdetail');?>">
								<img src="/xianpipa/Public/images/<?php echo explode(',', $data['pro_img'])[1];?>" alt="枇杷" />
								<div class="hotright">
									<p><?php echo ($data['pro_name']); ?></p>
									<p class="hot-price">￥<?php echo ($data['pro_disprice']); ?></p>
								</div>
							</a>
						</li><?php endforeach; endif; else: echo "" ;endif; ?>
				</ul>
			</div>
			<div class="addtocart txtcenter">
				<p class="addtitle">已成功加入到购物车</p>
				<p>购物车共有<span class="orange hasnum"><?php echo session('goodsnum');?></span>件商品，合计：<span class="orange total">￥<?php echo session('totalprice');?></span></p>
				<div class="close"><img src="/xianpipa/Public/images/close.png" alt="关闭" /></div>
				<div class="oprea-group">
					<button class="btn btn-default arround">继续逛逛</button>
					<a class="btn btn-default checkout" href="<?php echo U('Home/User/cart');?>">去结算</a>
				</div>
			</div>
			<div class="gotop" id="gotopbtn"><img src="/xianpipa/Public/images/top.png" alt="回到顶部" /></div>
		</div>
	</div>

		<div class="footer txtcenter">
	<div class="footer-nav">
		<a href="<?php echo U('Home/User/help');?>">友情链接</a>
		<a href="<?php echo U('Home/User/help');?>">关于天天鲜果</a>
		<a href="<?php echo U('Home/User/help');?>">问题与帮助</a>
		<a href="<?php echo U('Home/User/help');?>">联系我们</a>
		<a href="<?php echo U('Admin/Index/login');?>">后台管理</a>
	</div>
	<div class="copyright">
		<p>版权所有 © 2015天天鲜果 保留所有权利 | <a>站长统计</a></p>
		<p>天天鲜果&nbsp;&nbsp;&nbsp;&nbsp;鲜果网购</p>
	</div>
</div>
	</div>
	
	<script type="text/javascript" src="/xianpipa/Public/js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="/xianpipa/Public/js/myjs.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		$('.mainnav a').removeClass('current');
		$('.mainnav #fruit').addClass('current');

		/*添加到购物车*/
		$('.addcartbtn').click(function(){
			if($(".loginname").text()){
				$goodsid = $(this).parents('li').attr("data-id");
				$username = $('.loginname').text();
				$pronum = $(this).parents('li').children('.ml10').children('.number').text();
				$action = $('.addcartform').attr('action');
				$curstorage = $(this).parents('li').attr("data-storage");
				if($curstorage - $pronum < 0){
					alert('不好意思，当前库存量不够！');
				}else{
					$.post($action,{username:$username,goodsid:$goodsid,pronum:$pronum},function(data){
						$('.goodsnum').html(data.goodsnum);
						$('.total').html('￥' + data.totalprice);
						$('.addtocart').fadeIn(300).css({'display':"block"});
						setTimeout(function(){
							$('.addtocart').fadeOut(300).css({'display':"none"});
						},1500);
						$('.hasnum').html(data.goodsnum);
						// alert(data);
					});
					// location.reload();
				}
			}else{
				location.href = "/xianpipa/index.php/Home/User/login.html";
			}
		});
		
		/*商品详细*/
		$('.detailbtn').click(function(e){
			$action = $(this).attr('data-href');
			$goodsid = $(this).parent('li').attr('data-id');
			$username = $('.loginname').text();
			// alert($username + " " + $goodsid);
			$.post($action,{username:$username,goodsid:$goodsid},function(data){
				$('.goodsnum').html(data.goodsnum);
				$('.total').html(data.totalprice);
				$('.hasnum').html(data.goodsnum);
				// $('.goodstitle').html(data.proname);
				location.href = "detail.html";
			});
			e.preventDefault();
		});

		/*按价格筛选*/
		$('#default').click(function(){
			$('.splitline a').removeClass('sort-current');
			$(this).addClass('sort-current');
			$('.addcartform').css({'display':'none'});
			$('.pricedefault').css({'display':'block'});
		});
		$('#higher').click(function(){
			$('.splitline a').removeClass('sort-current');
			$(this).addClass('sort-current');
			$('.addcartform').css({'display':'none'});
			$('.pricehigher').css({'display':'block'});
		});
		$('#lower').click(function(){
			$('.splitline a').removeClass('sort-current');
			$(this).addClass('sort-current');
			$('.addcartform').css({'display':'none'});
			$('.pricelower').css({'display':'block'});
		});
	});
	</script>

</body>
</html>