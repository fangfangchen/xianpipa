<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
	<title><?php echo ($title); ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
	<link rel="shortcut icon" href="/xianpipa/Public/images/1.ico" />
	<link rel="stylesheet" type="text/css" href="/xianpipa/Public/css/dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/xianpipa/Public/css/index.css">
	<!--[if lt IE 9]>
	<script type="text/javascript">
		location.href = "/xianpipa/index.php/Home/User/ie";
	</script>
	<![endif]-->
</head>
<body>
	<div class="wrapper">
		<div class="top-nav">
	<div class="container">
		<div class="tn-left"><p>您好，欢迎来到天天鲜果！<?php echo $today;?></p></div>
		<div class="tn-right">
			<span class="thisname">您好，<span class="loginname"><?php echo session('user_name');?></span><span class="split">|</span></span>
			<a class="loginbtn" href="<?php echo U('Home/User/login');?>">[登录]<span class="split">|</span></a>
			<a class="registerbtn" href="<?php echo U('Home/User/register');?>">[注册]<span class="split">|</span></a>
			<a class="exitbtn" href="<?php echo U('Home/User/exitthis');?>">[退出]<span class="split">|</span></a>
			<a class="myfruit" href="<?php echo U('Home/User/myfruit');?>">我的果园</a>
		</div>
	</div>
</div>
<div class="user-header">
	<div class="container">
		<a class="logo fleft" href="<?php echo U('Home/Index/index');?>"><img src="/xianpipa/Public/images/logo.png" alt="logo" /></a>
		<a class="fright mycart">
			<img class="myhover" src="/xianpipa/Public/images/hover.png" alt="购物车" />
			<span class="goodsnum"><?php echo session('goodsnum');?></span>
		</a>
	</div>
</div>
		
	<div class="content">
		<div class="container mycartblock">
			<!-- 我的购物车 -->
			<div class="checkcart" id="cartblock">
				<div class="current-process">
					<p class="process-title fleft">我的购物车</p>
					<div class="cartimg processimg fright"></div>
				</div>
				<span class="nothingtips txtcenter">您购物车内暂时没有商品，您可以去<a href="<?php echo U('Home/Index/index');?>">首页挑选喜欢的商品</a></span>
				<table class="table ordertable cartlists" data-action="<?php echo U('Home/User/cart');?>">
					<thead>
						<th>商品信息</th>
						<th>规格</th>
						<th>原价(元)</th>
						<th>单价(元)</th>
						<th>数量</th>
						<th>优惠</th>
						<th>小计</th>
						<th>操作</th>
					</thead>
					<tbody>						
						<?php if(is_array($cartlist)): $i = 0; $__LIST__ = $cartlist;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$data): $mod = ($i % 2 );++$i;?><tr>
								<td><img src="/xianpipa/Public/images/<?php echo explode(',', $data['pro_img'])[1];?>" alt="枇杷" style="width: 80px;height 70px;margin-right:10px;" /><?php echo ($data['pro_name']); ?></td>
								<span class="goodsid" style="display:none;"><?php echo ($data['pro_id']); ?></span>
								<td class="weight"><?php echo ($data['pro_weight']); ?></td>
								<td class="disprice"><?php echo ($data['pro_price']); ?></td>
								<td class="perprice"><?php echo ($data['pro_disprice']); ?></td>
								<td>
									<span class="operate decrease"><img src="/xianpipa/Public/images/bag_close.gif" alt="数量减少" /></span>
									<span class="number txtcenter"><?php echo ($data['add_count']); ?></span>
									<span class="operate increase"><img src="/xianpipa/Public/images/bag_open.gif" alt="数量增加" /></span>
								</td>
								<td></td>
								<td class="sumprice"><?php echo ($data['sumprice']); ?></td>
								<td><a class="deletebtn" data-url="<?php echo U('Home/User/delete');?>" data-id="<?php echo ($data['pro_id']); ?>">删除</a></td>
							</tr><?php endforeach; endif; else: echo "" ;endif; ?>
					</tbody>
				</table>
				<div class="cart-operate">
					<p class="txtcenter">商品金额总计：<span class="orange total">￥<?php echo session('totalprice');?></span></p>
					<div class="oprea-group">
						<a class="btn btn-default arround" href="<?php echo U('Home/Index/index');?>">继续逛逛</a>
						<a class="btn btn-default checkout" href="<?php echo U('Home/User/check');?>">去结算</a>
					</div>
				</div>
				<div class="current-search">
					<p class="curtitle">最近浏览的商品</p>
					<ul class="curlists">
						<?php if(is_array($lastlist["list"])): $i = 0; $__LIST__ = $lastlist["list"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$data): $mod = ($i % 2 );++$i;?><li data-id="<?php echo ($data['pro_id']); ?>">
								<a class="detailbtn" data-href="<?php echo U('Home/Index/getdetail');?>">
									<img src="/xianpipa/Public/images/<?php echo explode(',', $data['pro_img'])[1];?>" alt="枇杷" />
									<div class="hotright">
										<p><?php echo ($data['pro_name']); ?></p>
										<p class="hot-price">￥<?php echo ($data['pro_disprice']); ?></p>
									</div>
								</a>
							</li><?php endforeach; endif; else: echo "" ;endif; ?>
					</ul>
				</div>
			</div>
		</div>
	</div>

		<div class="footer txtcenter">
	<div class="footer-nav">
		<a href="<?php echo U('Home/User/help');?>">友情链接</a>
		<a href="<?php echo U('Home/User/help');?>">关于天天鲜果</a>
		<a href="<?php echo U('Home/User/help');?>">问题与帮助</a>
		<a href="<?php echo U('Home/User/help');?>">联系我们</a>
		<a href="<?php echo U('Admin/Index/login');?>">后台管理</a>
	</div>
	<div class="copyright">
		<p>版权所有 © 2015天天鲜果 保留所有权利 | <a>站长统计</a></p>
		<p>天天鲜果&nbsp;&nbsp;&nbsp;&nbsp;鲜果网购</p>
	</div>
</div>
	</div>
	
	<script type="text/javascript" src="/xianpipa/Public/js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="/xianpipa/Public/js/myjs.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		/*判断购物车是否为空，为空提示去添加商品*/
		if($('.total').text() == '￥0'){
			$('.nothingtips').css({"display":"block"});
			$('.cartlists').css({"display":"none"});
			$('.checkout').attr('href','');
		}
		/*删除商品*/
		$('.deletebtn').click(function(){
			var r = confirm("是否要删除该记录？");
  			if (r == true){
				$action = $(this).attr('data-url');
				$goodsid = $(this).attr('data-id');
				// alert($goodsid);
				$.post($action,{goodsid:$goodsid},function(data){
					if(data.status == 1){
						// alert('删除成功！');
						$('.goodsnum').html(data.goodsnum);
						location.reload();
					}else{
						alert('删除失败！');
					}
				});
			}
		});
		/*商品详细*/
		$('.detailbtn').click(function(e){
			$action = $(this).attr('data-href');
			$goodsid = $(this).parent('li').attr('data-id');
			$username = $('.loginname').text();
			// alert($username + " " + $goodsid);
			$.post($action,{username:$username,goodsid:$goodsid},function(data){
				$('.goodsnum').html(data.goodsnum);
				$('.total').html(data.totalprice);
				$('.hasnum').html(data.goodsnum);
				// $('.goodstitle').html(data.proname);
				location.href = "/xianpipa/index.php/Home/Index/detail.html";
			});
			e.preventDefault();
		});
	});
	</script>

</body>
</html>