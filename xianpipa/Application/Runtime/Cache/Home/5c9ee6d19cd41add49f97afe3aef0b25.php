<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
	<title><?php echo ($title); ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
	<link rel="shortcut icon" href="/xianpipa/Public/images/1.ico" />
	<link rel="stylesheet" type="text/css" href="/xianpipa/Public/css/dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/xianpipa/Public/css/user.css">
	<!--[if lt IE 9]>
	<script type="text/javascript">
		location.href = "/xianpipa/index.php/Home/User/ie";
	</script>
	<![endif]-->
</head>
<body>
	<div class="wrapper">
		
	<div class="top-nav">
		<div class="container">
			<div class="tn-left"><p>您好，欢迎来到天天鲜果！<?php echo $today;?></p></div>
			<div class="tn-right">
				<span class="thisname">您好，<span class="loginname"><?php echo session('user_name');?></span></span>
				<a class="registerbtn" href="<?php echo U('Home/User/register');?>">[注册]</a><span class="split">|</span>
				<a href="<?php echo U('Home/Index/index');?>">[首页]</a><span class="split">|</span>
				<a class="myfruit">我的果园</a>
			</div>
		</div>
	</div>
	<div class="user-header">
		<div class="container">
			<a class="fleft" href="<?php echo U('Home/Index/index');?>"><img src="/xianpipa/Public/images/logo.png" alt="logo" /></a>
			<span class="welcome-tips"><span class="split">|</span>欢迎登录</span>
		</div>
	</div>
	<!-- class="user-content" start -->
	<div class="user-content">
		<div class="container">
			<div class="content-left fleft"><img src="/xianpipa/Public/images/loginleft.png" alt="logo" /></div>
			<div class="content-right fright">
				<p class="form-title">登录天天鲜果</p>
				<form class="form-horizontal login-form" action="<?php echo U('Home/User/login');?>" method="post">
					<div class="form-group">
						<label class="control-label col-xs-3">手机/邮箱</label>
						<div class="col-xs-9"><input class="form-control" id="username" name="username" type="text" placeholder="请输入手机或邮箱"></div>
						<label class="control-label usernameerror error"><img src="/xianpipa/Public/images/unchecked.gif" alt="错误" />用户名不存在或者错误，请重新输入！</label>
					</div>
					<div class="form-group">
						<label class="control-label col-xs-3">密码</label>
						<div class="col-xs-9"><input class="form-control" id="password" name="password" type="password" placeholder="请输入密码"></div>
						<label class="control-label pwderror error"><img src="/xianpipa/Public/images/unchecked.gif" alt="错误" />密码有错，请重新输入！</label>
					</div>
					<div class="form-group mt30">
						<button class="btn btn-default login userbtn" id="signin">登录</button>
						<a class="forgetpwd" href="#">忘记密码？</a>
					</div>
					<div class="form-group txtcenter"><label class="control-label" id="notice"></label></div>
				</form>
			</div>
		</div>
	</div>
	<!-- class="user-content" end -->

		<div class="footer txtcenter">
	<div class="footer-nav">
		<a href="<?php echo U('Home/User/help');?>">友情链接</a>
		<a href="<?php echo U('Home/User/help');?>">关于天天鲜果</a>
		<a href="<?php echo U('Home/User/help');?>">问题与帮助</a>
		<a href="<?php echo U('Home/User/help');?>">联系我们</a>
		<a href="<?php echo U('Admin/Index/login');?>">后台管理</a>
	</div>
	<div class="copyright">
		<p>版权所有 © 2015天天鲜果 保留所有权利 | <a>站长统计</a></p>
		<p>天天鲜果&nbsp;&nbsp;&nbsp;&nbsp;鲜果网购</p>
	</div>
</div>
	</div>
	
	<script type="text/javascript" src="/xianpipa/Public/js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="/xianpipa/Public/js/myjs.js"></script>
	<script type="text/javascript">
	$('#signin').click(function(e){
		$username = $("#username").val();
		console.log($("#username").val() + " " + $("#password").val());
		$pwd = $("#password").val();
		$action = $('.login-form').attr('action');
		$.post($action, {username:$username, password:$pwd}, function(data){
			if(data.status == 1){
				$('#notice').html(data.info);
				setTimeout(function(){
					window.location.href = "<?php echo U('Home/Index/index');?>"; 
				},1000);
			}else{
				$('#notice').html('<img src="/xianpipa/Public/images/unchecked.gif" alt="错误" />' + data.info);
			}
		});
		e.preventDefault();
	});
	</script>

</body>
</html>