<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
	<title><?php echo ($title); ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
	<link rel="shortcut icon" href="/xianpipa/Public/images/1.ico" />
	<link rel="stylesheet" type="text/css" href="/xianpipa/Public/css/dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/xianpipa/Public/css/index.css">
	<!--[if lt IE 9]>
	<script type="text/javascript">
		location.href = "/xianpipa/index.php/Home/User/ie";
	</script>
	<![endif]-->
</head>
<body>
	<div class="wrapper">
		<div class="top-nav">
	<div class="container">
		<div class="tn-left"><p>您好，欢迎来到天天鲜果！<?php echo $today;?></p></div>
		<div class="tn-right">
			<span class="thisname">您好，<span class="loginname"><?php echo session('user_name');?></span><span class="split">|</span></span>
			<a class="loginbtn" href="<?php echo U('Home/User/login');?>">[登录]<span class="split">|</span></a>
			<a class="registerbtn" href="<?php echo U('Home/User/register');?>">[注册]<span class="split">|</span></a>
			<a class="exitbtn" href="<?php echo U('Home/User/exitthis');?>">[退出]<span class="split">|</span></a>
			<a class="myfruit" href="<?php echo U('Home/User/myfruit');?>">我的果园</a>
		</div>
	</div>
</div>
<div class="user-header">
	<div class="container">
		<a class="logo fleft" href="<?php echo U('Home/Index/index');?>"><img src="/xianpipa/Public/images/logo.png" alt="logo" /></a>
		<div class="search fleft"><input class="form-control searchinput fleft" type="text" placeholder="请输入要搜索的内容"><button class="btn btn-default searchbtn fleft">搜索</button></div>
		<a class="fright mycart">
			<img class="myhover" src="/xianpipa/Public/images/hover.png" alt="购物车" />
			<span class="goodsnum"><?php echo session('goodsnum');?></span>
		</a>
	</div>
</div>
<div class="mainnav" data-action="<?php echo U('Home/Index/index');?>">
	<div class="container">
		<a class="current ml100" id="index" href="<?php echo U('Home/Index/index');?>">首页</a>
		<a id="fruit" href="<?php echo U('Home/Index/fruit');?>">鲜果区</a>
		<a id="gift" href="<?php echo U('Home/Index/gift');?>">礼品区</a>
		<a id="knowledge" href="<?php echo U('Home/Index/knowledge');?>">果食</a>
	</div>
</div>
		
	<div class="content">
		<div class="container">
			<div class="crumb">
				<a href="<?php echo U('Home/Index/index');?>"><span class="glyphicon glyphicon-home"></span>首页</a> >> 
				<a href="<?php echo U('Home/Index/fruit');?>"><span class="glyphicon glyphicon-cutlery"></span>鲜果区</a> >>
				<span class="goodstitle"><?php echo ($detail['goodsname']); ?></span>
			</div>
			<div class="detail">
				<div class="detail-left fleft">
					<div style="overflow: hidden;margin-bottom: 20px;">
						<div class="detail-imgs fleft">
							<div class="tb-booth tb-pic tb-s310">
				                <a>
				                    <img src="/xianpipa/Public/images/<?php $imgstr = explode(',', $detail['detailimg'])[2]; $root = explode('/', $imgstr)[0]; $mid = explode('/', $imgstr)[1]; echo $root.'/1_'.$mid ?>" alt="" rel="/xianpipa/Public/images/<?php $imgstr = explode(',', $detail['detailimg'])[2]; $root = explode('/', $imgstr)[0]; $mid = explode('/', $imgstr)[1]; echo $root.'/2_'.$mid ?>" class="jqzoom" style="cursor: crosshair;">
				                </a>
				            </div>
				            <ul class="detaillists tb-thumb" id="thumblist">
				                <li class="tb-selected">
				                    <div class="tb-pic tb-s40">
				                        <a>
				                            <img src="/xianpipa/Public/images/<?php echo explode(',', $detail['detailimg'])[2];?>" mid="/xianpipa/Public/images/<?php $imgstr = explode(',', $detail['detailimg'])[2]; $root = explode('/', $imgstr)[0]; $mid = explode('/', $imgstr)[1]; echo $root.'/1_'.$mid ?>" big="/xianpipa/Public/images/<?php $imgstr = explode(',', $detail['detailimg'])[2]; $root = explode('/', $imgstr)[0]; $mid = explode('/', $imgstr)[1]; echo $root.'/2_'.$mid ?>">
				                        </a>
				                    </div>
				                </li>
				                <li>
				                    <div class="tb-pic tb-s40">
				                        <a>
				                            <img src="/xianpipa/Public/images/<?php echo explode(',', $detail['detailimg'])[3];?>" mid="/xianpipa/Public/images/<?php $imgstr = explode(',', $detail['detailimg'])[3]; $root = explode('/', $imgstr)[0]; $mid = explode('/', $imgstr)[1]; echo $root.'/1_'.$mid ?>" big="/xianpipa/Public/images/<?php $imgstr = explode(',', $detail['detailimg'])[3]; $root = explode('/', $imgstr)[0]; $mid = explode('/', $imgstr)[1]; echo $root.'/2_'.$mid ?>">
				                        </a>
				                    </div>
				                </li>
				                <li>
				                    <div class="tb-pic tb-s40">
				                        <a>
				                            <img src="/xianpipa/Public/images/<?php echo explode(',', $detail['detailimg'])[4];?>" mid="/xianpipa/Public/images/<?php $imgstr = explode(',', $detail['detailimg'])[4]; $root = explode('/', $imgstr)[0]; $mid = explode('/', $imgstr)[1]; echo $root.'/1_'.$mid ?>" big="/xianpipa/Public/images/<?php $imgstr = explode(',', $detail['detailimg'])[4]; $root = explode('/', $imgstr)[0]; $mid = explode('/', $imgstr)[1]; echo $root.'/2_'.$mid ?>">
				                        </a>
				                    </div>
				                </li>
				                <li>
				                    <div class="tb-pic tb-s40">
				                        <a>
				                            <img src="/xianpipa/Public/images/<?php echo explode(',', $detail['detailimg'])[5];?>" mid="/xianpipa/Public/images/<?php $imgstr = explode(',', $detail['detailimg'])[5]; $root = explode('/', $imgstr)[0]; $mid = explode('/', $imgstr)[1]; echo $root.'/1_'.$mid ?>" big="/xianpipa/Public/images/<?php $imgstr = explode(',', $detail['detailimg'])[5]; $root = explode('/', $imgstr)[0]; $mid = explode('/', $imgstr)[1]; echo $root.'/2_'.$mid ?>">
				                        </a>
				                    </div>
				                </li>
				            </ul>
						</div>
						<div class="detail-info fleft">
							<form class="addcartform" action="<?php echo U('Home/Index/fruit');?>" method="post">
								<p class="info-title goodstitle"><?php echo ($detail['goodsname']); ?></p>
								<div class="info-block">
									<p><?php echo ($detail['weight']); ?>斤装&nbsp;&nbsp;&nbsp;&nbsp;<span class="orange">￥<?php echo ($detail['disprice']); ?></span>/<s>￥<?php echo ($detail['price']); ?></s></p>
									<p>商品编号：<span class='goodsid'><?php echo ($detail['goodsid']); ?></span></p>
								</div>
								<div class="info-block">
									<p><!-- 配送至：<span class="location">厦门 -->库存量：<?php echo ($detail['storage']); ?></span>&nbsp;&nbsp;&nbsp;&nbsp;<strong>有货</strong></p>
								</div>
								<div class="info-block">
									<div class="ml20 fleft">
										<span class="operate decrease"><img src="/xianpipa/Public/images/bag_close.gif" alt="数量减少" /></span>
										<span class="number txtcenter">1</span>
										<span class="operate increase"><img src="/xianpipa/Public/images/bag_open.gif" alt="数量增加" /></span>
									</div>
									<div class="ml20 fleft focus" data-url="<?php echo U('Home/User/goodsnotfocus');?>" data-focus="<?php echo ($detail['focus']); ?>"><img src="/xianpipa/Public/images/notfocus.png" alt="关注" /></div>
									<div class="addcartbtn ml20 fleft"  data-id="<?php echo ($detail['goodsid']); ?>"><img src="/xianpipa/Public/images/cart.png" alt="加入到购物车" /></div>
								</div>
							</form>
						</div>
					</div>
					<div class="detail-bottom">
						<ul class="detail-menu" id="detailmenu">
							<li class="current" data-id="detail">商品详情</li>
							<li data-id="comment">顾客评论(0)</li>
						</ul>
						<div class="detail-block detail-location" id="detail">
							<p>商品简介</p>
							<p style="text-indent:2em;"><?php echo ($detail['content']); ?></p>
							<p>产地：莆田&nbsp;&nbsp;&nbsp;&nbsp;净重：单重30-50g</p>
							<div class="bottomline"></div>
							<img src="/xianpipa/Public/images/<?php $imgstr = explode(',', $detail['detailimg'])[3]; $root = explode('/', $imgstr)[0]; $mid = explode('/', $imgstr)[1]; echo $root.'/2_'.$mid ?>" alt="" />
							<img src="/xianpipa/Public/images/<?php $imgstr = explode(',', $detail['detailimg'])[4]; $root = explode('/', $imgstr)[0]; $mid = explode('/', $imgstr)[1]; echo $root.'/2_'.$mid ?>" alt="" />
							<img src="/xianpipa/Public/images/<?php $imgstr = explode(',', $detail['detailimg'])[5]; $root = explode('/', $imgstr)[0]; $mid = explode('/', $imgstr)[1]; echo $root.'/2_'.$mid ?>" alt="" />
							<!-- 顾客评论 -->
							<p>顾客评论</p>
							<ul class="">
								<!-- <li>
									<img class="fleft" src="/xianpipa/Public/images/head_pic.png" alt="头像" />
									<div class="commentright fleft">
										<p class="com-user">匿名用户</p>
										<p class="com-content">非常好吃，很甜，个头很大</p>
										<p class="com-time">2015-03-25 17:23:09</p>
									</div>
								</li> -->
							</ul>
						</div>
						<div class="detail-block" id="comment">
							<p id="comment">顾客评论</p>
							<ul class="">
								<!-- <li>
									<img class="fleft" src="/xianpipa/Public/images/head_pic.png" alt="头像" />
									<div class="commentright fleft">
										<p class="com-user">匿名用户</p>
										<p class="com-content">非常好吃，很甜，个头很大</p>
										<p class="com-time">2015-03-25 17:23:09</p>
									</div>
								</li> -->
							</ul>
						</div>
					</div>
				</div>
				<div class="detail-right fright">
					<p class="curtitle">最近浏览的商品</p>
					<ul class="hotlists">
						<?php if(is_array($lastlist["list"])): $i = 0; $__LIST__ = $lastlist["list"];if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$data): $mod = ($i % 2 );++$i;?><li data-id="<?php echo ($data['pro_id']); ?>">
								<a class="detailbtn" data-href="<?php echo U('Home/User/getdetail');?>">
									<img src="/xianpipa/Public/images/<?php echo explode(',', $data['pro_img'])[1];?>" alt="枇杷" style="width:80px;height:70px;" />
									<div class="hotright fright">
										<p><?php echo ($data['pro_name']); ?></p>
										<p class="hot-price">￥<?php echo ($data['pro_disprice']); ?></p>
									</div>
								</a>
							</li>
							<div class="borderbottom"></div><?php endforeach; endif; else: echo "" ;endif; ?>
					</ul>
				</div>				
			</div>
		</div>
		<div class="addtocart txtcenter">
			<p class="addtitle">已成功加入到购物车</p>
			<p>购物车共有<span class="orange hasnum"><?php echo session('goodsnum');?></span>件商品，合计：<span class="orange total">￥<?php echo session('totalprice');?></span></p>
			<div class="close"><img src="/xianpipa/Public/images/close.png" alt="关闭" /></div>
			<div class="oprea-group">
				<button class="btn btn-default arround">继续逛逛</button>
				<a class="btn btn-default checkout" href="<?php echo U('Home/User/cart');?>">去结算</a>
			</div>
		</div>
		<div class="gotop" id="gotopbtn"><img src="/xianpipa/Public/images/top.png" alt="回到顶部" /></div>
	</div>

		<div class="footer txtcenter">
	<div class="footer-nav">
		<a href="<?php echo U('Home/User/help');?>">友情链接</a>
		<a href="<?php echo U('Home/User/help');?>">关于天天鲜果</a>
		<a href="<?php echo U('Home/User/help');?>">问题与帮助</a>
		<a href="<?php echo U('Home/User/help');?>">联系我们</a>
		<a href="<?php echo U('Admin/Index/login');?>">后台管理</a>
	</div>
	<div class="copyright">
		<p>版权所有 © 2015天天鲜果 保留所有权利 | <a>站长统计</a></p>
		<p>天天鲜果&nbsp;&nbsp;&nbsp;&nbsp;鲜果网购</p>
	</div>
</div>
	</div>
	
	<script type="text/javascript" src="/xianpipa/Public/js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="/xianpipa/Public/js/jquery.imagezoom.min.js"></script>
	<script type="text/javascript" src="/xianpipa/Public/js/myjs.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
        $(".jqzoom").imagezoom();
        $("#thumblist li a").click(function() {
            $(this).parents("li").addClass("tb-selected").siblings().removeClass("tb-selected");
            $(".jqzoom").attr('src', $(this).find("img").attr("mid"));
            $(".jqzoom").attr('rel', $(this).find("img").attr("big"));
        });

        /*添加到购物车*/
		$('.addcartbtn').click(function(){
			if($(".loginname").text()){	
				$goodsid = $(this).attr("data-id");
				$username = $('.loginname').text();
				$action = $('.addcartform').attr('action');
				// alert($goodsid);
				$.post($action,{username:$username,goodsid:$goodsid},function(data){
					$('.goodsnum').html(data.goodsnum);
					$('.total').html(data.totalprice);
					$('.addtocart').fadeIn(300).css({'display':"block"});
					setTimeout(function(){
						$('.addtocart').fadeOut(300).css({'display':"none"});
					},1500);
					$('.hasnum').html(data.goodsnum);
					// alert(data);
				});
				// location.reload();
			}else{
				location.href = "/xianpipa/index.php/Home/User/login.html";
			}
		});
		/*商品详细*/
		$('.detailbtn').click(function(e){
			$action = $(this).attr('data-href');
			$goodsid = $(this).parent('li').attr('data-id');
			$username = $('.loginname').text();
			// alert($username + " " + $goodsid);
			$.post($action,{username:$username,goodsid:$goodsid},function(data){
				$('.goodsnum').html(data.goodsnum);
				$('.total').html(data.totalprice);
				$('.hasnum').html(data.goodsnum);
				// $('.goodstitle').html(data.proname);
				location.href = "../xianpipa/index.php/Home/User/detail.html";
			});
			e.preventDefault();
		});
		if($('.focus').attr('data-focus') == 1){
			$('.focus img').attr('src','/xianpipa/Public/images/focus.png');
			$(this).attr('data-url',"<?php echo U('Home/User/goodsfocus');?>");
		}
		/*关注商品*/
		$('.focus').click(function(){
			if($('.focus img').attr('src') == '/xianpipa/Public/images/notfocus.png'){
				$('.focus img').attr('src','/xianpipa/Public/images/focus.png');
				$(this).attr('data-url',"<?php echo U('Home/User/goodsfocus');?>");
			}else{
				$('.focus img').attr('src','/xianpipa/Public/images/notfocus.png');
				$(this).attr('data-url',"<?php echo U('Home/User/goodsnotfocus');?>");
			}
			$action = $(this).attr('data-url');
			$goodsid = $('.goodsid').text();
			$.post($action,{goodsid:$goodsid},function(data){
				
			});
		});
    });
	
	/*滚动到商品详情之后停止不动*/
	function htmlScroll()
	{
	    var top = document.body.scrollTop ||  document.documentElement.scrollTop;
	    if((elFix.data_top) < top)
	    {
	        elFix.style.position = 'fixed';
	        elFix.style.top = 0;
	        elFix.style.left = elFix.data_left;
	    }
	    else
	    {
	        elFix.style.position = 'static';
	    }
	}
	function htmlPosition(obj)
	{
	    var o = obj;
	    var t = o.offsetTop;
	    var l = o.offsetLeft;
	    while(o = o.offsetParent)
	    {
	        t += o.offsetTop;
	        l += o.offsetLeft;
	    }
	    obj.data_top = t;
	    obj.data_left = l;
	}
	var oldHtmlWidth = document.documentElement.offsetWidth;
	window.onresize = function(){
	    var newHtmlWidth = document.documentElement.offsetWidth;
	    if(oldHtmlWidth == newHtmlWidth)
	    {
	        return;
	    }
	    oldHtmlWidth = newHtmlWidth;
	    elFix.style.position = 'static';
	    htmlPosition(elFix);
	    htmlScroll();
	}
	window.onscroll = htmlScroll;
	var elFix = document.getElementById('detailmenu');
	htmlPosition(elFix);

	$('#detailmenu li').click(function(){
		$('#detailmenu li').removeClass('current');
		var index = $(this).attr('data-id');
		$(this).addClass('current');
		$('.detail-block').removeClass('detail-location');
		$('#' + index).addClass('detail-location')
		document.body.scrollTop = 690;
		document.documentElement.scrollTop = 690;
	});
	</script>

</body>
</html>