<?php if (!defined('THINK_PATH')) exit();?><h1>立秋养生小知识：吃枇杷给肺“补水”</h1>
<p class="article-resource">百度百科</p>
<p>立秋养生小知识：吃枇杷给肺“补水”。这两天，有点热。进入八月人们就开始关心什么时候立秋、立秋后还热吗、立秋养生、立秋吃什么等问题。据了解，立秋是中国农历二十四节气之一，在8月7、8或9日。我国以立秋为秋季的开始。立秋养生应该有哪些注意事项呢？</p>
<p>立秋以后，天地的阴气会逐渐增长，阳气则逐渐收敛。尽管已进入秋季，但大多数地区的天气还是比较炎热，而降雨量会相对减少，这种燥热会伤害肺部的阴液，因此，润肺、除秋燥成为养生的重点。</p>
<div class="ar-block">枇杷</div>
<p>枇杷，又名金丸、芦枝，是蔷薇科中的苹果亚科的一个属，因果子形状像琵琶而得名，其味道鲜美、酸甜适度，很受水果达人的欢迎。</p>
<img src="/xianpipa/Public/images/2.jpg" alt="" />

<div class="ar-block">食疗功效</div>
<p>
润肺止咳最拿手，中医认为，枇杷味甘酸、性平，能清肺、生津、止渴，可用于治疗肺热和咳嗽、久咳不愈、咽干口渴及胃气不足等病症。枇杷中含有的胡萝卜素、维生素B1、维生素C等，都有增加机体抵抗力的作用；所含的苹果酸、柠檬酸等有机酸，能刺激消化腺的分泌，对增进食欲、帮助消化吸收、止渴解暑均有作用；其中的膳食纤维也有润肠通便的作用。此外，枇杷核中含有苦杏仁甙，能够镇咳祛痰。</p>
<img src="/xianpipa/Public/images/7.jpg" alt="" />

<div class="ar-block">吃的禁忌</div>
<p>拉肚子时别吃它</p>
<p>枇杷既可生食，又能熬膏或煎汤，每天食用别超过150克即可。虽然一般人群均可食用，但枇杷的润肺功效尤其适用于肺痿咳嗽、胸闷多痰、劳伤吐血者。不过，因其膳食纤维含量比较高，所以脾虚腹泻的人要忌食。</p>