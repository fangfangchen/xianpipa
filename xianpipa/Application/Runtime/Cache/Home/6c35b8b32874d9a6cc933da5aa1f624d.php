<?php if (!defined('THINK_PATH')) exit();?><!DOCTYPE html>
<html>
<head>
	<title><?php echo ($title); ?></title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport">
	<link rel="shortcut icon" href="/xianpipa/Public/images/1.ico" />
	<link rel="stylesheet" type="text/css" href="/xianpipa/Public/css/dist/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="/xianpipa/Public/css/index.css">
	<!--[if lt IE 9]>
	<script type="text/javascript">
		location.href = "/xianpipa/index.php/Home/User/ie";
	</script>
	<![endif]-->
</head>
<body>
	<div class="wrapper">
		<div class="top-nav">
	<div class="container">
		<div class="tn-left"><p>您好，欢迎来到天天鲜果！<?php echo $today;?></p></div>
		<div class="tn-right">
			<span class="thisname">您好，<span class="loginname"><?php echo session('user_name');?></span><span class="split">|</span></span>
			<a class="loginbtn" href="<?php echo U('Home/User/login');?>">[登录]<span class="split">|</span></a>
			<a class="registerbtn" href="<?php echo U('Home/User/register');?>">[注册]<span class="split">|</span></a>
			<a class="exitbtn" href="<?php echo U('Home/User/exitthis');?>">[退出]<span class="split">|</span></a>
			<a class="myfruit" href="<?php echo U('Home/User/myfruit');?>">我的果园</a>
		</div>
	</div>
</div>
<div class="user-header">
	<div class="container">
		<a class="logo fleft" href="<?php echo U('Home/Index/index');?>"><img src="/xianpipa/Public/images/logo.png" alt="logo" /></a>
		<div class="search fleft"><input class="form-control searchinput fleft" type="text" placeholder="请输入要搜索的内容"><button class="btn btn-default searchbtn fleft">搜索</button></div>
		<a class="fright mycart">
			<img class="myhover" src="/xianpipa/Public/images/hover.png" alt="购物车" />
			<span class="goodsnum"><?php echo session('goodsnum');?></span>
		</a>
	</div>
</div>
<div class="mainnav" data-action="<?php echo U('Home/Index/index');?>">
	<div class="container">
		<a class="current ml100" id="index" href="<?php echo U('Home/Index/index');?>">首页</a>
		<a id="fruit" href="<?php echo U('Home/Index/fruit');?>">鲜果区</a>
		<a id="gift" href="<?php echo U('Home/Index/gift');?>">礼品区</a>
		<a id="knowledge" href="<?php echo U('Home/Index/knowledge');?>">果食</a>
	</div>
</div>
		
	<div class="content">
		<div class="container">
			<div class="account fleft">
				<p>我的账户</p>
				<ul id="accountmenu">
					<li data-id="myorder">我的订单</li>
					<li data-id="focuson">关注的商品</li>
					<li data-id="myinfo">基本资料</li>
					<li data-id="address">收货地址</li>
				</ul>
			</div>
			<div class="crumb">
				<a href="<?php echo U('Home/Index/index');?>"><span class="glyphicon glyphicon-home"></span>首页</a> >> <span class="glyphicon glyphicon-user"></span>我的账户 >> <span class="glyphicon glyphicon-list-alt"></span><span class="account-lbs">我的订单</span>
			</div>
			<p class="account-lbs curtags">我的订单</p>
			<!-- 订单列表 -->
			<div class="account-right accountorder-right fleft ordercurrent" id="listorder">
				<!-- 我的订单 -->
				<div class="account-block current" id="myorder">
					<ul class="myorder-menu">
						<li class="myorder-current" data-id="allorder">所有订单</li>
						<li data-id="finish">完成的订单</li>
						<li data-id="close">取消的订单</li>
						<!-- <li data-id="waitaccess">待付款的订单</li> -->
					</ul>
					<table class="table ordertable">
						<thead>
							<th class="width28">商品信息</th>
							<th class="width12">单价(元)</th>
							<th class="width12">数量</th>
							<th class="width12">实付款(元)</th>
							<th class="width12">付款状态</th>
							<th class="width12">订单状态</th>
							<th class="width12">操作</th>
						</thead>
					</table>
					<!-- 所有订单 -->
					<div class="order-block cur-myorder" id="allorder">
						<?php if(is_array($orderlists)): $i = 0; $__LIST__ = $orderlists;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$all): $mod = ($i % 2 );++$i;?><table class="table ordertable">
								<div class="orderinfo">
									<div class="fleft orderid">订单编号： <?php echo ($all["order_id"]); ?></div>
									<div class="fright ordertime">下单时间：<?php echo ($all["post_time"]); ?></div>
								</div>
								<tbody>
									<tr>
										<td class="width28"><img src="/xianpipa/Public/images/<?php echo explode(',', $all['pro_img'])[1];?>" alt="枇杷" style="width:80px;height:70px;" /><?php echo ($all["pro_name"]); ?></td>
										<td class="width12"><?php echo ($all["pro_disprice"]); ?></td>
										<td class="width12"><?php echo ($all["pro_count"]); ?></td>
										<td class="width12"><?php echo ($all["total"]); ?></td>
										<td class="width12"><?php echo ($all["paystatus"]); ?></td>
										<td class="width12"><?php echo ($all["status"]); ?></td>
										<td class="width12"><a class="lookorder" data-orderid="<?php echo ($all["order_id"]); ?>" data-proid="<?php echo ($all["pro_id"]); ?>" data-href="<?php echo U('Home/User/myfruit');?>">查看订单&gt;&gt;</a></td>
									</tr>
								</tbody>
							</table><?php endforeach; endif; else: echo "" ;endif; ?>
					</div>
					<!-- 完成的订单 -->
					<div class="order-block" id="finish">
						<?php if(is_array($finishorder)): $i = 0; $__LIST__ = $finishorder;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$finish): $mod = ($i % 2 );++$i;?><table class="table ordertable">
								<div class="orderinfo">
									<div class="fleft">订单编号： <?php echo ($finish["order_id"]); ?></div>
									<div class="fright">下单时间：<?php echo ($finish["post_time"]); ?></div>
								</div>
								<tbody>
									<tr>
										<td class="width28"><img src="/xianpipa/Public/images/<?php echo explode(',', $finish['pro_img'])[1];?>" alt="枇杷" style="width:80px;height:70px;" /><?php echo ($finish["pro_name"]); ?></td>
										<td class="width12"><?php echo ($finish["pro_price"]); ?></td>
										<td class="width12"><?php echo ($finish["pro_count"]); ?></td>
										<td class="width12"><?php echo ($finish["total"]); ?></td>
										<td class="width12"><?php echo ($finish["paystatus"]); ?></td>
										<td class="width12"><?php echo ($finish["status"]); ?></td>
										<td class="width12"><a href="#">查看订单&gt;&gt;</a></td>
									</tr>
								</tbody>
							</table><?php endforeach; endif; else: echo "" ;endif; ?>
					</div>
					<!-- 取消的订单 -->
					<div class="order-block" id="close">
						<?php if(is_array($cancelorder)): $i = 0; $__LIST__ = $cancelorder;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$calcel): $mod = ($i % 2 );++$i;?><table class="table ordertable">
								<div class="orderinfo">
									<div class="fleft">订单编号： <?php echo ($calcel["order_id"]); ?></div>
									<div class="fright">下单时间：<?php echo ($calcel["post_time"]); ?></div>
								</div>
								<tbody>
									<tr>
										<td class="width28"><img src="/xianpipa/Public/<?php echo explode(',', $calcel['pro_img'])[1];?>" alt="枇杷" style="width:80px;height:70px;" /><?php echo ($calcel["pro_name"]); ?></td>
										<td class="width12"><?php echo ($calcel["pro_price"]); ?></td>
										<td class="width12"><?php echo ($calcel["pro_count"]); ?></td>
										<td class="width12"><?php echo ($calcel["total"]); ?></td>
										<td class="width12"><?php echo ($calcel["paystatus"]); ?></td>
										<td class="width12"><?php echo ($calcel["status"]); ?></td>
										<td class="width12"><a href="#">查看订单&gt;&gt;</a></td>
									</tr>
								</tbody>
							</table><?php endforeach; endif; else: echo "" ;endif; ?>
					</div>
					<!-- 待评价的订单 -->
					<!-- <div class="order-block" id="waitaccess">
						<?php if(is_array($waitorder)): $i = 0; $__LIST__ = $waitorder;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$wait): $mod = ($i % 2 );++$i;?><table class="table ordertable">
								<div class="orderinfo">
									<div class="fleft">订单编号： <?php echo ($wait["order_id"]); ?></div>
									<div class="fright">下单时间：<?php echo ($wait["post_time"]); ?></div>
								</div>
								<tbody>
									<tr>
										<td class="width28"><img src="/xianpipa/Public/images/<?php echo ($wait["pro_img"]); ?>" alt="枇杷" style="width:80px;height:70px;" /><?php echo ($wait["pro_name"]); ?></td>
										<td class="width12"><?php echo ($wait["pro_price"]); ?></td>
										<td class="width12"><?php echo ($wait["pro_count"]); ?></td>
										<td class="width12"><?php echo ($wait["total"]); ?></td>
										<td class="width12"><?php echo ($wait["paystatus"]); ?></td>
										<td class="width12"><?php echo ($wait["status"]); ?></td>
										<td class="width12"><a href="#">查看订单&gt;&gt;</a></td>
									</tr>
								</tbody>
							</table><?php endforeach; endif; else: echo "" ;endif; ?>
					</div> -->
				</div>
				<!-- 关注的商品 -->
				<div class="account-block" id="focuson">
					<table class="table ordertable">
						<thead>
							<th>商品信息</th>
							<th>关注时间</th>
							<th>操作</th>
						</thead>
						<tbody>
							<?php if(is_array($focuslist)): $i = 0; $__LIST__ = $focuslist;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$focus): $mod = ($i % 2 );++$i;?><tr data-id="<?php echo ($focus["pro_id"]); ?>">
									<td class="width28"><img src="/xianpipa/Public/images/<?php echo explode(',', $focus['pro_img'])[1];?>" alt="枇杷" style="width:80px;height:70px;" /><?php echo ($focus["pro_name"]); ?></td>
									<td><?php echo ($focus["focustime"]); ?></td>
									<td><a class="detailbtn" data-href="<?php echo U('Home/Index/getdetail');?>">查看</a><span class="split">|</span><a id="mynotfocus" data-href="<?php echo U('Home/User/goodsnotfocus');?>" data-proid="<?php echo ($focus["pro_id"]); ?>">取消关注</a></td>
								</tr><?php endforeach; endif; else: echo "" ;endif; ?>
						</tbody>
					</table>
				</div>
				<!-- 基本资料 -->
				<div class="account-block" id="myinfo">
					<p class="lasttime">您上次登录的时间是：<span id="logintime"><?php echo session('lasttime'); ?></span></p>
					<form class="form-horizontal myform" action="" method="">
						<div class="form-group">
							<label class="form-label col-xs-3">昵称：</label>
							<div class="col-xs-9"><input class="form-control" type="text" placeholder="请输入您的昵称" value="<?php echo ($data_info['username']); ?>"></div>
						</div>
						<!-- <div class="form-group">
							<label class="form-label col-xs-3">性别：</label>
							<div class="col-xs-9">
								<input type="radio">男
								<input type="radio">女
								<input type="radio" checked="checked">保密
							</div>
						</div> -->
						<div class="form-group">
							<label class="form-label col-xs-3">生日：</label>
							<div class="col-xs-9">
								<select>
									<option>--</option>
									<option>2014</option>
									<option>2013</option>
									<option>2012</option>
									<option>2011</option>
									<option>2010</option>
									<option>2009</option>
									<option>2008</option>
									<option>2007</option>
									<option>2006</option>
									<option>2005</option>
								</select>年
								<select>
									<option>--</option>
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
									<option>6</option>
									<option>7</option>
									<option>8</option>
									<option>9</option>
									<option>10</option>
									<option>11</option>
									<option>12</option>
								</select>月
								<select>
									<option>--</option>
									<option>1</option>
									<option>2</option>
									<option>3</option>
									<option>4</option>
									<option>5</option>
									<option>6</option>
									<option>7</option>
									<option>8</option>
									<option>9</option>
									<option>10</option>
									<option>11</option>
									<option>12</option>
								</select>日
							</div>
						</div>
						<div class="form-group">
							<label class="form-label col-xs-3">手机：</label>
							<div class="col-xs-9"><input class="form-control" type="text" placeholder="请输入您的手机号码"></div>
						</div>
						<div class="form-group">
							<label class="form-label col-xs-3">邮箱：</label>
							<div class="col-xs-9"><input class="form-control" type="text" placeholder="请输入您的邮箱"></div>
						</div>
						<div class="form-group">
							<button class="btn btn-default submitinfo">提交</button>
						</div>
					</form>						
				</div>
				<!-- 收货地址 -->
				<div class="account-block" id="address">
					<ul class="hasaddress">
						<?php if(is_array($addresslist)): $i = 0; $__LIST__ = $addresslist;if( count($__LIST__)==0 ) : echo "" ;else: foreach($__LIST__ as $key=>$addlists): $mod = ($i % 2 );++$i;?><li>
								<p class="fleft"><?php echo ($addlists['receive']); ?></p>
								<div class="fleft address-operate">
									<a class="updateaddress">修改</a>
									<a class="deleteaddress" data-href="<?php echo U('Home/User/deleteaddress');?>">删除</a>
								</div>
							</li><?php endforeach; endif; else: echo "" ;endif; ?>
					</ul>
					<p class="addbtn">+添加新地址</p>
					<form class="form-horizontal add-address" action="" method="">
						<div class="address-block">
							<div class="form-group">
								<label class="form-label col-xs-2"><span class="required">*</span>收货人：</label>
								<div class="col-xs-5"><input class="form-control" id="receive" name="receive" type="text" required palceholder="请输入收货人的名字"></div>
								<!-- <label class="control-label col-xs-5 usernameerror infoerror"><img src="/xianpipa/Public/images/unchecked.gif" alt="错误" />只能输入手机或邮箱，请重新输入！</label> -->
							</div>
							<div class="form-group">
								<label class="form-label col-xs-2"><span class="required">*</span>选择地区：</label>
								<div class="col-xs-5">
									<div class="col-xs-4 nopadding">
										<select class="form-control" id="province">
											<option>省份</option>
											<option>福建</option>
											<option>四川</option>
										</select>
									</div>
									<div class="col-xs-4 nopadding">
										<select class="form-control" id="city">
											<option>市</option>										
										</select>
									</div>
									<div class="col-xs-4 nopadding">
										<select class="form-control" id="county">
											<option>区</option>
										</select>
									</div>
								</div>
								<!-- <label class="control-label col-xs-5 usernameerror infoerror"><img src="/xianpipa/Public/images/unchecked.gif" alt="错误" />只能输入手机或邮箱，请重新输入！</label> -->
							</div>
							<div class="form-group">
								<label class="form-label col-xs-2"><span class="required">*</span>详细地址：</label>
								<div class="col-xs-5"><input class="form-control" id="detailadd" name="detailadd" type="text" required palceholder="请输入详细地址"></div>
								<!-- <label class="control-label col-xs-5 usernameerror infoerror"><img src="/xianpipa/Public/images/unchecked.gif" alt="错误" />只能输入手机或邮箱，请重新输入！</label> -->
							</div>
							<div class="form-group">
								<label class="form-label col-xs-2"><span class="required">*</span>联系方式：</label>
								<div class="col-xs-5"><input class="form-control" id="contact" name="contact" type="text" required palceholder="请输入联系方式"></div>
								<label class="control-label col-xs-5 usernameerror infoerror"><img src="/xianpipa/Public/images/unchecked.gif" alt="错误" />只能输入手机或邮箱，请重新输入！</label>
							</div>
						</div>
						<div class="form-group">
							<a class="btn btn-default save" id="saveinfo" data-href="<?php echo U('Home/User/addaddress');?>">保存收货信息</a>
							<a class="btn btn-default save" id="updateinfo" data-href="<?php echo U('Home/User/updateaddress');?>" style="display:none;">保存收货信息</a>
						</div>
					</form>
				</div>
			</div>
			<!-- 查看订单 -->
			<div class="account-right accountorder-right fleft" id="detailorder">
				订单状态 <span class="orange" id="orstatus"></span>，支付状态 <span class="orange" id="orpaystatus"></span>
				<div class="account-main">
					<div class="blocktitle">
						<div class="fleft" id="thisorderid"></div>
						<div class="fright" id="thisordertime"></div>
					</div>
					<div class="blockcontent">
						<p id="orreceive"></p>
						<p id="oraddress"></p>
						<p id="ortel"></p>
					</div>
					<div class="blocktitle">支付方式</div>
					<div class="blockcontent">
						<p id="orpayway">支付方式：支付宝付款</p>
					</div>
					<table class="table ordertable cartlists" data-action="<?php echo U('Home/User/cart');?>">
						<thead>
							<th>商品信息</th>
							<th>规格</th>
							<th>原价(元)</th>
							<th>单价(元)</th>
							<th>数量</th>
							<th>优惠</th>
							<th>小计</th>
						</thead>
						<tbody id="ordertable"></tbody>
					</table>
					<div class="cart-operate">
					<p class="txtcenter">商品金额总计：<span class="orange total"></span></p>
				</div>
				</div>
			</div>
		</div>
	</div>

		<div class="footer txtcenter">
	<div class="footer-nav">
		<a href="<?php echo U('Home/User/help');?>">友情链接</a>
		<a href="<?php echo U('Home/User/help');?>">关于天天鲜果</a>
		<a href="<?php echo U('Home/User/help');?>">问题与帮助</a>
		<a href="<?php echo U('Home/User/help');?>">联系我们</a>
		<a href="<?php echo U('Admin/Index/login');?>">后台管理</a>
	</div>
	<div class="copyright">
		<p>版权所有 © 2015天天鲜果 保留所有权利 | <a>站长统计</a></p>
		<p>天天鲜果&nbsp;&nbsp;&nbsp;&nbsp;鲜果网购</p>
	</div>
</div>
	</div>
	
	<script type="text/javascript" src="/xianpipa/Public/js/jquery-1.9.1.min.js"></script>
	<script type="text/javascript" src="/xianpipa/Public/js/myjs.js"></script>
	<script type="text/javascript">
	$(document).ready(function(){
		var $oldaddress = "";
		$('#accountmenu li').click(function(){
			var curblock = $(this).attr('data-id');
			var text = $(this).text();
			$('.account-lbs').html(text);
			$('.account-block').removeClass('current');
			$('#' + curblock).addClass('current');
		});
		/*我的订单*/
		$('#myorder li').click(function(){
			var blockid = $(this).attr('data-id');
			$('.order-block').removeClass('cur-myorder');
			$('#' + blockid).addClass('cur-myorder');
			$('#myorder li').removeClass('myorder-current');
			$(this).addClass('myorder-current');
		});
		/*查看订单*/
		$('.lookorder').click(function(){
			$('#listorder').removeClass('ordercurrent');
			$('#detailorder').addClass('ordercurrent');
			var $orderid = $(this).attr('data-orderid'),
				$proid = $(this).attr('data-proid'),
				$action = $(this).attr('data-href');
			$('#thisorderid').html('订单号：' + $orderid);
			$.post($action,{orderid:$orderid,proid:$proid},function(data){
				$('#thisordertime').html('下单时间：' + data.posttime);
				$('#orstatus').html(data.status);
				$('#orpaystatus').html(data.paystatus);
				$('#orreceive').html(data['address'][0]);
				$('#oraddress').html(data['address'][1]);
				$('#ortel').html(data['address'][2]);
				$('#orpayway').html('支付方式：' + data.payway);
				// alert(data['orderlength']);
				$('#ordertable').empty();
				for(var i = 0; i < data.orderlength; i++){
					var $proid = data[i]['proid'];
					$('#ordertable').append('<tr class="' + $proid + '"><td><img class="orimg" src="" alt="枇杷" style="width: 80px;height 70px;margin-right:10px;" /><span class="orname"></span></td><td class="orweight"></td><td class="orperprice"></td><td class="ordisprice"></td><td><span class="txtcenter oraddcount"></span></td><td></td><td class="sumprice orsumprice"></td></tr>');
					$('.' + $proid + ' .orname').html(data[i]['proname']);
					$('.' + $proid + ' .orimg').attr('src', '/xianpipa/Public/images/<?php echo explode(',', $all['pro_img'])[1];?>');
					$('.' + $proid + ' .orweight').html(data[i]['proweight']);
					$('.' + $proid + ' .ordisprice').html(data[i]['prodisprice']);
					$('.' + $proid + ' .orperprice').html(data[i]['proprice']);
					$('.' + $proid + ' .oraddcount').html(data[i]['proaddcount']);
					$('.' + $proid + ' .orsumprice').html(data[i]['prosumprice']);
				}
				$('.total').html('￥' + data.total);
			});
		});
		$('#accountmenu li').click(function(){
			$('#detailorder').removeClass('ordercurrent');
			$('#listorder').addClass('ordercurrent');
		});
		/*取消关注*/
		$('#mynotfocus').click(function(){
			$action = $(this).attr('data-href');
			$goodsid = $(this).attr('data-proid');
			$.post($action,{goodsid:$goodsid},function(data){
				if(data == 1){
					alert("取消关注成功！");
					location.reload();
					// $('.account-block').removeClass('current');
					// $('#focuson').addClass('current');
				}else{
					alert("取消关注失败！");
				}
			});
		});
		/*查看商品详细*/
		$('.detailbtn').click(function(e){
			$action = $(this).attr('data-href');
			$goodsid = $(this).parents('tr').attr('data-id');
			$username = $('.loginname').text();
			$.post($action,{username:$username,goodsid:$goodsid},function(data){
				$('.goodsnum').html(data.goodsnum);
				$('.total').html(data.totalprice);
				$('.hasnum').html(data.goodsnum);
				// $('.goodstitle').html(data.proname);
				location.href = "/xianpipa/index.php/Home/Index/detail.html";
			});
			e.preventDefault();
		});
		/*选择省市区*/
		$('#province').change(function(){			
			if($('#province').val() == "福建"){
				$('#city').empty().append('<option>厦门</option>');
				$('#county').empty().append('<option>集美区</option>')
							.append('<option>湖里区</option>');
			}else if($('#province').val() == "四川"){
				$('#city').empty().append('<option>成都</option>');
				$('#county').empty().append('<option>锦江区</option>')
							.append('<option>青羊区</option>');
			}else if($('#province').val() == "省份"){
				$('#city').empty().append('<option>市</option>');
				$('#county').empty().append('<option>区</option>');
			}
		});
		/*添加收货地址*/
		$('#saveinfo').click(function(){
			var $receive = $('#receive').val(),
				$province = $('#province').val(),
				$city = $('#city').val(),
				$county = $('#county').val(),
				$detailadd = $('#detailadd').val(),
				$contact = $('#contact').val(),
				$action = $(this).attr('data-href');
			if($receive != "" && $province != "省份" && $city != "市" && $county != "区" && $detailadd != "" && $contact != ""){
				var $infostr = $receive + "," + $province + "省" + $city + "市" + $county + $detailadd + "," + $contact;	
				var $thisindex = $(this).parents('.orangeborder');
				$.post($action,{address:$infostr},function(data){
					if(data == 1){
						alert('恭喜您，添加成功！');
						$('.hasaddress').append('<li><p class="fleft">' + $infostr + '</p><div class="fleft address-operate"><a class="updateaddress">修改</a><a class="deleteaddress" data-href="' + "<pre><?php echo U('Home/User/deleteaddress');?></pre>" + '">删除</a></div></li>');
						$('#receive').val(" ");
						$('#province').val(" ");
						$('#city').val(" ");
						$('#county').val(" ");
						$('#detailadd').val(" ");
						$('#contact').val(" ");
					}else if(data == 0){
						alert('不好意思，添加的地址已存在！');
					}
				});
			}
		});
		/*删除地址*/
		$('.deleteaddress').click(function(){
			$action = $(this).attr('data-href');
			$username = $('.loginname').text();
			$address = $(this).parents('li').children('p').text();
			$.post($action,{username:$username,address:$address},function(data){
				if(data == 1){
					alert('删除该地址成功！');
					location.reload();
					// $('.account-block').removeClass('current');
					// $('#address').addClass('current');					
				}else{
					alert('删除该地址失败！');
				}
			});
		});
		/*修改地址*/
		$('.updateaddress').click(function(){
			$('#saveinfo').css({'display':'none'});
			$('#updateinfo').css({'display':'inline-block'});
			$action = $(this).attr('data-href');
			$username = $('.loginname').text();
			$address = $(this).parents('li').children('p').text();
			$oldaddress = $address;   //存下要修改的旧地址
			$addarray = $address.split(',');
			$('#receive').val($addarray[0]);
			$('#contact').val($addarray[2]);
			$province = $addarray[1].split('省');
			$('#province').val($province[0]);
			if($('#province').val() == "福建"){
				$('#city').empty().append('<option>厦门</option>');
				$('#county').empty().append('<option>集美区</option>')
							.append('<option>湖里区</option>');
			}else if($('#province').val() == "四川"){
				$('#city').empty().append('<option>成都</option>');
				$('#county').empty().append('<option>锦江区</option>')
							.append('<option>青羊区</option>');
			}else if($('#province').val() == "省份"){
				$('#city').empty().append('<option>市</option>');
				$('#county').empty().append('<option>区</option>');
			}
			$city = $province[1].split('市');
			$('#city').val($city[0]);
			$county = $city[1].split('区');
			$('#county').val($county[0]);
			$('#detailadd').val($county[1]);
			$(this).parents('li').empty();
		});
		/*保存修改之后的地址*/
		$('#updateinfo').click(function(){
			var $receive = $('#receive').val(),
				$province = $('#province').val(),
				$city = $('#city').val(),
				$county = $('#county').val(),
				$detailadd = $('#detailadd').val(),
				$contact = $('#contact').val(),
				$action = $(this).attr('data-href');
			if($receive != "" && $province != "省份" && $city != "市" && $county != "区" && $detailadd != "" && $contact != ""){
				var $infostr = $receive + "," + $province + "省" + $city + "市" + $county + $detailadd + "," + $contact;	
				var $thisindex = $(this).parents('.orangeborder');
				$.post($action,{oldaddress:$oldaddress,address:$infostr},function(data){
					alert('恭喜您，更新成功！');
					$('.hasaddress').append('<li><p class="fleft">' + $infostr + '</p><div class="fleft address-operate"><a class="updateaddress">修改</a><a class="deleteaddress" data-href="' + "<pre><?php echo U('Home/User/deleteaddress');?></pre>" + '">删除</a></div></li>');
					$('#receive').val(" ");
					$('#province').val(" ");
					$('#city').val(" ");
					$('#county').val(" ");
					$('#detailadd').val(" ");
					$('#contact').val(" ");
					$('#saveinfo').css({'display':'inline-block'});
					$('#updateinfo').css({'display':'none'});
				});
			}			
		});
	});
	</script>

</body>
</html>