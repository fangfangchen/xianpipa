/*回到顶部函数*/
function pageScroll(){	    
    scrolldelay = setTimeout('pageScroll()',100);
    var sTop = document.documentElement.scrollTop + document.body.scrollTop;
    if(sTop >= 300){
		$('.gotop').css({'display':"block"});
	}else{
		$('.gotop').css({'display':"none"});
	}
}
$('#gotopbtn').click(function(){
	document.body.scrollTop = 0;
	document.documentElement.scrollTop = 0;
});
$(document).ready(function(){
	/*登录判断*/
	if($(".loginname").text()){
		$('.myfruit').attr('href','/xianpipa/index.php/Home/User/myfruit.html');
		$('.mycart').attr('href','/xianpipa/index.php/Home/User/cart.html');
		$('.loginbtn,.registerbtn').css({'display':"none"});
		$('.thisname,.exitbtn').css({'display':"inline-block"});
	}else{
		$('.myfruit').attr('href','/xianpipa/index.php/Home/User/login.html');
		$('.mycart').attr('href','/xianpipa/index.php/Home/User/login.html');
		$('.loginbtn,.registerbtn').css({'display':"inline-block"});
		$('.thisname,.exitbtn').css({'display':"none"});
		$('.goodsnum').html("0");
	}
	/*回到顶部*/
	pageScroll();

	$('.close,.arround').click(function(){
		$('.addtocart').fadeOut(300).css({'display':"none"});
	});
	/*数量增加或者减少*/
	$('.decrease').click(function(){
		var curnum = $(this).parent().children('.number').text();
		if(curnum == 1){
			$(this).parent().children('.number').html(curnum);
		}else{
			var num = parseFloat(curnum) - 1;
			$(this).parent().children('.number').html(num);
		}
		upanddown($(this));
	});
	$('.increase').click(function(){
		var curnum = $(this).parent().children('.number').text();
		var num = parseFloat(curnum) + 1;
		$(this).parent().children('.number').html(num);
		upanddown($(this));
	});
	function upanddown(obj){
		var perprice = parseFloat(obj.parents('tr').children('.perprice').text()),
			weight = parseFloat(obj.parent().children('.number').text()),
			sumprice = (perprice * weight).toFixed(2);
		obj.parents('tr').children('.sumprice').html(sumprice);
		var len = $('.sumprice').length,total = 0;
		for(var i = 0; i < len; i++){
			total = total + parseFloat($('.sumprice').eq(i).text());
		}
		$('.total').html(total.toFixed(2));
	}
	/*去结算提交数据至后台数据库*/
	$('.checkout').click(function(){
		$action = $('.cartlists').attr('data-action');
		var len = $('.goodsid').length;
		for(var i = 0; i < len; i++){
			$goodsid = $('.goodsid').eq(i).text();
			$goodsnum = $('.number').eq(i).text();
			$sumprice = $('.sumprice').eq(i).text();
			$.post($action,{goodsid:$goodsid, goodsnum:$goodsnum, sumprice:$sumprice},function(data){
				// alert(data);
				$('.goodsnum').html(data);
			});
		}
	});
	/*刷选条件*/
	// $('.resource a').click(function(){
	// 	$('.resource a').removeClass('con-current');
	// 	$(this).addClass('con-current');
	// 	$action = $('.resource').attr('data-url');
	// 	$con = $(this).text();
	// 	// $.post($action,{con:$con},function(data){
	// 	// 	location.href = "../Index/selproduce.html";
	// 	// 	// location.reload();
	// 	// 	// $('.addcartform').empty().append('<volist name="fruitlist.list" id="data"><li data-id="' + data.pro_id + '"><a class="detailbtn" data-href=' + "{:U('Home/Index/getdetail')}" + '><img src="__PUBLIC__/images/' + data.pro_img|default="default.png" + '" alt="枇杷" /><p class="goodstitle">' + data.pro_name + '</p><p>' + data.pro_weight + '斤装</p><p class="price">￥' + data.pro_price + '</p></a><div class="ml10 fleft"><span class="operate decrease"><img src="__PUBLIC__/images/bag_close.gif" alt="数量减少" /></span><span class="number txtcenter">1</span><span class="operate increase"><img src="__PUBLIC__/images/bag_open.gif" alt="数量增加" /></span></div><div class="addcartbtn fright"><img src="__PUBLIC__/images/cart.png" alt="加入到购物车" /></div></li></volist>');
	// 	// });
	// });
});